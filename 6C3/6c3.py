#!/usr/local/bin/python3
# -*- coding: UTF-8-*-

import subprocess 
import re
import json
import sys
import os
import xml.etree.ElementTree as ET

def lspl_search_file(txt1251_input_file_path, xml1251_output_file_path):
	"""
	Собирает командную строку для 
	"""
	shell_array = [
		os.path.join("bin","lspl-find"),
		"-i",
		txt1251_input_file_path,
		"-p",
		"lspl_patterns.txt",
		"-o",
		xml1251_output_file_path,
		#Псевдошаблон, содержащий в себе все остальные, 
		#чтобы изменения можно было вносить только в файл шаблонов
		"FinalLang" 
		]
	p = subprocess.check_output(shell_array)

def convert(input_file_path, output_file_path, input_encoding, output_encoding):
	"""
	Считывает файл в кодировке input_encoding, выводит обратно в output_encoding
	"""
	file_descriptor = open(input_file_path, 'r', encoding = input_encoding)
	file_data = file_descriptor.read()
	file_descriptor.close()
	file_descriptor = open(output_file_path,'w',encoding = output_encoding)
	file_descriptor.write(file_data)
	file_descriptor.close()

def lspl_search_directory():
	"""
	Проходит по всем файлам в директории input_files,
	Кодирует их в cp1251 и отправляет на вход lspl-find
	"""
	files = os.listdir("input_files")
	for filename in files:
		if filename[-4:] == ".txt":
			try:
				input_file_path = os.path.join("input_files", filename)
				txt1251_file_path = os.path.join("output_files", filename[:-4]+".txt1251")
				xml1251_file_path = os.path.join("output_files", filename[:-4]+".xml1251")
				#Копируем входные utf-8 файлы в cp1251 для утилиты lspl-find
				convert(input_file_path, txt1251_file_path, "utf-8", "cp1251")
				#Процесс поиска в файле по lspl шаблонам
				lspl_search_file(txt1251_file_path, xml1251_file_path)
			except (UnicodeEncodeError) as e:
				print ("UnicodeEncodeError")
				pass

def lspl_post_process_directory():
	"""
	Перекодирует выходные .xml файлы. Создает их дубликаты .txt, 
	содержащие только json массив найденных языков без лишней информации.
	"""
	total_languages = set() # для общей статистики.

	files = os.listdir("output_files")
	for filename in files:
		file_path = os.path.join("output_files", filename)
		if file_path[-8:] == ".txt1251":
			# Удаляем временный txt1251 файл
			os.remove(file_path)

		if file_path[-8:] == ".xml1251":	
			# Обрабатываем временный xml1251 файл, затем перенесем его в постоянное место.
			xml1251_file_path = file_path

			file_descriptor = open(xml1251_file_path, 'r', encoding="cp1251")
			xml_data = file_descriptor.read()
			file_descriptor.close()
			# переносим временный файл, чтобы не трогать его 
			# при повторном запуске программы
			os.remove(xml1251_file_path) 
			# записываем в новое место.
			xml_file_path = xml1251_file_path[:-8]+".xml"
			file_descriptor = open(xml_file_path, 'w', encoding = "utf-8")
			file_descriptor.write(xml_data)
			file_descriptor.close()
			try:
				texts = ET.fromstring(xml_data) # парсим прочитанные xml данные
			except (FileNotFoundError, ET.ParseError) as e:
				print(xml_data)
				print("Xml Parsing Error")
				continue
			# Инициализируем множество, чтобы автоматически отбросить все повторения в результатах
			lang_set = set()
			for text in texts:
				for goal in text:
					for match in goal:
						try:
							if len(match)>1:
								lang = match[1].text
								lang_set.add(lang)
								total_languages.add(lang)
						except (AttributeError, IndexError) as e:
							pass
			lang_list = []
			for lang in lang_set:
				lang_list.append(lang)
			txt_file_path = xml_file_path[:-4]+".txt"
			file_descriptor = open(txt_file_path,'w', encoding="utf-8")
			file_descriptor.write(json.dumps(lang_list, ensure_ascii=False, indent = 4))
			file_descriptor.close()
	"""
	Записываем полную статистику за текущий запуск.
	"""
	total_statistic_descr = open("last_total.txt",'w',encoding="utf-8")
	total_statistic_descr.write(json.dumps(list(total_languages), ensure_ascii=False, indent = 4))
	total_statistic_descr.close()

lspl_search_directory()
lspl_post_process_directory()