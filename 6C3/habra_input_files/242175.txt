     Facebook недавно открыл миру наличие скрытого tor-сервиса, дающего более безопасный доступ к их сайту. Пользователи и журналисты спрашивали комментариев у разработчиков Tor по этому вопросу. Перед вами их ответы и размышления.

Часть первая: да, в посещении сайта Facebook через Tor нет никаких противоречий
Мне казалось, что в этом пояснении нет нужды, пока я не услышал от журналиста вопрос – почему пользователи Tor не будут использовать Facebook. Оставляя в стороне отношение Facebook к приватности и их правила связанные с использованием настоящих имён, и надо ли вам что-то сообщать в Facebook о своей персоне,- главное здесь то, что анонимность не заканчивается только попыткой скрыться от сайта, на который вы ходите.

Нет никаких причин для того, чтобы показывать вашему провайдеру, используете ли вы Facebook. Нет причин для хостера Facebook или какого-либо агентства, отслеживающего интернет, знать о том, используете ли вы Facebook. И если вы добровольно раскрываете какие-то свои данные в Facebook, всё равно нет причин для того, чтобы они автоматически узнавали, в каком городе вы сегодня находитесь.

Кроме того, есть в мире места, откуда Facebook недоступен. Когда-то давно я разговаривал с человеком из Facebook, отвечающим за безопасность, который рассказал мне забавную историю. Когда он первый раз узнал про Tor, он ненавидел и боялся его, поскольку он «очевидно» предназначался для разрушения бизнес-модели Facebook – узнать всё про всех пользователей. Затем внезапно Иран заблокировал Facebook, после чего большая часть пользователей сервиса пересела на Tor для посещения Facebook, и человек превратился в фаната Tor, потому что без него все эти пользователи просто были бы отрезаны. Другие страны, к примеру Китай, после этого случая внедряли похожие меры. И эта перемена в отношении к Tor от «Tor – инструмент приватности, позволяющий пользователям контролировать их данные» к «Tor – инструмент общения, позволяющий пользователям выбирать самим, какие сайты они хотят посещать» — это отличный пример разнообразия возможностей использования Tor. Какое бы вы использование Tor не придумали, где-то найдётся человек, который использует его неожиданным для вас способом.

Часть вторая: мы с радостью встречаем расширение использования скрытых сервисов
Я считаю – очень здорово со стороны Facebook было добавить себе адрес .onion. Бывают случаи, когда использованию этих адресов нет альтернатив: к примеру статья «использование скрытых сервисов с благими намерениями», или грядущие децентрализованные сервисы типа чата Ricochet, где каждый пользователь – это скрытый сервис, поэтому нет центральной точки, куда можно было бы подключиться для прослушки или давления. Но мы особенно не афишировали эти адреса, по крайней мере не так сильно, как делали это некоторые резонансные случаи типа «мой сайт хотят закрыть». 

У скрытых сервисов есть много свойств, способствующих безопасности. Во-первых – благодаря устройству Tor тяжело выяснить, где именно находится сервис. Во-вторых – так как адрес сервиса одновременно является хэшем его ключа, идентификация происходит автоматически. Если вы ввели .onion-адрес, ваш клиент Tor гарантирует, что вы соединяетесь с сервисом, располагающим приватным ключом, соответствующим этому адресу. В-третьих – процесс диалога происходит с шифрованием, даже если на уровне приложений трафик не шифруется.

Поэтому я рад такому шагу со стороны Facebook – он поможет распространению информации о том, почему людям может понадобиться устроить свой скрытый сервис, и поможет людям задуматься о вариантах применения таких сервисов.

Ещё один приятный бонус – Facebook серьёзно относится к своим пользователям, приходящим к ним через Tor. Сотни тысяч людей таким образом используют Facebook уже несколько лет, но в наши дни, когда такой проект, как Википедия, запрещает редактирование материалов Tor-пользователям, такой крупный сайт решает что он не против того, чтобы его пользователи были в большей безопасности.

В дополнение я хотел бы сказать, что был бы очень расстроен, если бы Facebook после нескольких проблем с троллями решили бы запретить использовать их основной адрес через Tor. Мы должны быть бдительными и помогать Facebook продолжать предоставлять доступ к своему сайту через оба адреса.

Часть третья: адрес немного помпезный, и что с того
Адрес скрытого сервиса Facebook — facebookcorewwwi.onion. Он не выглядит как случайный хэш публичного ключа. Многие интересовались, как же им удалось пробрутфорсить такое длинное имя.

Сначало было просто сгенерировано много строчек, начинающихся с «facebook». Затем они выбирали из этих строчек такие, вторая половина которых была наиболее красивой. Выбрали «corewwwi», про которую можно даже сочинить хорошее объяснение того, что она означает.

В связи с этим поясню – нет возможности сгенерить точное имя, которое вам нужно. Для этого пришлось бы брутфорсить все 80 бит. Для дальнейшего чтения рекомендуем «Атака дней рождения». Тем, кто хочет помочь сервису Tor, мы рекомендуем прочесть статьи «скрытым сервисам нужно немного любви» и «предложение по Tor 224».

Часть четвёртая: что мы думаем по поводу https-сертификата для адреса .onion ?
В Facebook не только подняли скрытый сервис, они ещё получили сертификат для https на этот сервис, подписанный Digicert. Это привело к жарким дискуссиям в сообществах людей, занимающихся сертификатами и браузерами – они пытаются решить, каким именам можно получать сертификаты. Диалог пока продолжается, и вот мои размышления по этому вопросу.

За сертификаты: мы, люди из сообщества специалистов по безопасности в интернете, учим людей, что https необходим, а http – опасен. Поэтому есть смысл в том, что пользователя хотят видеть https перед адресом.

Против: В Tor вся эта безопасность уже встроена, поэтому агитируя людей платить Digicert, мы пропагандируем эту бизнес-модель, в то время как мы должны пропагандировать альтернативу ей.

За: Вообще, в данном случае https даёт кое-какое преимущество – если tor-сервис находится не на том же сервере, на котором находится сам сайт. Конечно, эта «последняя миля» между сервисом и сайтом проходит по внутренним сетям компании, но тем не менее.

Против: если один сайт получит сертификат, это наведёт пользователь на мысли о его необходимости, и они станут спрашивать его у других сервисов. Меня волнует, не начнётся ли такая тенденция, что для поднятия скрытого сервиса вам надо заплатить денег Digicert, или же ваши пользователи не воспримут его всерьёз. Особенно, когда скрытые сервисы, заботящиеся о своей анонимности, будут иметь проблемы с получением сертификатов.

Одна из альтернатив – встроить в Tor Browser условие, чтобы он не показывал пугающее всплывающее окно для адресов в зоне .onion, работающих по https. Более интересный вариант – сделать так, чтобы скрытые сервисы могли сами генерить самоподписанные https-сертификаты, используя свой приватный onion-ключ, и научить Tor Browser подтверждать их – в общем, ввести децентрализованную систему выдачи сертификатов для адресов .onion, поскольку они и так автоматически идентифицируются. Тогда не надо будет заниматься ерундой с обычной процедурой получения сертификатов.

Можно также представить такую модель поведения, когда пользователь может сообщить своему Tor Browser, что вот этот адрес .onion – это Facebook. Или более прямолинейно – распространять список ссылок «известных» скрытых адресов вместе с Tor Browser, на манер своего собственного списка сертификатов. Тогда встанет вопрос – а какие сайты включать в этот список.

Поэтому я ещё не определился, в каком направлении должна идти дискуссия. Я симпатизирую подходу «мы приучили пользователей к необходимости https, давайте не будем их путать», но я ещё и волнуюсь, чтобы получение сертификата не стало необходимым шагом для сервиса.

Часть пятая: что ещё нужно сделать?
В плане дизайна и безопасности скрытым сервисам нужно ещё немного любви. Мы планируем улучшения дизайна, но не имеем достаточно разработчиков и финансирования для их реализации. Мы беседовали с инженерами Facebook на этой неделе по поводу надёжности и масштабируемости скрытых сервисов, и мы рады, что Facebook раздумывает над тем, не помочь ли нам в развитии скрытых сервисов.
