   	 

В этом обзоре мы поговорим про бесплатные курсы виртуальной академии Microsoft MVA, которые будут полезны как профессиональным разработчикам программного обеспечения и ИТ-про, так и новичкам. Обратите внимание, что видео-плеер на сайте для ряда курсов, которые созданы на английском языке, содержит возможность включить русские субтитры.

Новый! Введение в Azure API Management

Данный курс знакомит слушателей с новыми возможностями облачной платформы Microsoft Azure в виде сервиса управления API Management. В рамках курса вы познакомитесь с сервисов управления API, его возможностями для разработчиков API и пользователей. 

Хит! Новый! Основы построения доменной сети. Часть 2 

Курс «Основы построения доменной сети. Часть 2» представляет собой введение в инструменты управления пользователями в компьютерных сетей, построенных на основе операционных систем Microsoft. В рамках курса рассматриваются настройки политик паролей, настройки предоставления доступа к файлам в доменной сети и настройки сетевых принтеров. В курсе вы узнаете о том, как настроить требования к паролям для пользователей вашей доменной сети, как предоставить разным пользователям разные права на доступ к общим файлам, как создать принтер и предоставить к нему доступ. Продолжение, начало в первой части курса.

Хит! Введение в сервис машинного обучения Azure Machine Learning

Если вы не исследователь данных, но интересуетесь анализом данных (data mining) и прогнозным анализом (predictive analytics), а так же желаете узнать больше чем просто о построении отчетности в цифрах, то просмотрите этот курс о сервисе Azure Machine Learning (ML). ML – дешевый, легкодоступный и мощный сервис прогнозного анализа предлагаемый Microsoft.

В этом курсе построенном на демонстрациях эксперты Buck Woody, Seayoung Rhee и Scott Klein покажут различные пути, которые вы можете использовать для эффективного внедрения прогнозирования в свои решения обработки больших данных, и разберут лучшие практики анализа трендов и паттернов. Узнайте как расширять Azure ML с помощью сервисов Azure ML API и познакомьтесь с методами и сценариями монетизации ваших ML-приложений c помощью Azure Marketplace.

Хит! Кроссплатформенная разработка вместе с Visual Studio

Веб-разработчики, вы заинтересованы в том, как используя свои навыки строить кроссплатформенные приложения для основных мобильных платформ? В курсе вы увидите как команда экспертов делится своим большим опытом разработки реальных приложений, плюс гости курса поделятся с вами тем, как быстро перейти от веба к приложениям.

От инструментов разработки до фреймворков и среды исполнения. Узнайте на что способны HTML5, JavaScript и CSS вместе с открытым проектом Apache Cordova и интеграцией в Visual Studio. Исследуйте множество технологий, включая Cordova, Coco Games, Babylon,js и другие. 

Новый! Как инвентаризировать свое окружение на Windows Server 2003

Сейчас самое время для того чтобы обновить свои устаревшие системы. Поддержка Windows Server 2003 подходит к концу, так что вам пригодится помощь в миграции. Эксперт Джастин Престон расскажет вам о двух инструментах, которые помогут упростить процесс миграции. Множество приложений может быть перенесено в облако с Microsoft Azure. Воспользуйтесь инструментами чтобы принять правильное решение.

Глубокое погружение. Развертывание сервисов Remote Desktop Services в облаке Azure 

Если вам хочется научится создавать безопасные, масштабируемые и надежные решения хостинга десктопов для небольших и средних организаций до 1500 пользователей, обратите внимание на этот курс. Узнайте как использовать Remote Desktop Services и виртуальные машины Microsoft Azure для создания мультитенантных сервисов хостинга десктопов и приложений Windows.

Новичкам. Английский. Введение в разработку мобильных приложений

Nothing motivates students more than building an app that they care about and that they can immediately see, use, and share. Get insights from Windows Platform Developer MVP​ Lance McCarthy, as he teaches students to build a mobile app using Windows App Studio and to then extend and enhance the app using Visual Studio.

Students learn the basics of the app ecosystem and Software Development Lifecycle. They also learn about code modification and additional basic app coding skills, including the topics of variables, simple data types, conditional programming constructs, and simple library classes. This course addresses AP Computer Science learning requirements.

Новичкам. Английский. Последняя остановка: Вывод вашего приложения в Магазин 

Developers, if you're ready to cross the finish line and get your Windows Store apps to market, don't miss this opportunity! Learn about a new pilot program that combines a free technical review from Microsoft Premier Field Engineering (PFE) with credit toward your MCSD: Windows Store Apps certification. Sneak a peek into the app review process, get tips on design and implementation, and take a look at common pitfalls and blockers to getting apps into the Windows Store. Don't miss the last stop!

Новичкам. Английский. Введение в jQuery

Want to know what exactly jQuery is and why it's so wildly popular? Get answers, and watch an exciting day of jQuery goodness hosted by Jeremy Foster and Rachel Appel, who teach you what it is, why you need it, and how you use it. (Spoiler alert: jQuery makes your dev work significantly easier, and it's inherently reactive!)

In this sixth Web Wednesdays event, learn about the history and contributors to jQuery, and explore lots of features. Find out how this JavaScript library makes DOM manipulation and event handling easier. Examine AJAX, asynchrony, and effects. Plus, get practical guidance on when to use jQuery and when to combine it with other libraries.

Новичкам. Английский. Основы работы с Visual Studio Online

New to Visual Studio Online? Explore this set of services for developers, and learn how they can help you with your daily work or personal projects. Watch a series of 10-minute videos to get an overview of the Visual Studio Online services and how to work with them. Keep an eye on the Features Timeline, which lists updates made to Visual Studio Online and identifies the version of Team Foundation Server which includes them. We deploy new bits approximately every three weeks. Happy coding!

Английский. Оценка и улучшение ваших возможностей DevOps

Why DevOps, why now, and how can it help you? Build your resume and confidence by knowing how to answer these questions, which tools to use, and how to resource your projects. DevOps can mean the difference between being a leader in a market or a follower behind the competition.

Watch industry experts, executives, and thought leaders, including Gene Kim of The Phoenix Project, as they take a deep dive into DevOps and how it can help streamline your workload and enable continuous delivery. Get practical tips you can implement immediately in your company, and gain an understanding of how a maturity model can transition your organization into a meaningful DevOps practice.

Английский. Быстрое испытание: Отладка графических приложений

Now's your chance to get to know the DirectX graphics debugging tools in Visual Studio. Learn how to capture a frame from a game, and then see how that frame was rendered by stepping through the individual graphics events. Explore the shader, and look at the C++ code that needs to be edited to make changes to the game.

Английский. Быстрое испытание: Разделение данных — XAML & openFrameworks

If you'd like to find out how to bind XAML controls so that data is passed from one control to another, don't miss this hands-on lab. Then, build on this knowledge to learn how to implement a bindable class that allows you bind an openFramework object to a XAML control. Get the details in this Quick Start Challenge. 

Английский. Реализация Basic PKI в Windows Server 2012 R2

Do you know how to implement a public key infrastructure (PKI)? If not, this is your chance! A PKI is a set of hardware, software, people, policies, and procedures needed to create, manage, distribute, use, store, and revoke digital certificates. In this course, see demonstrations and walk-throughs of the requirements to deploy a PKI, hear how to install the required components, and explore management of the certificate environment.

Английский. Windows Server 2012 R2: улучшения в Active Directory

Interested in Active Directory enhancements within Windows Server 2012 R2? Join us for a detailed look. Explore the options for deploying domain controllers using the new Server Manager and using both the GUI and Windows PowerShell. Hear about the new Active Directory Management tool, and use its Windows PowerShell History Viewer.

Building on the PowerShell theme, find out how to use PowerShell to automate day-day IT Professional tasks. Finally, see how to prepare and carry out domain controller cloning.

Набор курсов по Office 365 и Project Online:


Уголок поддержки Office 365: Начало работы с Project Online. Office 365 Support Corner is an ongoing series of on-demand courses that cover leading Office 365 support issues. ​Check out this course on Project Online, the newest member of the Project family. Find out what this subscription service is and what it isn't, and learn what you need to know to get up and running with Project Online smoothly and quickly. 
Уголок поддержки Office 365: Следующие шаги с Project Online. Office 365 Support Corner is an ongoing series of on-demand courses that cover leading Office 365 support issues. ​​If you have a new Project Online tenant, this is your opportunity to learn more about its capabilities and what you need to configure to deliver business value for your users. In this session, walk through the Project Management Office (PMO) level configuration necessary to support your business needs.
Уголок поддержки Office 365: Управление Project Online. Office 365 Support Corner is an ongoing series of on-demand courses that cover leading Office 365 support issues. ​Understanding the administration options for Project Online is key for a successful experience, both from the tenant administration within Office 365 and the Project Management Office (PMO) administration for your projects. In this course, get insight into what you have control over within Office 365 Admin Center and the PMO administration within each Project Web App (PWA) site. 

Английский. Быстрое введение в модернизацию вашего ЦОД

With Windows Server 2003 reaching the end of support in less than a year, many IT Pros want to get ahead of the process. Explore modernization and datacenter transformation options, storage, networking, and the cloud, with Windows Server 2012 R2 and Microsoft Azure. Find out what you need to update your datacenter to match your workloads.

Watch as experts Matt Hester and Jennelle Crothers take a demo-rich look at administration tools, storage improvements, Hyper-V, and best practices for virtualizing domain controllers, plus how to simplify day-to-day server management with PowerShell and Desired State Configuration. Explore Microsoft Azure and how to make hybrid cloud a reality. And review the four major steps for planning a migration project.

Полезные ссылки

Попробовать Azure бесплатно на 30 дней! 


Центр разработки Microsoft Azure (azurehub.ru) – сценарии, руководства, примеры, рекомендации по разработке 
Twitter.com/windowsazure_ru — последние новости Microsoft Azure 
Сообществе Microsoft Azure на Facebook – эксперты, вопросы 


 
Изучить курсы виртуальной академии Microsoft по облачным и другим технологиям 


Загрузить бесплатную или пробную Visual Studio 


Стать разработчиком