   	Сегодня информационные технологии невозможно представить без грамотного управления. Программирование перестает быть «уделом избранных», привлекая все больше и больше людей к работе в этой области. Поэтому все важнее становится профессия менеджера IT-проектов, способного собрать команду первоклассных специалистов и в заданные сроки решить поставленную задачу. 

Что представляет собой проектный менеджмент в IT, какова личность современного руководителя проекта и что необходимо сделать, чтобы стать профессионалом в этой области? Об этом расскажет Сергей Архипенков craft_brother, директор департамента комплексных архитектурных решений ЗАО «Эр-Стайл», автор 5 книг, более 100 статей, докладов и курсов, председатель программного комитета конференции по вопросам управления IT-проектами SPM Conf – 4. 





1. Руководитель ИТ-проектов – это профессия, призвание или образ жизни?

Разумеется, руководитель проектов – это, прежде всего, профессия. Однако раньше человек сначала учился, чтобы хорошо работать, а потом старательно работал, чтобы счастливо жить. Сейчас стирается разница между учебой, работой и жизнью. Это один неразрывный процесс. Современное общество устроено так, что наивысших результатов добиваются люди, для которых главное не работа, карьера или учеба по отдельности, а призвание. «Быть», а не «обладать». Вот, на мой взгляд, главная жизненная цель современного успешного человека. Поэтому, если у кого-то нет призвания быть руководителем, то такой человек не многого добьется в этой профессии.

2. Как Вы сами стали руководителем проектов? Пожелали бы другим такой участи?

Думаю, что мне немного повезло. В НИИ, где я тогда работал молодым специалистом, открылась новая научная программа – имитационное моделирование больших и сложных систем, которое очень тесно связано с программированием. Видимо, я сумел себя до этого положительно зарекомендовать, и поэтому мне доверили руководить одним из направлений этой программы.
Один из авторитетов нашей отрасли, Том Демарко, как-то сказал: «Управление благородно по своей сути. Менеджмент ничего не имеет общего с деятельностью бюрократа, сидящего наверху. Менеджер делает самую необходимую в компании работу. Благодаря ему, становится возможным выполнение самых грандиозных проектов. Нет более почетной работы, чем та, которую делает настоящий менеджер». Я разделяю это мнение, чего и другим хочу пожелать.

3. Какие ключевые качества нужны, чтобы стать руководителем ИТ-проектов?
Вот небольшой набор основных навыков, которыми, на мой взгляд, должен обладать современный успешный руководитель ИТ-проектов:
• Видение целей и стратегии их достижения. 
• Глубокий анализ проблем и поиск новых возможностей.
• Нацеленность на успех, стремление получить наилучшие результаты. 
• Способность сочувствия, понимания состояния участников команды. 
• Искренность и открытость в общении. 
• Навыки в разрешении конфликтов. 
• Умение создавать творческую атмосферу и положительный микроклимат. 
• Терпимость, умение принимать людей какие они есть, принятие их права на собственное мнение и на ошибку. 
• Умение мотивировать правильное профессиональное поведение членов команды. 
• Стремление выявлять и реализовывать индивидуальные возможности для профессионального роста каждого. 
• Способность активно «обеспечивать», «доставать», «выбивать» и т.д.

4. С какими проблемами приходится сталкиваться современным руководителям IT-проектов?
Мой коллега, Слава Панкратов, как мне кажется, очень точно отметил, что основная проблема в управлении IT – это то, что здесь работают умные люди. 
Про другие сложности не готов ответить за всех руководителей. Думаю, что у каждого из них свой набор «граблей». Для меня главной проблемой, с которой я больно столкнулся в самом начале своего менеджерского пути, стал провал моей стройной модели типов личности. Согласно моей теории, должно было существовать только два типа личности: правильные – это те, которые думают и поступают так же, как я, и неправильные – все остальные, которых надо срочно переделывать в правильных. Но жизнь это поправила. Пришлось понять, что все люди разные и каждый человек уникален. Правда, до этого довелось вдоволь побегать по одним и тем же «граблям».

5. Каким должен быть идеальный руководитель IT-проекта?
Современные менеджеры должны быть и управленцами, и лидерами. Лидерство и управление одинаково важны, они не могут существовать в отрыве друг от друга. Нельзя быть лидером запасов, денежных потоков и затрат. Ими необходимо управлять. Потому что у вещей нет права и свободы выбора, которые присущи только человеку. С людьми нужно быть эффективными, с вещами — производительными. Нельзя ориентироваться на производительность в отношениях с людьми. Интеллектуальными людьми невозможно управлять. Творческие команды можно только направлять и вести.

6. Как достичь этого состояния?
Ну, во-первых, надо очень этого захотеть. А если есть мотивация, то дальше работает правило 10000 часов.

7. Нужен ли руководителю проектов помощник? Если да, то кто может им стать?
«Высший пилотаж» менеджмента — это делегирование полномочий и наблюдение за работой команды. Поэтому в идеале помощником руководителя должен стать каждый боец проектной команды. Правда, для этого руководителю придется изрядно потрудиться. Делегирование невозможно без взаимного доверия. Тот, кому делегировали руководство, становится сам себе боссом, ведомым собственной совестью в направлении достижения оговоренных желаемых результатов. Доверие требует времени и терпения, необходимости обучать и развивать людей, так, чтобы их способности смогли соответствовать этому доверию. Но сделав эти разовые инвестиции в человеческий капитал, мы потом долго сможем получать дивиденды. 

8. Какова роль PM-конференций в становлении профессионала?
Сегодня наши знания устаревают быстрее, чем мы успеваем отпраздновать окончание учебного заведения. Обучение – это процесс, который длится теперь всю жизнь. Хорошая конференция – это самые новые знания и опыт коллег. И, конечно, новые профессиональные контакты. Если вы столкнётесь с проблемой, вам всегда поможет «звонок другу».

9. На территории СНГ проходит не так много мероприятий, посвященных проектному менеджменту в IT. В чем особенность таких русскоязычных конференций по сравнению с западными?

Думаю, что наша конференция SPM Conf, которая посвящена исключительно проблемам управления проектами разработки ПО, уникальна. И в этом ее особенность. Погуглите «software project management conference». Гугл ничего не знает про другие подобные конференции ни в СНГ, ни в дальнем зарубежье.

10. Какой главный совет вы дадите участникам грядущей SPM Conf – 4?
Мой совет – будьте счастливы!
Формула для этого очень проста. Человек состоит из четырех компонентов: тело, сердце, разум и душа. Телу необходимы деньги и безопасность. Сердцу — любовь и признание. Разуму – развитие и самосовершенствование. Душе – самореализация. И все это можно найти в нашей отрасли. 
