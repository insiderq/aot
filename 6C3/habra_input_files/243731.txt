   	Суть проблемы:

 — Если на телефоне имелись приложения, подписанные self-signed cert, то в момент обновления на Lolipop эти приложения удаляются.
 — При попытке заново установить из Google Market вылезает ошибка INSTALL_FAILED_DUPLICATE_PERMISSION -505

В первую очередь это касается приложений на базе air: forums.adobe.com/message/6934105
