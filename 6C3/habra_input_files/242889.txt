   	EMET обновился до версии 5.1. В новой версии были исправлены проблемы совместимости пятой версии со сторонними приложениями, в т. ч., с MS Internet Explorer, Adobe Reader, Adobe Flash Player, и Mozilla Firefox. Исправлена несовместимость настройки EAF+ с Adobe Reader sandbox (Защищенный режим просмотра), которая приводила к ложным срабатываниям при открытии документов pdf, для которых должен был включаться режим защищенного просмотра. Кроме этого, теперь процессы и сервис EMET запускаются как 64-битные. Скачать новую версию можно по этой ссылке.



Детальная информация по настройкам EMET см. [1], [2], [3]. Полный список изменений см. ниже.

Основные улучшения

Enables the EMET service to log EMET configuration when the service is started.
Resolves a potential race condition in the Mandatory ASLR mitigation.
Addresses an issue in which some mitigations stop working if EAF is disabled.
Addresses errors that occur if EMET is not installed in the default folder.
Improves and hardens several mitigations to make them more resilient to attacks and bypasses.

Исправления проблем совместимости с приложениями

Addresses application compatibility issues between Certificate Trust and 64-bit Internet Explorer.
Addresses application compatibility issues between EAF+ and several applications, such as Internet Explorer, Adobe Reader, Adobe Flash, and Mozilla Firefox.
Addresses application compatibility issues that affect Internet Explorer Developer Tools and the Manage Add-ons feature.
Improves EAF mitigation to address several application compatibility issues.

Улучшения механизмов развертывания и конфигурации

Adds a default configuration for EAF+ for Google Chrome and Oracle Java 8.
Adds the «Local Telemetry» feature that allows you to save memory dumps locally when a mitigation is triggered.