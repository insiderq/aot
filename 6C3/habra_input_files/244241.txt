     Последняя часть истории об отзывчивых изображениях, которую мы начали здесь и продолжили тут, рассказывая о применении srcset и sizes. Сегодня речь пойдет об использовании тега  для обертывания изображений.

Вторая стадия: picture и режиссура

srcset для ленивых, picture для безумных™
Мэт Маркес

Итак, для изображений, которые просто нужно масштабировать, мы приводим список наших исходников и их ширину в пикселях в srcset, позволяем браузеру выбирать, какая ширина изображения будет отображаться с помощью sizes, и отпускаем наше безумное желание все контролировать. Но! Иногда нам захочется адаптировать наши изображения, выходя за рамки масштабирования. В таком случае, нам нужно вернуть небольшую часть контроля над подбором исходников. Вводим picture.

У наших детальных изображений большое соотношение сторон – 16:9. На больших экранах они выглядят отлично, но на телефоне они становятся крошечными. Простежка и вышивка, которые нужно показать на детальных изображения, слишком мелкие, чтобы их рассмотреть. 

Было бы неплохо, если бы мы могли «увеличивать» изображения на маленьких экранах, представляя их в более плотном и высоком виде. 


 
Такие вещи – подгон контента изображений под отдельные среды – называются «режиссурой». Каждый раз, когда мы обрезаем или иным образом изменяем изображение, чтобы оно соответствовало контрольной точке (вместо изменения размера всего), мы занимаемся режиссурой.

Если мы включаем увеличенные обрезанные изображения в srcset, неизвестно, где они подойдут, а где – нет. С помощью picture и source media мы можем сделать наши желания реальностью: загружать широкие прямоугольные кадры только когда окно шире 36 em. А в небольших окнах всегда загружать квадратные картинки. 

<picture>
	<!-- кадрирование 16:9 -->
	<source
		media="(min-width: 36em)"
		srcset="quilt_2/detail/large.jpg  1920w,
		        quilt_2/detail/medium.jpg  960w,
		        quilt_2/detail/small.jpg   480w" />

	<!-- квадратное кадрирование -->
	<source
		srcset="quilt_2/square/large.jpg  822w,
		        quilt_2/square/medium.jpg 640w,
		        quilt_2/square/small.jpg  320w" />
	<img
		src="quilt_2/detail/medium.jpg"
		alt="Detail of the above quilt, highlighting the embroidery and exotic stitchwork." />
</picture>

Элемент picture содержит любое количество исходных элементов и один img. Браузер просматривает все исходники изображений, пока не найдет атрибут media, который соответствует текущей среде. Он отправляет srcset подходящего исходника в img, который до сих пор остается тем элементом, который мы «видим» на странице. 

Вот более простой пример: 

<picture>
	<source media="(orientation: landscape)" srcset="landscape.jpg" />
	<img src="portrait.jpg" alt="A rad wolf." />
</picture>

В окнах с альбомной ориентацией в img подается landscape.jpg. При книжной ориентации (или если браузер не поддерживает picture) img не изменяется, и загружается portrait.jpg. 

Это поведение может показаться вам слегка удивительным, если вы привыкли к аудио и видео. В отличие от этих элементов, picture представляет собой невидимую обложку: волшебный span, который задает изображению значение srcset.
Еще один способ кадрирования: img – это не шаг назад. Мы прогрессивно улучшаем img, обертывая его в picture.
На практике это означает, что любые стили, которые мы хотим применить к нашему изображению на экране, необходимо настраивать с учетом img, а не picture. Код picture { width: 100% } ничего не делает. Код picture > img { width: 100% } делает то, что нам нужно. 

Вот наша страница лоскутных одеял с примененным шаблоном. Вспоминаем, что целью использования picture было предоставить пользователям с небольшими экранами больше (и более полезных) пикселей, и смотрим на то, как развивается производительность:



Неплохо! Мы отправляем немного больше байтов на 1х-экранах. Но по каким-то сложным причинам, имеющим отношение к размерам наших исходных изображений, мы фактически увеличили диапазон размеров экранов, которые ощущают рост производительности на 2х. Экономия на первой стадии изменения страницы остановилась на 480 пикселях для 2х-экранов, но после нашей второй стадии она расширилась вплоть до 700 пикселей. 

Теперь наша страница загружается быстрее и лучше выглядит на небольших устройствах. Но мы еще не закончили. 

Третья стадия: делаем несколько форматов с помощью source type

За 25-летнюю историю интернета в нем доминировали два растровых формата: JPEG и GIF. PNG потребовалось десять мучительных лет, чтобы вступить в этот эксклюзивный клуб. Новые форматы, такие как WebP и JPEG XR, уже стоят на пороге, обещая разработчикам превосходное сжатие и предлагая такие полезные функции, как альфа-каналы и режимы без потерь. Но ввиду одинокого атрибута изображений src, внедрение происходит очень медленно – разработчикам нужна практически универсальная поддержка формата, прежде чем они смогут его использовать. Но не сегодня. picture позволяет легко использовать несколько форматов, следуя той же модели source type, которая установлена для audio и video:

<picture>
	<source type="image/svg+xml" srcset="logo.svg" />
	<img src="logo.png" alt="RadWolf, Inc." />
</picture>

Если браузер поддерживает атрибут type исходника, он отправит srcset этого исходника в img.
Это довольно простой пример, но когда мы наслаиваем переключение source type поверх нашей существующей страницы лоскутных одеял чтобы, скажем, добавить поддержку WebP, все становится слишком сложным (и повторяющимся):

<picture>
	<!-- кадрирование 16:9 -->
	<source
		type="image/webp"
		media="(min-width: 36em)"
		srcset="quilt_2/detail/large.webp  1920w,
		        quilt_2/detail/medium.webp  960w,
		        quilt_2/detail/small.webp   480w" />
	<source
		media="(min-width: 36em)"
		srcset="quilt_2/detail/large.jpg  1920w,
		        quilt_2/detail/medium.jpg  960w,
		        quilt_2/detail/small.jpg   480w" />
	<!-- квадратное кадрирование -->
	<source
		type="image/webp"
		srcset="quilt_2/square/large.webp   822w,
		        quilt_2/square/medium.webp  640w,
		        quilt_2/square/small.webp   320w" />
	<source
		srcset="quilt_2/square/large.jpg   822w,
		        quilt_2/square/medium.jpg  640w,
		        quilt_2/square/small.jpg   320w" />
	<img
		src="quilt_2/detail/medium.jpg"
		alt="Detail of the above quilt, highlighting the embroidery and exotic stitchwork." />
</picture>


Получается слишком много кода для одного изображения. Кроме того, теперь у нас еще и большое количество файлов: целых 12! Три разрешения, два формата и два типа кадрирования на каждое изображение – это реально много. Все, чего мы добились в отношении производительности и функциональности, получается за счет предварительного столкновения со сложностями и дальнейшей возможности сопровождения. 

Автоматизация – ваш друг; если подразумевается, что ваша страница будет содержать массивные блоки кода, ссылающиеся на большое количество различных версий изображения, лучше не делать это все вручную. 

То же самое с пониманием, что хорошего должно быть понемногу. Я использовал все инструменты из спецификаций на нашем примере. Это почти никогда не будет рассудительным. Огромного увеличения эффективности можно добиться, используя любую из новых функций по отдельности, и вы должны тщательно рассмотреть все сложности их наслоения, прежде чем сохранять и делать все нужное и ненужное. 

Но все-таки, давайте посмотрим, что WebP может сделать для наших одеял.



Дополнительные 25-30% экономии свыше того, чего мы уже достигли – не только на нижнем пределе, но и по всему диапазону – это определенно не шутка! Моя методология здесь ни в коем случае не является точной; ваша производительность с WebP может отличаться. Суть в том, что новые форматы, которые обеспечивают значительную выгоду по сравнению с текущим положением JPEG/GIF/PNG, уже существуют и продолжают появляться. Атрибуты picture и source type снижают барьер доступа, навечно расчищая путь для инноваций в области форматов изображений. 

Используем size уже сегодня 

На протяжении многих лет мы знали, что отягощает наши отзывчивые страницы: изображения. Огромные изображения, созданные специально для огромных экранов, которые мы отправляли всем. Мы также знали, как решить эту проблему: отправлять различные исходники разным клиентам. Новая разметка позволяет нам делать именно это. srcset дает возможность предложить несколько версий изображения браузеру, который с помощью атрибута sizes подбирает из пачки наиболее подходящий исходник и загружает его. Атрибуты picture и source позволяют нам вмешаться и взять на себя немного больше контроля, гарантируя, что некоторые исходники будут выбраны, основываясь либо на медиа-запросах, либо на поддержке типа файла. 
