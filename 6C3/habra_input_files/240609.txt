		Быстрая настройка Windows
Установка операционной системы Windows стала обыденным делом для многих пользователей. Кто-то использует оригинальные образы, а кто-то экспериментирует со сборками — разницы особой нет. Если идти стандартным путём, а не развёртыванием из подготовленного образа, то процесс установки не должен вызвать проблем на исправном железе. После нескольких перезагрузок мы видим чистый рабочий — дальше нас ждёт настройка.

Можно долго и нудно настраивать различные параметры системы по разным аплетам Панели управления и др., ставя и снимая галочки, прописывая нужные параметры. Но есть и более быстрый путь — применить все необходимые настройки «в два клика» — через готовый файл настроек реестра. Про твики реестра Windows системные администраторы должны знать. Наверняка у многих есть свой набор настроек, собранных в *.reg файле. С помощью твиков можно не только быстро настроить «чистую» операционную систему, но и переиначить параметры рабочей машины без переустановки ОС.

Свой *.reg файл твиков реестра я составлял несколько лет. Основа была взята с форума OSZone, но много добавил и своего. Часть твиков была найдена опытным путём с помощью программы RegShot 1.8.2. Также кое-что было найдено в недрах документации Microsoft. Я буквально подряд проходил некоторые ветки реестра и искал информацию по неизвестным ключам настройки. Основная цель данных твиков — это повышение быстродействия, высвобождение памяти и создание удобства в работе.

Windows Registry Editor Version 5.00

;Добавить пункт "Копировать в папку..." в контекстное меню файлов
[HKEY_CLASSES_ROOT\AllFilesystemObjects\shellex\ContextMenuHandlers\CopyTo]
@="{C2FBB630-2971-11d1-A18C-00C04FD75D13}"

;Добавить пункт "Переместить в папку..." в контекстное меню файлов
[HKEY_CLASSES_ROOT\AllFilesystemObjects\shellex\ContextMenuHandlers\MoveTo]
@="{C2FBB631-2971-11d1-A18C-00C04FD75D13}"

[HKEY_CURRENT_USER\Control Panel\Desktop]
;Увеличить частоту мерцания курсора
"CursorBlinkRate"="200"
;Отключить сглаживание неровностей экранных шрифтов
"FontSmoothing"="0"
;Отключить сглаживание ClearType
"FontSmoothingType"=dword:00000000

;Дополнительные настройки окон
[HKEY_CURRENT_USER\Control Panel\Desktop\WindowMetrics]
;Показывать полностью имена длинных файлов в проводнике
"IconTitleWrap"="0"
;Отключить анимацию окон при свертывании и развертывании
"MinAnimate"="0"

[HKEY_CURRENT_USER\Control Panel\Keyboard]
;Убрать задержку повтора при зажатии клавиши
"KeyboardDelay"="0"

[HKEY_CURRENT_USER\Environment]
;Использовать %WinDir%\Temp как временную папку для всех пользователей
"TEMP"=-
"TMP"=-

[HKEY_CURRENT_USER\Keyboard Layout\Preload]
;Английский язык по умолчанию, а также добавление русского и украинского
"1"="00000409"
"2"="00000419"
"3"="00000422"

[HKEY_CURRENT_USER\Keyboard Layout\Toggle]
;Переключение между языками ввода Ctrl+Shift
"Hotkey"="2"
"Language Hotkey"="2"
"Layout Hotkey"="3"

[HKEY_CURRENT_USER\Software\Microsoft\FTP]
;Не разрешать представление FTP-узлов в виде папок
"Use Web Based FTP"="yes"

;Насторойки меню браузера Internet Explorer 8
[HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\CommandBar]
;Не отображать Командную строку
"CommandBarEnabled"=dword:00000000
;Не отображать кнопку совместимости
"ShowCompatibilityViewButton"=dword:00000000

;Параметры проверки цифровой подписи программ
[HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\Download]
;Не проверять цифровую подпись загружаемых программ
"CheckExeSignatures"="no"
;Запускать программы с непроверенной цифровой подписью
"RunInvalidSignatures"=dword:00000001

[HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\LinksBar]
;Не отображать Панель избранного
"Enabled"=dword:00000000

;Дополнительные настройки Internet Explorer 8
[HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\Main]
;Не выполнять поиск из адресной строки
"AutoSearch"=dword:00000000
;Не предлагать Internet Explorer использовать по умолчанию
"Check_Associations"="no"
;Запретить сторонние расширения обозревателя
"Enable Browser Extensions"="no"
;Время завершения настройки
"IE8RunOnceCompletionTime"=hex:00
;Браузер IE8 уже запускался
"IE8RunOnceLastShown"=dword:00000001
;Время пследнего запуска
"IE8RunOnceLastShown_TIMESTAMP"=hex:00
;Предварительная настройка IE8 выполнена
"IE8RunOncePerInstallCompleted"=dword:00000001
;Тур знакомства с IE8 пройден
"IE8TourShown"=dword:00000001
;Время показа тура знакомства с IE8
"IE8TourShownTime"=hex:00
;Отключить уведомление по окончании загрузки файла
"NotifyDownloadComplete"="no"
;Убрать эффект затемнения при переходе с одной страницы на другую
"Page_Transitions"=dword:00000000
;Пустая страница по умолчанию в Internet Explorer
"Start Page"="about:blank"

[HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_LOCALMACHINE_LOCKDOWN]
;Разрешить запуск активного содержимого файлов на моем компьютере
"iexplore.exe"=dword:00000000

[HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_LOCALMACHINE_LOCKDOWN\Settings]
;Разрешить запуск активного содержимого компакт-дисков на моем компьютере
"LOCALMACHINE_CD_UNLOCK"=dword:00000001

;Дополнительные настройки Windows Media Player 12
[HKEY_CURRENT_USER\Software\Microsoft\MediaPlayer\Preferences]
;Принять лицензионное соглашение
"AcceptedPrivacyStatement"=dword:00000001
;Не добавлять файлы видео, найденные в библиотеке изображений
"AddVideosFromPicturesLibrary"=dword:00000000
;Отключить автоматическое добавление музыки в библиотеку
"AutoAddMusicToLibrary"=dword:00000000
;Не удалять файлы с компьютера при удалении из библиотеки
"DeleteRemovesFromComputer"=dword:00000000
;Запретить автоматическую проверку лицензий защищённых файлов
"DisableLicenseRefresh"=dword:00000001
;Проигрыватель уже запускался
"FirstRun"=dword:00000000
;Не сохранять оценки в файлах мультимедиа
"FlushRatingsToFiles"=dword:00000000
;Не спрашивать о воспроизведении с веб-страницы
"HTMLViewAsk"=dword:00000000
;Не обновлять содержимое библиотеки при первом запуске
"LibraryHasBeenRun"=dword:00000001
;Не показывать сведения о мультимедиа из Интернета и не обновлять файлы
"MetadataRetrieval"=dword:00000000
;Отключить загрузку лицензий на использование файлов мультимедиа
"SilentAcquisition"=dword:00000000
;Не устанавливать автоматически часы на устройсвтах
"SilentDRMConfiguration"=dword:00000000
;Увеличить время запроса автоматического обновления проигрывателя
"UpgradeCheckFrequency"=dword:00000002

;Дополнительные настройки проводника
[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer]
;Отображать все значки в области уведомлений
"EnableAutoTray"=dword:00000000
;He дoбaвлять "- Яpлык" для coздaвaeмыx яpлыкoв
"link"=hex:00,00,00,00

;Расширенные настройки проводника и меню "Пуск"
[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced]
;Показывать скрытые файлы, папки и диски
"Hidden"=dword:00000001
;Показывать пустые диски в папке "Компьютер"
"HideDrivesWithNoMedia"=dword:00000000
;Показывать расширения для зарегистрированных типов файлов
"HideFileExt"=dword:00000000
;Мелкие значки в Главном меню
"Start_LargeMFUIcons"=dword:00000000
;Не выделять недавно установленные программы
"Start_NotifyNewApps"=dword:00000000
;Не отображать "Документы"
"Start_ShowMyDocs"=dword:00000000
;Не отображать "Музыка"
"Start_ShowMyMusic"=dword:00000000
;Не отображать "Изображения"
"Start_ShowMyPics"=dword:00000000
;Не отображать "Подключение к"
"Start_ShowNetConn"=dword:00000000
;Отображать команду "Выполнить"
"Start_ShowRun"=dword:00000001
;Не отображать "Программы по умолчанию"
"Start_ShowSetProgramAccessAndDefaults"=dword:00000000
;Отключить скольжение кнопок Панели задач
"TaskbarAnimations"=dword:00000000
;Не группировать кнопки на Панели задач
"TaskbarGlomLevel"=dword:00000002
;Использовать маленькие значки на Панели задач
"TaskbarSmallIcons"=dword:00000001

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\AutoComplete]
;Использовать встроенное автозаполнение
"Append Completion"="yes"

[-HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\EventHandlersDefaultSelection]
[-HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\AutoplayHandlers\UserChosenExecuteHandlers]
;Сбросить настройки Автозапуска для всех типов носителей

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\CIDOpen\Modules\GlobalSettings\Sizer]
;Скрыть Область переходов в диалоге "открыть"
"PageSpaceControlSizer"=hex:a0,00,00,00,00,00,00,00,00,00,00,00,9b,02,00,00

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\CIDSave\Modules\GlobalSettings\Sizer]
;Скрыть Область переходов в диалоге "сохранить"
"PageSpaceControlSizer"=hex:a0,00,00,00,00,00,00,00,00,00,00,00,9b,02,00,00

;Настройка Панели управления
[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel]
;Показывать крупные значки
"AllItemsIconView"=dword:00000000
;Показывать все элементы Панели управления
"StartupPage"=dword:00000001

;Параметры значков Рабочего стола
[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel]
;Отображать Компьютер на Рабочем столе
"{20D04FE0-3AEA-1069-A2D8-08002B30309D}"=dword:00000000
;Отображать %UserName% на Рабочем столе
"{59031a47-3f72-44a7-89c5-5595fe6b30ee}"=dword:00000000

;Параметры раскладки дополнительных панелей в папках
[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Modules\GlobalSettings\Sizer]
;Скрыть Область переходов в папках
"PageSpaceControlSizer"=hex:a0,00,00,00,00,00,00,00,00,00,00,00,56,05,00,00
;Скрыть область сведений в папках
"PreviewPaneSizer"=hex:35,00,00,00,00,00,00,00,00,00,00,00,9f,02,00,00

;Изъять все программы из Панели задач
[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Taskband]
;Не показывать ссылки на программы в Панели задач
"Favorites"=hex:ff
;Удалить записи о закреплённых программах в Панели задач
"FavoritesResolve"=-

;Настройка дополнительных параметров Интернета
[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings]
;Запретить встроенную проверку подлинности Windows
"EnableNegotiate"=dword:00000000
;Отключить предупреждение о несоответствии сертифиата
"WarnonBadCertRecving"=dword:00000000
;Отключить предупреждение о переходе в локальную зону
"WarnOnIntranet"=dword:00000000
;Отключить предупреждение о перенаправления публикаций
"WarnOnPostRedirect"=dword:00000000

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Associations]
;Список "безопасных" типов файлов для загрузки из интернета
"LowRiskFileTypes"=".zip;.rar;.nfo;.txt;.exe;.bat;.com;.cmd;.reg;.msi;.htm;.html;.gif;.bmp;.jpg;.avi;.mpg;.mpeg;.mov;.mp3;.m3u;.wav;"

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Attachments]
;Отключить предупреждение при открытии файлов, загруженных из Интернета
"SaveZoneInformation"=dword:00000001

[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer]  
;Отключить значок "Центр поддержки" из области уведомлений
"HideSCAHealth"=dword:00000001
;Отключить автозапуск всех устройств, кроме CD-привода
"NoDriveTypeAutoRun"=dword:000000dd
;Убрать ярлык "Программы по умолчанию" из меню Пуск
"NoSMConfigurePrograms"=dword:00000001

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Device Metadata]
;Заменять стандартные значки устройств улучшенными
"PreventDeviceMetadataFromNetwork"=dword:00000000

[-HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\RemoteComputer\NameSpace\{863aa9fd-42df-457b-8e4d-0de1b8015c60}]
;Не сканировать сеть на наличие сетевых принтеров

;Настройка контроля учётных записей
[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System]
;Выполнение программ Администратором без запроса
"ConsentPromptBehaviorAdmin"=dword:00000000
;Отключить контроль учётных записей (UAC)
"EnableLUA"=dword:00000000
;Не спрашивать при запуске приложений
"PromptOnSecureDesktop"=dword:00000000

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update]
;Отключить автоматическое обновление системы
"AUOptions"=dword:00000001

[HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\ScriptedDiagnosticsProvider\Policy]
;Не обращаться к средствам устранения неполадок через интернет
"EnableQueryRemoteServer"=dword:00000000

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Windows Error Reporting]
;Запретить отправку отчёта об ошибках
"Disabled"=dword:00000001

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore]
;Отключить восстановление системы на всех дисках
"RPSessionInterval"=dword:00000000

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Search\CrawlScopeManager\Windows\SystemIndex\WorkingSetRules\16]
;Не индексировать автономные файлы
"Include"=dword:00000000

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Search\CrawlScopeManager\Windows\SystemIndex\WorkingSetRules\17]
;Не индексировать историю IE8
"Include"=dword:00000000

;Поведение в случае отказа сисемы (BSOD)
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\CrashControl]
;Не выполнять автоматическую перезагрузку
"AutoReboot"=dword:00000000
;Не записывать отладочную информацию
"CrashDumpEnabled"=dword:00000000

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Network\NetworkLocationWizard]
;Скрыть мастер выбора типа сети и считать все сети общественными
"HideWizard"=dword:00000001

;Параметры Удалённого помощника
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Remote Assistance]
;Запретить удалённое управление этим компьютером
"fAllowFullControl"=dword:00000000
;Запретить подключения удалённого помощника к этому компьютеру
"fAllowToGetHelp"=dword:00000000

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\CSC]
;Отключить драйвер автономных файлов
"Start"=dword:00000004

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\CscService]
;Отключить службу автономных файлов
"Start"=dword:00000004

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\lanmanserver\Parameters]
;Отключить стандартные административные общие ресурсы (C$ и т.д.)
"AutoShareWks"=dword:00000000

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\MpsSvc]
;Отключить службу Брандмауэр Windows
"Start"=dword:00000004

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile]
;Отключить доменный профиль Брандмауэра Windows
"EnableFirewall"=dword:00000000

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\PublicProfile]
;Отключить общий профиль Брандмауэра Windows
"EnableFirewall"=dword:00000000

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile]
;Отключить частный профиль Брандмауэра Windows
"EnableFirewall"=dword:00000000

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\W32Time]
;Отключить службу времени Windows
"Start"=dword:00000004

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\W32Time\Parameters]
;Не выполнять синхронизацию с сервером времение в интернете
"Type"="NoSync"

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WinDefend]
;Отключить службу Защитник Windows
"Start"=dword:00000004

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\wscsvc]
;Отключить сдужбу Центр обеспечения безопасности
"Start"=dword:00000004

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\wuauserv]
;Отключить службу Центр обновления Windows
"Start"=dword:00000004


Как видите, довольно много пунктов, но все они содержат описание, так что каждый выберет то, что ему нужно. Для тех, кто не знает как воспользоваться: копируете в текстовый файл данные твики (можно не все, но обязательно с верхней строчкой) и сохраняете. После у текстового файла нужно поменять расширение с *.txt на *.reg, а также учтите, что некоторые твики требуют права Администратора.

Бонус: Перемещение пользовательских данных.

По-умолчанию пользовательские данные на компьютерах под управлением Windows 7 хранятся в папке C:\Users\%username%, что неудобно. Внутри папки пользователя есть несколько под-папок, которые за некоторое время у «неряшливых» пользователей значительно «разрастаются» в объёме. Вот эти папки: Рабочий стол, Загрузки, Документы, Музыка, Изображения, Видеозаписи. Я предлагаю сразу после переустановки Windows перенести эти папки на диск D:\. Конечно, можно это сделать и вручную — в свойствах каждой из папок изменить её местоположение. А можно воспользоваться моим командным файлом, который «бережно» скопирует всё содержимое на новое место и изменить нужные параметры реестра.

@ECHO OFF
TITLE Перемещение пользовательских данных
XCOPY "%USERPROFILE%\Мои документы\Моя музыка" D:\Аудио /S /I /Q /H /K /O /Y
XCOPY "%USERPROFILE%\Мои документы\Мои рисунки" D:\Фото /S /I /Q /H /K /O /Y
XCOPY "%USERPROFILE%\Мои документы\Мои видеозаписи" D:\Видео /S /I /Q /H /K /O /Y
MD D:\Аудио
MD D:\Фото
MD D:\Видео
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "My Music" /t REG_SZ /d D:\Аудио /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "My Pictures" /t REG_SZ /d D:\Фото /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "My Video" /t REG_SZ /d D:\Видео /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v "My Music" /t REG_EXPAND_SZ /d D:\Аудио /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v "My Pictures" /t REG_EXPAND_SZ /d D:\Фото /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v "My Video" /t REG_EXPAND_SZ /d D:\Видео /f
RD /S /Q "%USERPROFILE%\Мои документы\Моя музыка"
RD /S /Q "%USERPROFILE%\Мои документы\Мои рисунки"
RD /S /Q "%USERPROFILE%\Мои документы\Мои видеозаписи"
XCOPY "%USERPROFILE%\Рабочий стол" "D:\Рабочий стол" /S /I /Q /H /K /O /Y
XCOPY "%USERPROFILE%\Мои документы" "D:\Мои документы" /S /I /Q /H /K /O /Y
MD "D:\Рабочий стол"
MD "D:\Мои документы"
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v Desktop /t REG_SZ /d "D:\Рабочий стол" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v Desktop /t REG_EXPAND_SZ /d "D:\Рабочий стол" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v Personal /t REG_SZ /d "D:\Мои документы" /f
REG ADD "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v Personal /t REG_EXPAND_SZ /d "D:\Мои документы" /f
REG ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\DocFolderPaths" /v %USERNAME% /t REG_SZ /d "D:\Мои документы" /f
RD /S /Q "%USERPROFILE%\Рабочий стол"
RD /S /Q "%USERPROFILE%\Мои документы"
PAUSE


Как видите, всё довольно просто — перечисленные выше «важные» папки создаются в корне диска D:\. Это решение удобно применять для однопользовательского домашнего компьютера. Чтобы воспользоваться, нужно сохранить текст в файл с раширением *.cmd. Кодировка должна быть OEM 866.

