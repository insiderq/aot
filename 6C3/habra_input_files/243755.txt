   	Добрый вечер! Сегодня вперые говорим об анализе данных в нашем блоге. Для многих это крайне актуальная тема. Однако в Беларуси не так много действительно полезных встреч и конференций, посвященных аналитике.
25 октября 2014 года в Минске состоялся первый DataTalks. DataTalks – это неформальные встречи специалистов в области анализа данных. Для участников это отличная возможность узнать об опыте применения аналитики в компаниях, работающих на мировом и местном рынках, а также познакомиться со специалистами в области анализа данных из различных индустрий.

Докладчики из Wargaming, Yandex, Dmlabs.org и Нанотех, на примерах решаемых ими задач, объясняли общие закономерности и применимость алгоритмов, которые важно знать и использовать при анализе данных в любой индустрии. Вопросы спикерам перерастали в профессиональные дискуссии, остановить которые не могло даже начало следующего доклада.



Рассказать подробнее о том, как появилась идея создания DataTalks, мы попросили Сергея Кадомского, идейного вдохновителя сообщества аналитиков и директора по исследованиям Wargaming:
Идея организовать такое мероприятие появилась после того, как мы начали искать экспертов в области анализа данных в Беларуси для обмена опытом. Выяснилось, что таких людей мало и многие из них даже не догадываются о существовании друг друга. При этом опыт нашей компании свидетельствует о том, что анализ данных является не просто конкурентным преимуществом, а обязательным условием при создании продуктов и ведении бизнеса на международной арене. Я уверен, что наши мероприятия окажут благотворное влияние на профессиональное развитие аналитиков данных, и как результат – на рост эффективности работы белорусских компаний как на внутреннем рынке, так и на мировой арене.
Я очень рад тому, что при поддержке компании Wargaming нам удалось собрать вместе 200 специалистов, интересующихся и профессионально занимающихся анализом данных.
От лица компании хочу выразить благодарность докладчикам и всем участникам мероприятия. Мы планируем провести следующую встречу в феврале 2015, будем рады снова видеть вас!

Перейдем к выступлениям. Первым слово на DataTalks взял Михаил Левин.

Михаил Левин, руководитель группы анализа больших данных, Yandex Data Factory
«Как заработать миллионы на своих данных и машинном обучении».
Михаил провел небольшой экскурс в мир Яндекс.Директ. Это система размещения релевантной контекстной рекламы в каждом поисковом запросе, благодаря которой Яндекс получает основную прибыль. Он рассказал об основных принципах работы Директа и о том, как внедряя Machine Learning не ошибиться, проверяя эффективность новых алгоритмов на реальных пользователях.
Посмотреть и скачать презентацию Михаила можно тут.




Ксения Петрова, COO dmlabs.org
«Data mining на практике. Подводные камни анализа данных» 
Ксения, с точки зрения своего опыта, рассказала про распространенные ошибки, с которыми сталкиваются аналитики. Речь шла о том, как правильно строить процессы, чтобы избегать этих ошибок, и тем самым экономить время и деньги. Для этого исследователям стоит строго следить за качеством данных, изучать опыт других аналитиков, бесконечно улучшать модели и главное – помнить, что бизнес-задача всегда должна быть на первом месте.
Посмотреть и скачать презентацию Ксении можно тут.




Сергей Кадомский, директор по исследованиям Wargaming
«Как продать аналитику бизнесу»
Сергей рассказал, при помощи каких подходов может строиться аналитика в бизнесе в целом, и как это происходит конкретно в Wargaming. Речь шла об особенностях и принципах, которые стоит помнить аналитикам в работе и общении с бизнесом, чтобы получать результат устраивающий обе стороны.
Посмотреть и скачать презентацию Сергея можно тут.




Иван Мякишев и Алексей Юркевич, data scientist’ы Wargaming
«Компромиссы в Data Science»
Каждый специалист сталкивается с этим в профессиональной деятельности: в какой-то момент мы понимаем, что теоретические знания, полученные в ВУЗе и из специальной литературы не работают. Реальная жизнь – не примеры из онлайн-курса по Machine Learning. Иван и Алексей рассказали об основных компромиссах, на которые приходится идти бизнесу и аналитикам в совместной работе. Это компромиссы по скорости, точности и простоте. Важность этих компромиссов докладчики показали на примерах из своего опыта работы в Wargaming.
Посмотреть и скачать презентацию Ивана и Алексея можно тут.




Александр Гринчук, доцент ИБМТ БГУ, ООО Нанотех
«Практическое применение Data Mining технологий на примере реальных бизнес-задач в различных индустриях».
Александр рассказал о национальных особенностях белорусского рынка анализа данных. На примере реальных бизнес-задач он показал основные проблемы, с которыми сталкиваются специалисты при внедрении Data Mining в Беларуси. Это как недостаточно четкое понимание необходимости обработки, сохранения и анализа данных со стороны бизнеса, так и низкий уровень качества обработки, часто предлагаемый специалистами бизнесу.
Посмотреть и скачать презентацию Александра можно тут.




Круглый стол по актуальным вопросам анализа данных.
В конце для самых активных и заинтересованных специалистов был организован круглый стол. Главной темой обсуждения стали перспективы применения Data science в бизнесе Беларуси и России.
 

Надеемся, вас заинтересовал DataTalks. Присоединяйтесь к сообществу аналитиков на LinkedIn, где вы сможете обсудить интересные профессиональные кейсы и полезные материалы.