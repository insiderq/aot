   	Если у вас есть сайт или приложение, которые при регистрации нового пользователя просят подтвердить адрес электронной почты, у меня для вас есть совет и небольшая таблица с данными, которые помогут сделать процесс регистрации проще и удобнее для пользователя, а также позволят немного поднять процент конверсии посетителей в пользователи.

Что происходит после того, как пользователь указывает свой email в форме регистрации и отправляет форму на сервер?

Вариант первый, хороший: вы отправляете письмо и сразу же даете доступ к основным функциям вашего сайта или приложения, попутно напоминая пользователю о том, что неплохо бы перейти по ссылке из почты, чтобы убрать это назойливое напоминание и получить доступ ко всем функциям без исключения. Если адрес email не является для вас жизненно важным и вы можете дать частичный доступ без подтверждения почты, следует всегда делать именно так. Но эта схема применима не во всех случаях, поэтому…

Вариант второй, хуже: письмо отправляется, приложение показывает полупустую страницу с сообщением «проверьте вашу почту» и на этом стройная последовательность действий обрывается. Мне грустно смотреть на сайты, которые платят деньги Google или Яндексу за каждого привлеченного посетителя, корпят над landing страницам, стараясь выжать максимальный процент конверсии, максимально упрощают форму регистрации, но после отправки формы бросают пользователя на произвол судьбы.

Товарищи, регистрация ещё не закончена! Я понимаю, когда десяток лет назад все пользовались standalone почтовыми программами и у вас не было выбора, кроме как попросить пользователя запустить Outlook или The Bat!.. Но сейчас все пользуются веб-почтой. У веб-почты есть адрес URL и пользователь только что вам намекнул, какой именно веб-почтой он пользуется.

Постройте бесшовный процесс регистрации, проведите пользователя за руку через каждый этап, не упускайте его внимания ни на минуту. После отправки регистрационной формы покажите в центре экрана большую жирную ссылку на почтовый сервис пользователя. Не нужно просить юзера искать закладку или набирать «gmail.com» в адресной строке. Не давайте ему отвлечься. :)

Даже если слово «конверсия» для вас не имеет большого значения, такой хак имеет смысл внедрить просто потому, что это удобно и дарит пользователю приятное ощущение, что о нём заботятся.

Итак, как получить адрес и название почты, используя адрес email, который сообщил вам пользователь? Ловите табличку в формате CSV:

"почтовый домен", "название почтового сервиса","адрес для входа в почту"
"mail.ru","Почта Mail.Ru","https://e.mail.ru/"
"bk.ru","Почта Mail.Ru (bk.ru)","https://e.mail.ru/"
"list.ru","Почта Mail.Ru (list.ru)","https://e.mail.ru/"
"inbox.ru","Почта Mail.Ru (inbox.ru)","https://e.mail.ru/"
"yandex.ru","Яндекс.Почта","https://mail.yandex.ru/"
"ya.ru","Яндекс.Почта","https://mail.yandex.ru/"
"yandex.ua","Яндекс.Почта","https://mail.yandex.ua/"
"yandex.by","Яндекс.Почта","https://mail.yandex.by/"
"yandex.kz","Яндекс.Почта","https://mail.yandex.kz/"
"yandex.com","Yandex.Mail","https://mail.yandex.com/"
"gmail.com","Gmail","https://mail.google.com/"
"googlemail.com","Gmail","https://mail.google.com/"
"outlook.com","Outlook.com","https://mail.live.com/"
"hotmail.com","Outlook.com (Hotmail)","https://mail.live.com/"
"live.ru","Outlook.com (live.ru)","https://mail.live.com/"
"live.com","Outlook.com (live.com)","https://mail.live.com/"
"me.com","iCloud Mail","https://www.icloud.com/"
"icloud.com","iCloud Mail","https://www.icloud.com/"
"rambler.ru","Рамблер-Почта","https://mail.rambler.ru/"
"yahoo.com","Yahoo! Mail","https://mail.yahoo.com/"
"ukr.net","Почта ukr.net","https://mail.ukr.net/"
"i.ua","Почта I.UA","http://mail.i.ua/"
"bigmir.net","Почта Bigmir.net","http://mail.bigmir.net/"
"tut.by","Почта tut.by","https://mail.tut.by/"
"inbox.lv","Inbox.lv","https://www.inbox.lv/"
"mail.kz","Почта mail.kz","http://mail.kz/"Разумеется, это далеко не все почтовые службы. Рекомендую заглянуть в список почтовых адресов ваших пользователей и добавить именно те сервисы, которыми они пользуются.

Создать таблицу и импортировать CSV-файл в MySQL можно следующей командой:
CREATE TABLE email_services(domain CHAR(24) NOT NULL,name CHAR(32) NOT NULL,url CHAR(64) NOT NULL);
LOAD DATA LOCAL INFILE '/home/user/services.csv' INTO TABLE email_services FIELDS TERMINATED BY ',' ENCLOSED BY '"';
А дальше вы просто выделяете из адреса email почтовый домен, по нему находите в таблице название и адрес почтового сервиса, и показываете их пользователю.

Вы можете написать название сервиса, можете просклонять названия и сделать красивую надпись «Перейти в Яндекс.Почту» или просто показать сам email. Главное, чтобы там была ссылка на его «Входящие».

Посмотреть этот простой хак в работе можно, например, на нашем сервисе для тестирования знаний программистов CrowdTest, для которого эта таблица и создавалась:

