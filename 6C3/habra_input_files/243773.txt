		Я хочу рассказать про мой опыт развертывания ASP.NET vNext под Ubuntu 14.04. Сам я долгое время разрабатываю под Windows и дела с Unix системами если и имел, то вскользь и незаметно для меня и для Unix систем. Но, несмотря на это, новости о переводе части .Net в OpenSource с поддержкой Unix меня порадовали как повод для дальнейшего развития и расширения границ знаний.

В результате недолгих мучений и исследований различных форумов и issue трекеров мне удалось то, что раньше казалось немыслимым. Код написанный на C#, который запускается в среде придуманной и разработанной в MS, который представляет из себя ASP.NET MVC приложение (!) заработал.
 
Для начала нужно скачать и установить саму операционку. Я брал образ с официального сайта.

http://www.ubuntu.com/download/desktop/contribute/?version=14.04.1&architecture=amd64


Далее этот образ я развернул на VirtualBox, поскольку это был просто эксперимент. 

Чтобы запустить приложение на ASP.NET vNext под Ubuntu нам необходимо две вещи: проект оформленный в новом стиле ASP.NET vNext и развернутый и запущенный KRuntime с возможностью поднятия веб сервера Kestrel.

Создание приложения
Первая задача решается просто. Можно взять готовый очень простой пример с гитхаба:

https://github.com/aspnet/Home/tree/master/samples/HelloWeb.

Если гит еще не установлен, устанавливаем его:

sudo apt-get install git

Затем клонируем себе репозиторий с примерами:

git clone https://github.com/aspnet/Home.git


Инфраструктура
Вторая задача посложнее, но тоже решаема. Процесс состоит из следующих этапов:


Установка Mono
wget http://download.mono-project.com/repo/xamarin.gpg
sudo apt-key add xamarin.gpg
echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee --append /etc/apt/sources.list.d/mono-xamarin.list
sudo apt-get update
sudo apt-get install mono-complete
 
 
Установка KRuntime
 Разрешаем kvm лазить на свои сайты по своим сертификатам.
sudo certmgr -ssl -m https://go.microsoft.com
sudo certmgr -ssl -m https://nugetgallery.blob.core.windows.net
sudo certmgr -ssl -m https://nuget.org
mozroots --import --sync
 
Скачиваем скрипт установки kvm и запускаем его, после чего запускаем kvm upgrade. Теперь все, что нужно, у нас уже установлено:

curl https://raw.githubusercontent.com/aspnet/Home/master/kvminstall.sh | sh && source ~/.kre/kvm/kvm.sh && kvm upgrade

Вытягиваем все зависимости KRuntime:

kpm restore


Установка Bower
Если не установлен NPM, устанавливаем:

sudo apt-get install npm


Запуск Web-сервера
Компилим libuv, и (жесткий хак), копируем сборку в папки KRuntime, поскольку там лежат по умолчанию несовместимые версии, поскольку MS еще не работали над поддержкой Linux.

На данный момент KRuntime необходима версия 1.0.0-rc1 и сам KRuntime находится в версии 1.0.0-beta1. Все это очень быстро меняется, поэтому всегда необходимо знать какие версии линковались между собой последний раз. (Скрипт был взят и немного поправлен отсюда).

wget http://dist.libuv.org/dist/v1.0.0-rc1/libuv-v1.0.0-rc1.tar.gz 
tar -xvf libuv-v1.0.0-rc1.tar.gz
cd libuv-v1.0.0-rc1/
./gyp_uv.py -f make -Duv_library=shared_library
make -C out
sudo cp out/Debug/lib.target/libuv.so /usr/lib/libuv.so.1.0.0-rc1
sudo cp out/Debug/lib.target/libuv.so ~/.kpm/packages/Microsoft.AspNet.Server.Kestrel/1.0.0-beta1/native/darwin/universal/libuv.dylib
sudo ln -s libuv.so.1.0.0-rc1 /usr/lib/libuv.so.1

Вся работа с KRuntime выполняется над приложением, соответственно мы должны находится в директории с проектом, где лежит project.json. В нашем случае делаем:

cd Home/samples/HelloWeb/


Запускаем наше приложение на сервере kestrel (все настройки в файле project.json):

k kestrel

Открываем браузер и переходим по адресу:

http://localhost:5004

Ну или другой адрес указанный в project.json рядом с kestrel:

