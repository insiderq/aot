   	Обзор HP ProLiant DL360 Gen9
С появлением процессоров семейства Xeon E5-2600 v3 от Intel, многие поставщики стали выпускать вычислительные платформы, использующие преимущества повышенной производительности новой архитектуры Xeon. DL360 Gen9 является новой серийной вычислительной платформой с форм-фактором 1U в семействе HP ProLiant, и в дополнение к новым процессорам E5-2600 v3 и памяти DDR4 DL360 Gen9 отражает технологические достижения компании HP и общее видение компании по поводу конвергированных центров обработки данных.



Как и более крупная платформа DL380 Gen9 с форм-фактором 2U, DL360 увеличивает производительность процессоров Intel Xeon E5-2600 v3. В сочетании с модулями памяти HP SmartMemory DDR4 емкость сервера может быть увеличена до 768 Гбайт, скорость до 2 133 МГц, в отличие от максимальной скорости 1 866 МГц у процессоров предыдущего поколения Gen 8 с памятью DDR3. Новые процессоры E5-2600 v3 имеют до 18 ядер и позволяют увеличить производительность на Ватт в три раза по сравнению с серверами Gen8 ProLiant. Для серверов DL360 подходят контроллеры последнего поколения Smart Array Controller со скоростью 12 Гб/с и ускорители PCIe. В отличие от своих предшественников DL360 Gen9 поддерживают графические процессоры: до двух активных карт одинарной ширины длиной до 9,5 дюймов.

Устройство, тестируемое в данном обзоре, использует шасси DL360 с форм-фактором 1U с 8 отсеками под дисковод малого форм-фактора, оснащено двумя процессорами Intel Xeon E5-2697 v3 2.6GHz, 256 Гб оперативной памяти (16 слотов DIMM по 16 Гб 2Rx4 PC4-2133) и пятью твердотельными накопителями SAS малого форм-фактора на 400 Гб. В тестируемом устройстве не присутствует универсальный отсек Media Bay, но он по желанию может использоваться для размещения двух дополнительных отсеков под дисковод малого форм-фактора или накопителя на оптических дисках и VGA-разъема с доступом с лицевой стороны. Мы используем двухразъёмный приемопередатчик типа PCA FLR 10GbE SFP+интерфейсную плату для сетевого соединения.


 
Технические характеристики HP ProLiant DL360 Gen9:

Процессоры: семейство продуктов Intel Xeon E5-2600 v3
Количество процессоров: 2
Количество ядер процессора: 4, 6, 8, 10, 12, 14, 16 или 18
Форм-фактор: 1U
Блок питания: (2) Flex Slot
Слоты расширения: (3) максимум. 
Максимальный объем памяти: 768 ГБ DDR4 HP SmartMemory
Слоты для памяти: 24 слота DIMM до 2 133МГц
Накопители: (8) SAS/SATA дисков малого форм-фактора + универсальный отсек Media Bay или (4) диска SAS/SATA большого форм-фактора
Сеть: 4 x 1GbE + слот FlexibleLOM 
Сетевой контроллер: Broadcom 5720 и/или адаптер FlexFabric 10 Гбит 533FLR-T.
контроллер Dynamic Smart Array B140i
адаптер главной шины H240ar
контроллер Smart Array P440ar/2 Гбайт FBWC
Flash-backed write cache (FBWC): 2Гб DDR3-1,866MГц, шина шириной 72 бита 14,9Гб/с на P440ar
Батарея: HP DL/ML/SL 96Вт Smart Storage Battery
VGA-порт с задней стороны, порт последовательного ввода-вывода, 5x USB-портов 3.0
Поддержка графических процессоров: две активных карты одинарной ширины длиной до 9,5 дюймов до 150Вт каждая. 
Локальное управление: HP OneView и HP iLO Advanced
Управление облаком: HP Insight Online с мобильным приложением
Подача электроэнергии и охлаждение: до 94% эффективности (Platinum Plus) с HP Flexible Slot FF
Оперативно подключаемые вентиляторы с избыточностью N+1, дополнительные высокоэффективные вентиляторы
Соответствие требованиям производства: ASHRAE A3 и A4, энергосберегающий режим ожидания
Форм-фактор / глубина шасси: стойка (1U), 27.5'' (малый форм-фактор), 29.5'' (большой форм-фактор)
Microsoft Windows Server: 2008 R2 (только x64), 2012, 2012 R2
Red Hat Enterprise Linux 6.5 и более поздние версии
SUSE Linux Enterprise Server 11 SP3 и более поздние версии
Ubuntu Server 14.04 и более поздние версии
VMware vSphere 5.1 U2 и более поздние версии
Citrix XenServer
Solaris 11.1 и более поздние версии
Гарантия: 3/3/3


Разработка и создание
В рассматриваемом устройстве используется шасси с восемью доступными с лицевой стороны отсеками для дисковода малого форм-фактора, а также в правом верхнем углу отсутствует универсальный отсек Media Bay, который можно использовать для двух дополнительных драйверов малого форм-фактора или сочетания накопителя оптических дисков и VGA-порта с доступом с лицевой стороны. Панель состояния с кнопкой питания и светодиодными индикаторами для отображения состояния, работоспособности системы, ID устройства и сетевого статуса находится справа рядом с одним USB-портом. Также доступны шасси DL360 Gen9 с четырьмя отсеками для дисковода большого форм-фактора.


 
Вентиляционная сеть обеспечивает охлаждение с лицевой стороны системы. В нашей конфигурации используются два процессора Intel Xeon E5-2697 v3 2.6GHz и 16 из 24 DIMM-слотов. В системе используется форм-фактор DIMM16GB 2Rx4 PC4-2133 DDR4 с общим количеством DDR4-памяти 256 Гб. Слот для microSD находится между блоками питания и слотами DIMM. Вторичная переходная плата PCIe с третьим PCIe-слотом может использоваться для конфигураций сервера DL360 с двойным процессором, как в нашем случае, слот FlexibleLOM и два SATA-порта также расположены с задней стороны. 


 
Серверы DL360 Gen9 оборудованы одной стандартной переходной платой PCIe с двумя слотами PCIe, в то время как дополнительная переходная плата предоставляет третий слот PCIe для конфигураций с двойным центральным процессором. Задняя часть сервера включает порт VGA, встроенный 1GbE сетевой адаптер с четырьмя портами, порт управления iLO, последовательный порт, два порта USB 3.0, светодиодный ID устройства и доступ к карте FlexibleLOM, в данном случае в конфигурации с двухразъёмным приемопередатчиком типа PCA FLR 10GbE SFP и сетевым интерфейсом.
Наш сервер оснащен двойными оперативно заменяемыми блоками питания Flexible Slot Platinum на 800Вт с эффективностью 94%. Дизайн блоков питания HP Flex Slot позволяет замену в горячем режиме, а также не требует инструментов для установки в серверы HP ProLiant Gen9. В настоящее время доступны модели на 500Вт, 800Вт и 1400Вт.

Управление
Серверы HP ProLiant Gen9 разработаны для введения в эксплуатацию при помощи единого расширяемого микропрограммного интерфейса (UEFI), но допускает также и режим загрузки старого образца.

HP Integrated Lights-Out (iLO) – интеллектуальная система управления HP, встроенная в серверы HP для безагентского управления. Agentless Management осуществляет связь при помощи Direct Media Interface (DMI), а также может использоваться в сочетании с Intelligent Provisioning для развертывания и обновления серверов без конфигурации среды с повторно используемыми профилями развертывания. Новые резервные характеристики ProLiant Gen9 включают возможность доступа к 1 ТБ виртуального хранилища HP StoreVirtual без доплаты и новому скрипту для автоматической установки сервера.


 
Стандартные возможности iLO включают Agentless Management, Active Health System, Embedded Remote support и новый уровень абстракции Adaptive ProLiant Management Layer (APML) для улучшения температурного уровня с целью обновления данных работоспособности системы без перепрограммирования системного ПЗУ и неразрушающих онлайн-обновлений температурных и вентиляционных данных. На новых серверах ProLiant Gen9 добавлена функция iLO Federation, позволяющая обнаруживать и управлять многочисленными серверами, а также предоставляющая доступ к 1ТБ пространства для дополнительного использования хранилища, когда к серверу подключено 4 ГБ iLO NAND. Доступны различные схемы лицензирования iLO, которые предоставляют различные аспекты функциональности iLO Federation.


 
HP Active Health System (AHS) является интегрированным компонентом HP iLO Management для самодиагностики. AHS контролирует работоспособность сервера, конфигурацию и телеметрию в реальном времени на iLO 4, системном ПЗУ, сложных программируемых логических устройствах (CPLDs), умных массивах, BladeSystem Onboard Administrator, Agentless Management Service и платах сетевого интерфейса. Это обеспечивает синхронизированный контроль и сбор данных по всем системам для ускоренного выявления проблем и может автоматически отсылаться для анализа HP через прямое соединение Insight Online или Insight Remote Support 7.x.



Мобильное приложение iLO для iOS и Android может взаимодействовать непосредственно с процессором iLO на серверах ProLiant для обеспечения доступа к состоянию и протоколам системы, а также скриптам и виртуальной среде. Новый инструмент HP RESTful для серверов HP ProLiant Gen9 использует API, способный создать конфигурацию для быстрого развертывания серверов ProLiant, а также конфигурирует серверы с разнородными операционными системами. Серверы HP ProLiant Gen9 также поддерживают управление через HP Systems Insight Manager 7.4 и HP Virtual Connect Enterprise Manager 7.4.


 
Серверы HP Gen9 в настоящее время поставляются с программным обеспечением Insight Control. HP Insight Control включает автоматизированное развертывание сервера и инструменты перемещения наряду с интеграцией управления виртуальной машиной с VMware ESX, Microsoft Hyper-V, Citrix XenServer и Xen на Linux VMs. Инструменты управления производительностью обнаруживают и анализируют проблемы конфигурации аппаратных средств и факторы, сдерживающие производительность.

Insight Control включает централизованные инструменты контроля и управления для энергопотребления сервера и теплоотдачи. Доступ к HP Insight Control осуществляется через Systems Insight Manager (HP SIM) через сетевой графический интерфейс пользователя. HP Insight Control запускается на центральном сервере управления (CMS) на базе Windows и может управлять узлами сети Linux и Windows. Функции удаленного управления включают графический удаленный доступ (Virtual KVM), командное сотрудничество, загрузку сервера и видеоматериалы ошибки, видео отчет и воспроизведение по требованию, и удаленный доступ к виртуальной среде.

Лицензионные ключи для HP OneView и HP Insight Control предоставляются в одной системе для клиентов, планирующих перейти на OneView, как только это станет возможно в 2014 году. Услуга OneView разработана, чтобы объединить управление HP серверами, хранением и организацией сети, предложив определенные программным обеспечением шаблоны, централизованный центр автоматизации и другие особенности, разработанные для основы перехода к Инфраструктуре как услуге (IaaS) и архитектуре гибридного облака.

HP OneView может быть объединена с VMware vCenter, Microsoft System Center, Red Hat Enterprise Virtualization (RHEV), HP Universal Configuration Management Database (UCMDB) и HP Operations Orchestration. HP Insight Online поставляется бесплатно с гарантией HP и обслуживается по договору. Insight Online это управляемый на базе облака и поддерживаемый портал с панелью наблюдения, который отслеживает сервисные события, отображает конфигурации устройства и контролирует договоры и гарантии HP. Приложение HP Support Center обеспечивает доступ к данным Insight Online для мобильных устройств.

HP Insight Online Direct Connect — предложение удаленного обслуживания, ориентированное на малые и средние компании, позволяет серверам ProLiant Gen8/9 и блейд-системам c-класса BladeSystem передавать информацию об отказе аппаратных средств и диагностики непосредственно для анализа, генерации сценариев и автоматизированной замены частей.

HP Insight Remote Support использует размещение на локальных серверах для сбора данных и их отправки в HP. Услуга разработана IT-cреды с 2 500 устройствами максимум. Insight Remote Support может контролировать предшественников серверов Gen8 HP ProLiant, а также HP продукты для хранения данных и сети. Функция Insight Remote Support доступна как через локальную консоль, так и через приборную панель онлайн.

Условия тестирования
Мы публикуем информацию о ресурсах лаборатории, сетевых возможностях лаборатории и другие подробности нашего тестирования для того, чтобы администраторы и ответственные за приобретение оборудования лица могли справедливо оценить условия, при которых мы достигли данных результатов. Чтобы поддержать независимость тестирования, ни за один из наших обзоров не финансируется или управляется производителем оборудования, которое мы проверяем.

Наши первые результаты относятся к серверу HP ProLiant DL360 Gen9 в базовой комплектации с массивом RAID10, использующим четыре установленных твердотельных накопителя и пятый в качестве загрузочного диска. Второй набор результатов относится к производительности FlexibleLOM PCA FLR 10GbE SFP сервера DL360 + платы сетевого интерфейса, соединенных с массивом all flash array, чтобы продемонстрировать возможности сетевого хранения.

Анализ имитируемой нагрузки предприятия
Для проведения каждой оценки мы предварительно перевели устройство в устойчивое состояние со значительной нагрузкой в 16 потоков с 16 очередями в потоке. Затем хранилище будет проверяться с множеством вариаций потоков и глубины очередей для выявления результативности работы при малой и большой нагрузке.

Предварительная подготовка и первые проверки в стабильном состоянии: 

Пропускная способность (суммарное число операций ввода-вывода в секунду (IOPS) при выполнении чтения и записи)
Среднее время задержки (среднее время задержки при выполнении чтения и записи)
Максимальное время задержки (максимальное время задержки при выполнении чтения и записи)
Стандартное отклонение времени задержки (стандартное отклонение времени задержки при выполнении чтения и записи)


Этот анализ включает четыре профиля, широко используемых в спецификациях производителей и сравнительных тестах:

4k — 100% чтение или 100% запись
8k — 100% чтение или 100% запись
8k -70% чтение 30% запись
128k — 100% чтение или 100% запись

При рабочих нагрузках, составленных из произвольных операций 4k, ProLiant DL360 Gen9, достигает 244 058 операций ввода/вывода в секунду при чтении и только 41 021 операций ввода/вывода в секунду при записи, используя внутренний массив RAID10. В сочетании с массивом all flash array плата DL360 10GbE способна выдерживать 253 984 операций ввода/вывода в секунду при чтении и 300 743IOPS операций ввода/вывода в секунду при записи.


 
Среднее время задержки внутреннего массива RAID10 составило всего 1,05 мс для чтения и 6,24 мс для записи. Среднее время задержки интерфейса 10GbE с массивом all flash array составило 10.1 мс для чтения и 0.85 мс для записи.


 
Максимальное время задержки DL360 Gen9 составляет 86,6 мс при чтении и 112,3 мс при записи. Максимальное время задержки сетевого интерфейса DL360 10GbE с массивом all flash array составило 60,9 мс при чтении и 28,4 мс при записи.


 
Результаты стандартного отклонения свидетельствуют о наличии больших изменений в результатах времени ожидания для операций записи 4k на диске RAID10 SSD, что согласуется с результатами, показанными выше.


 
После изменения условий сервера и массива all flash array для последовательной передачи данных 8k мы измерили пропускную способность при нагрузке в 16 потоков и с 16 очередями в потоке для операций 100% чтения и 100% записи. Результаты внутреннего хранилища Gen9 DL360 RAID10 составили 147 178 операций ввода-вывода в секунду для последовательных операций чтения и 36 786 операций ввода-вывода в секунду для операций записи. Сетевой интерфейс 10GbE с массивом all flash array показали 159 158 операций ввода-вывода в секунду для операций чтения и 217 183 операций ввода-вывода в секунду для операций записи.


 
Следующие результаты получены на основании протокола, составленного из произвольных операций чтения 70% и операций записи 30% 8k с различной нагрузкой потоков и очередей. Никакие сочетания потоков и очередей не создали для сервера DL360 Gen9 с массивом RAID10 неожиданных сложностей. Пропускная способность массива RAID10 приближается к своему максимуму в 75 147 операций ввода-вывода в секунду при 16 потоках с 8 очередями в потоке. Пропускная способность интерфейса 10GbE к массиву all flash array при 16 потоках и 16 очередях в потоке достигает 218 801 операций ввода-вывода в секунду.


 
Во время первых нескольких циклов рабочей нагрузки 8k в сравнительном тесте 70/30, конфигурация массива 10GbE all flash array сохраняла среднее время задержки на теоретическом минимуме приблизительно в 0,23 мс. Ни одна конфигурация не проявила слабых мест с точки зрения среднего времени задержки.


 
Массив RAID10 испытал несколько заметных скачков времени задержки при большой глубине очереди. Во время самой тяжелой нагрузки 8k 70/30 сетевого интерфейса произошел скачок времени задержки, в остальное время максимальная задержка была последовательной.


 
Стандартное отклонение времени задержки при сравнительном тестировании 8k 70/30 также отражает максимальное время задержки в конфигурации RAID10 с многочисленными очередями. Результаты стандартного отклонения показывают, что массив RAID10 испытывает большинство затруднений последовательного времени задержки при максимальной нагрузке.


 
Наше заключительное тестирование основано на последовательной передаче 128k с операциями чтения 100% и записи 100%. Внутренний массив Gen9 ProLiant DL360 RAID10 в состоянии выдержать 1 678 541 Кб/с для операций чтения и 637 581 Кб/с для операций записи. При оценке дополнительного двойного сетевого интерфейса 10GbE Gen9 DL360 результат составил 2 311 475 кБ/с для операций чтения и 2 310 042 Кб/с для операций записи.


 
Заключение
У HP ProLiant DL360 Gen9 есть разработка, которая может служить полезным компонентом в конвергированной архитектуре предприятия или информационного центра вместе с платформой OneView HP и другими инновациями компании в управлении, что позволяет легко координировать работу сотен и тысяч серверов. Для тех, у кого более скромные цели DL360 Gen9 предлагает плотность хранения и вычислительную мощность в качестве гибкого и эффективного дополнения к множеству сред. В семействе DL300 компания HP охватила основные достижения в технологиях, включая поддержку новейшего чипсета Intel, DDR4 DRAM, 10GbE и SAS3. В нашем случае сервер соединен с 2,5-дюймовыми твердотельными накопителями, хотя сервер DL360 мог использоваться с четырьмя 3,5-дюймовыми накопителями в зависимости от предпочтений в работе. В целом, в семействе DL300 нет ничего революционного, но есть причина, по которой компания HP продает больше серверов, чем кто-либо еще, ведь она продолжает предоставлять функционал, который решает проблемы подавляющей части рынка.

В нашей конфигурации с пятью «устаревшими» твердотельными накопителями на базе малого логического котроллера в интерфейсе SAS2 на базе SanDisk Pliant мы получили ожидаемо стабильную работу. В дополнение к высоким показателям внутреннего хранилища, доступность высокоэффективных 10GbE интерфейсов означает, что DL360 Gen9 можно поставить выше быстродействующих массивов хранения с целью проведения большого количества операций ввода-вывода с минимальной задержкой и меньшим количеством кабелей. Во многих случаях нагрузка против хранения окажется наиболее популярным сценарием использования. В своем тестировании мы соединили сервер DL360 Gen9 с массивом all flash array на базе Windows Server 2012 R2 и добились показателей, которые легко превосходят двойное соединение 10GbE. Мы применили значительную нагрузку смешанных операций ввода/вывода, с максимальной нагрузкой в 218 000 произвольных операций ввода/вывода в секунду 8k 70/30, а также 2,3Гб/с последовательных операций. Так как с этим справлялся внутренний слот FlexibleLOM, мы смогли оставить все дополнительные слоты PCIe свободными для будущего использования.

Обновляемое семейство ProLiant Gen9 продолжает быть самым надежным среди серверов. Компания HP взяла все новые разработки и применила их в сервере. В то время как мы заметили, что другие производители экспериментируют с плотностью хранения в форм-фаторах 1U и 2U, компания HP играет по-крупному, предоставляя покупателям в точности то, что нужно. В то время как другие применяют новую технологию лишь в некоторых продуктах, позиция компании HP более прагматична; когда существует достаточно большой спрос на новую технологию, они ее вводят. На данный момент они полагаются на хорошую репутацию качества сборки, управления и структуры издержек, которые выдвигают их в авангард производителей серверов.

Преимущества

Возможность двухпортовой сети 10GbE обеспечивает высокоскоростное сетевое соединение высокоэффективных массивов хранения.
Широкий диапазон инструментов управления внутри и снаружи экосистемы управления HP
Улучшенные возможности хранения, включая поддержку твердотельных накопителей SAS3


Недостатки

Для долгосрочных некритических обновлений программного обеспечения/BIOS необходим договор обслуживания


Итог
HP ProLiant DL360 Gen9 предлагает центры обработки данных, которые управляются компактными вычислительными серверами с форм-фактором 1U, основывающимися на происхождении сервера HP и обеспечивающих преимущество в производительности и мощности по сравнению с предыдущим семейством Gen8.

Товарищи прошу не судить строго, пост публиковал ночью и забыл оформить его как перевод.
Оригинал статьи лежит тут: www.storagereview.com/hp_proliant_dl360_gen9_review
