      Спустя семь лет с момента запуска iPhone количество пользователей смартфонов в США достигло отметки в 70% от всего населения страны. Смартфоны существовали и до появления iPhone, поэтому данной категории товаров явно больше семи лет. Однако, учитывая количество текущих пользователей, уже сейчас можно сказать, что это самый быстрый процесс принятия населением новой категории товаров за всю историю.

СD-плеер за те же семь лет достиг охвата в 55% населения, а бумбокс – 62%. Если замерить период, в течение которого проникновение смартфонов в США увеличится с 9% до 90% [1], то его длительность составит примерно 9 лет, начиная с 2008 года. До 2008 года продукт был, по большей части, экспериментальным, а принимающие участие в этом эксперименте производители[2], в основном, провалились. После озвученного периода в 9 лет подавляющее большинство продуктов станет «общедоступным и взаимозаменяемым», при этом рентабельность продуктов начнет снижаться, а консолидация производства вокруг крупных вендоров – возрастать.

Скорость роста аудитории выглядит еще более впечатляющей, если учесть, что проникновение происходит на индивидуальном уровне, а не на уровне домохозяйства. Следовательно, общее число пользователей составляет свыше 270 миллионов, тогда как обычно потребительские технологии нацелены на 115 миллионов пользователей [это показатель проникновения потребительских технологий среди населения США в абсолютных величинах – прим. перев.]. Таким образом, в случае смартфонов, совершается практически на 60% покупок больше. Примечательно и то, что у данного продукта более короткий жизненный цикл (2 года), по сравнению с другими технологическими продуктами для потребителей[3].



Учитывая вышесказанное, рынок смартфонов США сейчас находится в стадии распространения среди «Позднего Большинства» (консерваторов). Это неудивительно. Точка перегиба кривой жизненного цикла датируется серединой 2012 года, что говорит о том, что в текущей фазе рынок находится уже 2 года. Вполне возможно, что этот период замедления роста может продлиться еще 2 года.

Согласно Джеффри Муру (Geoffrey Moore), подходы к реализации товаров должны выбираться в зависимости от текущего этапа развития рынка. В случае продаж новаторам (первым 2,5% пользователей) стоит делать акцент на самой новизне продукта. Ранние последователи (следующие 13,5%) ищут в продукте эксклюзивность и возможность показать свой финансовый статус в обществе. Для раннего большинства (34%) важно признание продукта другими, а позднему большинству (34%) необходимо повышение продуктивности при использовании продукта. «Скептикам» (последним 16% пользователей) важно сделать безопасный выбор.

Одно из заблуждений насчет жизненного цикла принятия продукта связано со значимостью его цены. Считается, цена продукта становится все более значимой по мере принятия технологии обществом. В действительности цена всегда имеет высокое значение, поэтому это значение можно нивелировать. Ценообразование является лишь одной из составных частей маркетинга, и в любой момент времени [на рынке – прим. перев.] существуют решения в различных диапазонах ценового спектра. Ценообразование может, кроме того, служить фактором намеренного внесения путаницы из-за возможности пакетного предложения продуктов и, наоборот, дробления решений на несколько продуктов.

В качестве примера можно привести отношение Apple к своим продуктам, находящихся на поздних стадиях жизненного цикла. Продукты данной компании известны своим устоявшимся ценообразованием.



Доход с продаж одной единицы Mac, iPod, iPhone и iPad упорно остается стабильным. Это не означает, что устройства продаются по фиксированной цене. Компания постоянно меняет соотношение количества продуктов среднего, низкого и высокого класса, чтобы держать среднюю розничную цену постоянной. По существу средние показатели остаются неизменными, что в свою очередь означает, что вне зависимости от текущей рыночной стадии, рентабельность Apple остается неизменной.

Итак, если мы посмотрим на оставшиеся 2 года роста проникновения смартфонов, то как будут обстоять дела у Apple?

Существует несколько косвенных подтверждений, что дела у компании будут идти достаточно хорошо. В поисках каких-либо зацепок я проанализировал два набора данных: скорость пополнения аудитории новыми пользователями смартфонов (чел./месяц) по отношению к новым пользователям iPhone или Android.


Примечание: Заголовок второго графика должен быть следующим: «Число новых пользователей смартфонов в США в неделю (в разрезе приблизительно трех месяцев)»

Я пытаюсь найти связь между скоростью принятия продукта (смартфона) общественностью и скоростью распространения iPhone. При анализе новых пользователей смартфонов я учитывал количественное изменение пользовательской базы (в разрезе нескольких недель), а также использовал выбор пользователей в пользу iPhone как дифферециад роста.[4] Какой-то заметной зависимости между скоростью распространения продукта и предпочтениями в пользу конкретной платформы не прослеживается. Однако, при более детальном анализе, можно заметить вспышки притока новых пользователей, особенно начиная с 2012 года.

Цифры разнятся весьма существенно: от ~300 тысяч пользователей в неделю, до 1 миллиона. Периоды всплеска, при которых наблюдается рост числа пользователей, могут быть как следствием запуска новых продуктов, так и результатом акций мобильных операторов. Так или иначе, наблюдаются отклонения то в одну, то в другую сторону.

Если мы посмотрим на соответствующий график принятия мобильных платформ среди пользователей, то соотношение будет следующим: пиковые моменты приходятся на выбор в пользу Android, а впадины соответствуют выбору в пользу iPhone. Данный факт может показаться вполне логичным: после того, как уровень насыщения рынка достиг 50%, мобильные операторы «приманивают» акциями оставшихся потенциальных потребителей, которые тяготеют к покупке более дешевых продуктов.

Однако в этих данных также есть нечто парадоксальное. Почему в то время, как рынок находится в стадии насыщения, iPhone показывает хорошие результаты, хотя никто не принуждает его покупать? Судя по расположенному выше графику, после превышения 70%-ного порога принятия технологии, количество новых пользователей iPhone, по сравнению с Android, увеличилось на 1,4 миллиона.

Даже если взглянуть на статистику прошедших 6 месяцев, мы увидим, что количество пользователей iPhone из позднего большинства увеличилось на 15,5 миллионов, тогда как пользователей Android прибавилось на 14,2 миллионов. Станут ли показатели iPhone еще лучше, если количество рекламных акций для «последних консерваторов» и «скептиков» сократится?

Единственное, что можно сказать, это то, что дешевизна не является главным двигателем данного рынка. Повторюсь, она никогда не была им. Незадолго до появления рынка смартфонов наиболее популярным телефоном в США был RAZR. И это было устройство премиум-класса.

Заметки:
[1] этот период роста я считаю «экономически привлекательным»

[2] Palm, BlackBerry, Nokia

[3] например, по сравнению с телевизорами, холодильниками, радиоприемниками и т.д.
