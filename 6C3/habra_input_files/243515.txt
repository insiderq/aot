   	После того, как Apple представила абсолютно новый язык программирования Swift, он не перестает быть предметом активного обсуждения на форумах разработчиков и в курилках dev-конференций. Учить язык можно уже сейчас — Apple соорудила подробный мануал по своему детищу. Правда, только на английском. 

Недавно к нам обратился клиент, который хотел исправить этот недостаток и перевести документацию по Swift на русский язык. Благо переводчикам в Alconost не привыкать к задачам по техническому переводу с обилием IT-шной терминологии. Результат порадовал и нас, и клиента: первые главы уже ждут читателей по этой ссылке, вэлкам!
 
Мы надеемся, что наши усилия по переводу будут востребованы разработчиками. Да и хаб по Swift не зря на Хабре появился.