		Как-то мне на глаза попалась публикация на Хабре — «Батники против эксплойтов». В ней рассказывалось, как одним движением запускать браузер из под специально созданного юзера, у которого нет прав запускать приложения. По замыслу автора это может защитить от эксплойтов и Drive-by атак.

Эта, несомненно полезная статья, имела один недостаток — она была написана для Windows 7 (о чём в ней честно было написано).

После того как Windows XP сняли с поддержки — у меня остался нетбук с хрюшей и идея усилить безопасность системы, адаптировав решение, показалась вполне естественной.

Я надел на себя волшебную шляпу и кастанул биллгейтса 10-го уровня
Попытки адаптировать батник под имеющуюся русскую хрюшу выявили следующие недостатки в коде:

Автор уверен, что все профайлы пользователей лежат в папке c:\users;
Автор уверен, что папка рабочего стола всегда называется Desktop;
Автор постоянно забывает, что системные папки могут содержать пробелы, поэтому очень часто забывает кавычить пути;
Автор видимо не знает как из батника получить директорию где лежат профайлы;
Автор не учёл, что в русской винде бат файлы выполняются в 866-й кодовой странице, а VBS скрипты — в 1251-й. Поэтому русские буквы из батников превращаются в тыкву в скриптах.


После доработки у меня получился следующий код батника:

ВНИМАНИЕ! Код будет работать только на WinXP. Для семерки — смотрите исходную статью.
::Writed by: Sergey.Golovanov at kaspersky.com for habrahabr.ru (enchanted by Alexey Girin — vk.com/alexey.girin)
@echo on
@Echo This batch file will create a new user for browsers with no rights to run downloaded from Internet files. 
Pause
::Setup new user::
set safeusername=saferun_user_%random%
set safepassword=%random%Ai%random%
echo Login: %safeusername% 
echo Password: %safepassword%
net user %safeusername% /delete
del Browserlist4saferun.txt
net user %safeusername% %safepassword% /add

::init new user profile::
echo Option explicit > init_new_user_profile.vbs
echo Dim oShell >> init_new_user_profile.vbs
echo set oShell= Wscript.CreateObject(«WScript.Shell») >> init_new_user_profile.vbs
echo oShell.Run «RunAs /profile /user:%safeusername% ping» >> init_new_user_profile.vbs
echo WScript.Sleep 1000 >> init_new_user_profile.vbs
echo oShell.Sendkeys "%safepassword%" >> init_new_user_profile.vbs
echo oShell.Sendkeys "{ENTER}" >> init_new_user_profile.vbs
echo Wscript.Quit >> init_new_user_profile.vbs
call cscript init_new_user_profile.vbs 
ping -n 10 localhost >> nul
del init_new_user_profile.vbs 

::Setup privileges for new user::
net localgroup users %safeusername% /delete
cscript "%Programfiles%\Windows Resource Kits\Tools\XCACLS.vbs" "%USERPROFILE%\..\%safeusername%\" /D %safeusername%:(OI)(IO)(WDAC,WO,X) 

::Setup browsers::
:FindOpera
if exist "%APPDATA%\Opera\" xcopy /E /I /C /Y /Q /H /R "%APPDATA%\Opera\*" "%USERPROFILE%\..\%safeusername%\AppData\Roaming\Opera\"
if exist "%Programfiles%\Opera\Opera.exe" goto run4opera
if exist "%Programfiles(x86)%\Opera\Opera.exe" goto run4operax86
Goto FindFireFox
:run4opera
echo Opera^|%Programfiles%\Opera>> Browserlist4saferun.txt
Goto FindFireFox
:run4operax86
Set Browsername=Opera
echo Opera^|%Programfiles(x86)%\Opera>> Browserlist4saferun.txt
Goto FindFireFox
:FindFireFox
if exist "%APPDATA%\Mozilla\" xcopy /E /I /C /Y /Q /H /R "%APPDATA%\Mozilla\*" "%USERPROFILE%\..\%safeusername%\AppData\Roaming\Mozilla\"
if exist "%Programfiles%\Mozilla Firefox\Firefox.exe" goto run4Firefox
if exist "%Programfiles(x86)%\Mozilla Firefox\Firefox.exe" goto run4Firefoxx86
Goto FindChrome
:run4Firefox
echo Firefox^|%Programfiles%\Mozilla Firefox>> Browserlist4saferun.txt
Goto FindChrome
:run4Firefoxx86
echo Firefox^|%Programfiles(x86)%\Mozilla Firefox>> Browserlist4saferun.txt
Goto FindChrome
:FindChrome
If exist "%LOCALAPPDATA%\Google\Chrome\Application\chrome.exe" goto run4chrome
Goto FindIE
:run4chrome
::// Can work for some versions of Chrome by not stable. Dissabled for performance. 
::xcopy /E /I /C /Y /Q /H /R "%LOCALAPPDATA%\Google\Chrome\*" "%USERPROFILE%\..\%safeusername%\AppData\Local\Google\Chrome\"
::for /r "%USERPROFILE%\..\%safeusername%\AppData\Local\Google\Chrome\" %%C in (*.exe) do icacls %%C /grant %safeusername%:(X)
::for /r "%USERPROFILE%\..\%safeusername%\AppData\Local\Google\Chrome\" %%C in (*.dll) do icacls %%C /grant %safeusername%:(X)
::echo Chrome^|"%USERPROFILE%\..\%safeusername%\AppData\Local\Google\Chrome\Application\">> Browserlist4saferun.txt
Goto FindIE
:FindIE
::// TODO A lot of XCOPYs
if exist "%LOCALAPPDATA%\Microsoft\Internet Explorer" (
xcopy /E /I /C /Y /Q /H /R "%USERPROFILE%\Favorites\*" "%USERPROFILE%\..\%safeusername%\Favorites\"
xcopy /E /I /C /Y /Q /H /R "%LOCALAPPDATA%\Microsoft\Internet Explorer\*" "%USERPROFILE%\..\%safeusername%\AppData\Local\Microsoft\Internet Explorer\"
xcopy /E /I /C /Y /Q /H /R "%LOCALAPPDATA%\Microsoft\Windows\History\*" "%USERPROFILE%\..\%safeusername%\AppData\Local\Windows\History\"
xcopy /E /I /C /Y /Q /H /R "%APPDATA%\Roaming\Microsoft\Windows\Cookies\*" "%USERPROFILE%\..\%safeusername%\AppData\Roaming\Microsoft\Windows\Cookies\"
)
if exist "%Programfiles(x86)%\Internet Explorer\iexplore.exe" goto run4iex86
if exist "%Programfiles%\Internet Explorer\iexplore.exe" goto run4ie
:run4iex86
echo IExplore^|%Programfiles(x86)%\Internet Explorer>> Browserlist4saferun.txt
goto MakeLinks
:run4ie
echo IExplore^|%Programfiles%\Internet Explorer>> Browserlist4saferun.txt

::Make links::
:MakeLinks
rd /s /q "%USERPROFILE%\Downloads\Browser"
rd /s /q "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks"
"%Programfiles%\Windows Resource Kits\Tools\linkd.exe" /d "%USERPROFILE%\Downloads\Browser" "%USERPROFILE%\..\%safeusername%\Downloads"
mkdir "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks"
echo on
For /f «tokens=1,2 delims=|» %%A in (Browserlist4saferun.txt) do (
echo Option explicit > "%%B\%%A.vbs"
echo Dim oShell >> "%%B\%%A.vbs"
echo set oShell= Wscript.CreateObject^(«WScript.Shell»^) >> "%%B\%%A.vbs"
echo oShell.Run «RunAs /user:%safeusername% %%A.exe» >> "%%B\%%A.vbs"
echo WScript.Sleep 1000 >> "%%B\%%A.vbs"
echo oShell.Sendkeys "%safepassword%" >> "%%B\%%A.vbs"
echo oShell.Sendkeys "{ENTER}" >> "%%B\%%A.vbs"
echo Wscript.Quit >> "%%B\%%A.vbs"
echo Set oWS = WScript.CreateObject^(«WScript.Shell»^) > "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\%%A.lnk.vbs"
echo sLinkFile = "%USERPROFILE%\Рабочий стол\SafeLinks\%%A_saferun.LNK" >> "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\%%A.lnk.vbs"
echo Set oLink = oWS.CreateShortcut^(sLinkFile^) >> "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\%%A.lnk.vbs"
echo oLink.TargetPath = "%%B\%%A.vbs" >> "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\%%A.lnk.vbs"
echo oLink.IconLocation = "%%B\%%A.exe,0" >> "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\%%A.lnk.vbs"
echo oLink.WorkingDirectory = "%%B\" >> "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\%%A.lnk.vbs"
echo oLink.Save >> "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\%%A.lnk.vbs"
) 
for /r "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\" %%p in (*.vbs) do cscript %%p
for /r "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\" %%v in (*.vbs) do del %%v
:: Open Explorer with links::
explorer "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks\"

:: Create Uninstall::
echo @echo off > uninstall_%~n0.bat
echo net user %safeusername% /del >> uninstall_%~n0.bat
echo rd /s /q "%USERPROFILE%\Downloads\Browser" >> uninstall_%~n0.bat
echo rd /s /q "%USERPROFILE%\ђ Ў®зЁ© бв®«\SafeLinks" >> uninstall_%~n0.bat
echo rd /s /q "%USERPROFILE%\..\%safeusername%\" >> uninstall_%~n0.bat
echo For /f «tokens=1,2 delims=|» %%%%A in (Browserlist4saferun.txt) do del "%%%%B\%%%%A.vbs" >> uninstall_%~n0.bat
echo del Browserlist4saferun.txt >> uninstall_%~n0.bat
echo del %%0 >> uninstall_%~n0.bat

:Exit


Этот текст надо скопировать в notepad, сохранить его как SaveRun.bat.

ВНИМАНИЕ! Если запускать планируется на английской винде, необходимо в коде реплейснуть все подстроки "ђ Ў®зЁ© бв®«" и «Рабочий стол» (без кавычек) на подстроку Desktop (ну, или на то название директории, где у вас лежит десктоп). Иначе — данный код будет корректно работать только в русской винде.

Кроме этого, необходимо скачать и поставить на машину:

1. Windows Server 2003 Resource Kit Tools;
2. Extended Change Access Control List Tool (Xcacls).

ВНИМАНИЕ! И то и другое надо поставить в дефолтную директорию, куда ставится ресурс кит — %Programfiles%\Windows Resource Kits\Tools\

Или поправьте эту строку в коде.

Теперь — можно запускать.

Система работает очень просто. Она:

Создаёт рандомного юзера, у которого нет прав запускать приложения из профайла, куда по замыслу автора все вирусы будут стремиться попасть, что бы оттуда запуститься;
Сканирует дефолтные пути, по которыми ставятся все основные браузеры — лиса, хром, ослик, опера, и если находит их — создаёт для каждого браузера VBS скрипт, который (если его запустить) генерит линк на запуск браузера. Если этот линк потом запустить — браузер запустится с правами того кастрированного юзера;
Создаёт на рабочем столе папку SafeLinks, куда кладёт все VBS скрипты.
Туда же будут попадать сгенерённые этими скриптами линки на запуск браузеров;
Создаёт файл uninstall_%~n0.bat рядом с исходным батником. Этот батник (если его запустить) удалит и кастрированного пользователя и скрипты и директорию. И самого себя тоже.

