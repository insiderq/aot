   	

В этом обзоре мы поговорим про бесплатные курсы виртуальной академии Microsoft MVA, которые будут полезны как профессиональным разработчикам программного обеспечения и ИТ-про, так и новичкам. Обратите внимание, что видео-плеер на сайте для ряда курсов, которые созданы на английском языке, содержит возможность включить русские субтитры.

Хит! Введение в программирование игр на Unity

Создавать игры просто и весело. Цель этого курса – познакомить вас с тем, как создавать 2D-игры на Unity, и по итогам курса сделать свою собственную игру. Мы разберемся, как создать персонажа, научиться им управлять и добавить в игру несколько уровней, наполненных препятствиями и призами. Кроме того, курс содержит в себе базовую информацию, необходимую для того, чтобы начать программировать на C# в рамках Unity.

Плюс еще курсы: Разработка 2D-игр на HTML5 | Создание игр на Project Spark | Быстрое испытание: Project Spark

Хит! Бизнес и облако: лучшие практики решений – по материалам конференции Azure Business Talks

В данном курсе вы узнаете о практической стороне действующих бизнесов, эксперты из Microsoft вместе с вами ищут простые и понятные ответы на краеугольные вопросы. О своем опыте использования Microsoft Azure и облаков рассказывают компании и стартапы. В курсе также приведены технические доклады про использование обсуждаемых сервисов в концепции и на практике.

Хит! Модернизация инфраструктуры организации с помощью Windows Server 2012 R2

14 июля 2015 года завершится поддержка серверных операционных систем Windows Server 2003 и Windows Server 2003 R2. Во многих компаниях, использующих эти ОС, запускаются проекты модернизации ИТ-инфраструктуры и миграции на более современные системы. Именно поэтому ИТ-специалистам будет особенно полезно взглянуть на технологические решения, заложенные в новейшую ОС семейства Windows Server – Windows Server 2012 R2. В рамках этого курса рассмотрены усовершенствования компонент, применяемых практически в любой организации: службы Active Directory, инструменты построения СХД, средства автоматизации административных задач. Кроме того, вы сможете познакомиться с решениями по управлению идентификационными данными в гибридных структурах, в частности с сервисом Azure Active Directory Premium. 

Хит! Быстрый старт: создание приложений на Node.js

Разработчики, хотелось ли вам попробовать Node.js? Интересуетесь тем, как добавить двустороннее взаимодействие в свои приложения? Посмотрите как эксперты Stacey Mulcahy и Rami Sayar рассказывают о использовании Node.js, как простейшем способе разработки приложений обменивающихся сообщениями в реальном времени. Узнайте как работать с Node.js в Visual Studio и Azure на примере разработки с нуля приложения чата. 

В этом курсе, четвертом по счету в серии Web Wednesdays вы увидите пошаговую демонстрацию и получите практические советы и детальные пояснения. К концу курса вы будете знать как настроить Node.js на Windows, разработать веб-фронтенд на базе Express, разместить приложение Express в Azure, использовать Socket.IO для уровня обмена сообщениями в реальном времени и совмещении всего этого вместе.

Хит! Английский Приложения Single Page Applications на базе jQuery или AngularJS

Devs, want to translate your MVC .NET skills to the web platform? Take advantage of jQuery and AngularJS to dynamically update HTML5-based apps, and learn how AJAX works for asynchronous calls. Simplify the development process by separating the model, view, and controller for your web apps, enabling your content to work within a browser and within Windows 8 and Windows Phone 8 apps. 
 
Watch popular presenters Stacey Mulcahy and Dave Voyles, in our third Web Wednesdays event, for an exploration of single page apps and a look at Visual Studio and Internet Explorer tools to add structure to your projects. See in-depth demos on debugging, and learn how to deploy your apps to Azure. Get the real-world examples you need to get started in jQuery and AngularJS.

Набор новых полезнейших курсов по Office 365:

Глубокое погружение в программную модель Office 365 — In this training, find out what you can do with APIs, task panes, and data manipulation. See how easy it can be, with experts Scot Hillier and Ted Pattison. They walk you through what you need to know and offer lots of practical tips and real-world demos, with a close-up look at all the hooks and wires; 
Предотвращение потери данных в Office 365 — In this course, examine how to configure, customize, and monitor the data loss prevention (DLP) features provided through Office 365. Learn how the rich DLP functionality seamlessly integrates into Microsoft Exchange, Outlook, OWA, and SharePoint, providing context, user education, and information protection; 
Глубокое погружение в интеграцию Office 365 API и ваших веб-приложений — Get the basics on the Office 365 APIs for Calendar, Mail, Contacts, OneDrive for Business, SharePoint, and more. And dive deep into the details, including authentication, authorization, access tokens, and process Flow; 
Шифрование в Office 365 — It's important for administrators to not only understand how encryption is used and configured but also to understand what users need to do to help reduce support calls. This course looks at encryption from the perspective of administrators, senders, and recipients; 
Безопасность и соответствие требованиям Office 365 и Exchange Online Protection — This course provides the details an IT Professional needs to know about Microsoft Exchange Online Protection (EOP), why it is necessary in your organization, how Microsoft has architected EOP to handle spam and malware, and how to deploy, configure, and manage EOP; 
Управление производительностью Office 365 — Want to make sure you get the best performance possible from Office 365? We want you to, too! Take this course, and learn how you can plan for great performance for a new deployment of Office 365. And find out how to troubleshoot performance issues that may arise after you are deployed.


Новый! Английский Сценарии разработки и тестирования (Dev/Test) в мире DevOps

Devs, want a deep dive into the dev/test portion of DevOps and application lifecycle management (ALM)? Explore modern tools for unit testing, functional UI testing, load testing, and more, see helpful demos on debugging Azure workloads using Visual Studio, and learn about the fundamentals of source control in TFS.

Watch as leading experts Bret Stateham, Cale Teeter, Jeff Levinson, and Charles Sterling walk through the entire ALM cycle. See how to access the platform, explore how the tools fit together, and get the details on real-world practices you can use today to improve your dev process. Get the pros and cons of different tools, so you can pick what works best for you. If you've got questions about using current ALM tools in a real production environment, check out this course.

Новый! Английский Разработка универсальных приложений Windows на C# и XAML

Get real-world guidance for developing universal Windows apps, and save yourself valuable time when creating apps for today’s mobile workforce and consumer marketplace. Learn from Microsoft experts as they build a working app using tools and techniques that can give you a dramatic advantage as a developer targeting both Windows and Windows Phone devices. See what's smart to share and what's not, when developing for the two platforms. Explore a broad range of features, covering both consumer and enterprise scenarios.

Jerry Nixon and Daren May bring together best practices and key insights from Microsoft internal teams, including the built-in code-generation tools in Visual Studio that can automatically build out hundreds of classes and thousands of lines of code. Check the module list to find the topics that interest you.

Новый! Английский Быстрый старт: DevOps с Azure Resource Manager

Developers and IT Pros, do you want to make management and deployment of Azure resources more efficient? If you want faster and more repeatable application deployments across all your Azure environments, add to your skill set and walk with the experts through Azure resource management.

This course promises lots of helpful demos, an exploration into the JSON language for creating resource templates, and a look at Application Lifecycle Management (ALM) integration. And it takes a deep dive into some DevOps best practices, like implementing infrastructure and configuration as code. Watch the experts and experience the new declarative model at work, complete with templates to define, configure, connect, and deploy your infrastructure resources.

Новый! Английский Windows Server 2012 R2: Using IP Address Management (IPAM)

Get a detailed look at IP Address Management (IPAM) in Windows Server 2012 R2. IPAM is an integrated suite of tools to enable end-to-end planning, deploying, managing, and monitoring of one’s IP address infrastructure, with a rich user experience. It automatically discovers IP address infrastructure servers on your network and enables you to manage them from a central interface. Find out more in this course.

Новый! Английский Автоматизация управления севисами с Windows Azure Pack

Want to get a head start on automating your cloud infrastructure and services? Join Microsoft's Symon Perriman, as he hosts an interactive training session on Service Management Automation (SMA), the automation component of Windows Azure Pack (the self-service portal from Microsoft that connects to System Center 2012 R2). Symon and members of the automation engineering team explore the Windows Azure Pack and SMA vision, plus how to configure it. They cover best practices for service, tenant, and fabric administration, along with how to integrate with PowerShell, Orchestrator, and other Microsoft and third-party systems. Don't miss this opportunity to get the details you need on SMA!

Хит! Английский Imagine Cup: постройте свой студенческий стартап

Are you interested in participating in the Microsoft Imagine Cup? To do well in this global student technology competition, you need more than your already-awesome coding skills. You also need an understanding of product development, from vision to pitch to research, plus competitive analysis and business models. Explore required skills with seasoned experts who offer their practical tips and insights on how a multi-disciplinary team develops a complete project.

Learn the difference between an issue and a problem, map the ecosystem around it, and see how your project can fill a role and meet a need. By the end of the course, you know how to research, conceptualize, develop, plan, and execute your project. Check out this course for a look at what it takes to succeed in the Imagine Cup, with special emphasis on indie games, innovation, and world citizenship.

Новый! Английский От данных к изучению и пользе: революция бизнес-аналитики (BI)

As a finance professional, you use Excel every day. Did you know that Excel has advanced data visualization capabilities in Power View and Power BI that can change the way you work? Stop creating static presentations for business reviews, and start having proactive conversations with your business partners. Gone are the days of waiting for IT to create or edit a report so you can answer a question.

This course was designed for finance professionals, but the tools and techniques taught will benefit anyone who is not a business intelligence (BI) specialist. Join the experts to find out how easy it is to get data, shape it for your business needs, and maximize your impact.

Новый! Английский Быстрое испытание: переносимость HTML5

Want to know why HTML5 is so popular with game developers? And why this flexible and powerful language continues to gain popularity? Part of the reason is the language's cross-platform ability and the ease with which you can port your game. In this Quick Start Challenge, walk through the easy steps to port an existing web app to a Windows Store and Windows Phone game.

Новый! Английский Быстрое испытание: Microsoft Advertising SDK

If you’re building a game, one of the main decisions you have to make is how to monetize your app. With the growing freemium economic strategy, a simple way to make some money is to display ads in your application. And you can do so in your Windows 8.1 apps using the Microsoft Advertising SDK for Windows 8.1.

Get started by integrating the SDK from within Visual Studio 2013, design ads into your apps, and stay up-to-date with the latest developments. For more information, visit the product site. Roughly 50 percent  of the revenue brought in on the Windows platform is through the Microsoft Ads SDK! Find out more, in this Quick Start Challenge.

Новый! Английский Жизненный круг App-V 5.0 Package

Would you like to take a journey through the lifecycle of a Microsoft Application Virtualization (App-V) 5.0 package? Join us to do just that, from the birth of a sequenced package, through deployment, streaming, configuration, updates, connection groups, user customization, and eventual termination.

In this course, explore stand-alone mode, demo-driven, behind-the-scenes views of how each part of the application lifecycle is handled by the App-V Client. Look under the hood for insight into how App-V applications are handled, regardless of delivery mechanisms (no ties to Configuration Manager or Full Infrastructure).

Полезные ссылки

Видео пленарного доклада и технических докладов TechEd Europe 2014 


Попробовать Azure бесплатно на 30 дней! 


Центр разработки Microsoft Azure (azurehub.ru) – сценарии, руководства, примеры, рекомендации по разработке 
Twitter.com/windowsazure_ru — последние новости Microsoft Azure 
Сообществе Microsoft Azure на Facebook – эксперты, вопросы 


 
Изучить курсы виртуальной академии Microsoft по облачным и другим технологиям 


Загрузить бесплатную или пробную Visual Studio 


Стать разработчиком