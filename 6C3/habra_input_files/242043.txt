   	 Skype. Как мы его любим и одновременно ненавидим. Долгое время, Skype был единственной программой, которую я старался закрывать при работе лаптопа от батареи: потреблял около 4% CPU, будил процессор 250-300 раз в секунду, ничего при этом не делая, оставляя процессору меньше времени на нахождение в более энергосберегающем состоянии.

Увидел я однажды комментарий пользователя Vayun:
Возможно проблема проявляется не у всех, проверить легко: запускаем скайп, запускаем top (на ноутбуке работающем от батареи наглядней будет powertop) и смотрим сколько он отъедает cpu.

Я долго не возился, придавил только poll, увеличив timeout. Это даёт наибольший выигрыш (уменьшая %CPU скайпа раз в 5), но наверное можно лучше. 
Собственно, вот: Skype-poll-fix. Увеличивает таймаут у вызовов poll (Linux) и kevent (Mac OS), снижая потребление процессора в 4-5 раз.

Как установить
Для Linux:
git clone https://github.com/ValdikSS/skype-poll-fix.git
cd skype-poll-fix
make
LD_PRELOAD=./skype-poll-fix.so skype

Для Mac OS:
git clone https://github.com/ValdikSS/skype-poll-fix.git
cd skype-poll-fix
make
DYLD_INSERT_LIBRARIES=./skype-poll-fix.dylib /Applications/Skype.app/Contents/MacOS/Skype
