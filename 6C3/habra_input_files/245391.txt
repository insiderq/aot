   	Все больше игровых релизов, все меньше свободного времени. Начало декабря — самое время оторваться от прохождения очередного ААА-шедевра и почитать, что интересного произошло в игровой индустрии за ноябрь. 



Разработка

 Небольшой гайд для лида разработки.
 Состоялся релиз Blend4Web SDK версии 14.11, предназначенной для создания 3D web-приложений.
 На gamedev.net появился полезный материал от Джеймса Вучера об инструментах, которые пригодятся программистам в работе.
 Как сгенерировать красивый запутанный лабиринт?
 Джеймс Вучер рассказал об интересном инструменте Viewport.
 WatchKit стал доступен для разработчиков.
 Реализуем Meta System в С++.
 Чему IKEA может научить гейм-дизайнера? 
 Тем, кто разрабатывает спортивные игры, будет интересна статья «Бросаем выигрышный пас».
 «Динамический поиск пути» – большая обучающая статья на портале gamedev.net.
 Unity 4.6 и новые инструменты UI.
 В статье «ПИД-регулятор физических тел» обсуждаются разные подходы к приданию мягкого вращения физических тел, независимо от того, являются они стационарными или пребывают в движении.
 Немного о новом игровом движке Banshee.
 Казуальные игры: как выбрать правильный формат карты?
 Наконец-то состоялся релиз версии 1.00 среды разработки и компилятора Sega Genesis Development Kit.
 Как снизить риски при разработке игры? В SuperData попробовали найти ответ на этот вопрос.
 Гейм-дизайн хоррора может быть пугающим.
 OpenGL и специфический рендеринг – об этом статья Марека Кржемински.
 Состоялся релиз NeoAxis 3D Engine 3.0
 Визуализация данных в играх: таблица лидеров.

Мобильное направление

 Стандартный геймплей и IAP-метрика в мобильных играх: часть первая, часть вторая.
 Кристер Кларкссон из Modesty рассказал, что такое по-настоящему успешная мобильная игра. 
 Тренды мобильного игрового рынка в 2015 году.
 Почему многие мобильные игры проваливаются на международном рынке? Ответ на этот вопрос – в статье Винсента Чана.
 Сейчас люди проводят больше времени, просматривая приложения на телефоне, чем ТВ-программы.
 Android начинает обгонять iOS на рынке планшетов.
 Питер Молиньё: в мобайле постоянный чертов ад. 
 Маркетинговый директор компании Dots Кристиан Калдерон поделился размышлениями о том, какую роль маркетинг играет в успехе мобильных игр.
 Кевин МакКарди рассказал о том, как его компания Golden Gate Games завоевывает мобильный рынок Китая.
 На мобильном рынке увеличивается количество молодых геймеров, а прибыль растет.
 Исследование: как мобильные игры могут стать успешными.

Звук

 Создаем заглавный трэк для инди-игры в жанре RPG.
 Я слышу голоса: запись озвучки для игрового проекта. 
 Мэтью Бентли попробовал разобраться, почему игровое аудио часто бывает отстойным.
 Как инди-разработчики меняют индустрию звукозаписи.
 Илья Волков рассказал о принципах имитации объемного звучания. 
 Как записывалась музыка к Mario Kart 8.

Новости

 BlizzCon 2014: Starcraft 2, Hearthstone: Gnomes vs. Goblins, фильм Warcraft и новый IP – Overwatch.
 В 2014 году будет активировано 800 млн смартфонов.
 Unreal Engine обновился до версии 4.6
 В Unity Asset Store теперь можно купить музыку.
 Sony объяснила, почему в PSN нельзя менять имя.
 Ветераны шведской игровой индустрии помогут начинающим разработчикам.
 Игра The Black Glove, разработчики которой ранее работали над серией Bioshock, не собрала нужную сумму на Kickstarter. В статье на портале wired.com Бо Мур попытался найти ответ на вопрос, почему.
 EA отменили разработку MOBA-игры Dawngate.
 Студия Big Fish Games из Сиэтла будет куплена компанией Churchill Downs.
 Россия и F2P MMO доминируют на восточно-европейском цифровом рынке.
 The Entertainment Software Association выяснила, что игровая индустрия растет в 4 раза быстрее экономики США. 
 Marmalade Technologies проведет бесплатную конференцию для инди-разработчиков в Лондоне в следующем месяце.
 Креативный директор первого Assassin's Creed основал свою студию – Panache Digital Games.
 Newzoo: Юго-Восточная Азия более перспективный регион, чем Латинская Америка и Восточная Европа.
 О Casual Connect можно прочесть на портале AppTracktor.

Это интересно

 Питер Кардвел-Гарднер поделился своими эмоциями в статье «Создание игры – это тяжелый труд».
 Джон Ромеро: «Стоит ли создавать шутеры?».
 Немного фактов о Gamergate.
 История игровой индустрии в России: от «Тетриса» и русских квестов до мобильных игр.
 KitchenRiots попробовали разобраться, чему учат игры.
 Цикл статей об играх жанра RTS: «И нашлось это в пустыне…», «А потом был Blizzard».
 Томас Хеншел рассказал о том, как успевать к дедлайнам и подкинул рецепт отличного супа.
 Sumoman: история вывода игры на Steam Greenlight.
 Дэвид Гейдер рассказал о создании сценария для Dragon Age II.
 «Нажмите Х, чтобы испытать эмоции», или Как разработчики игр пытаются добиться от игрока сопереживания.
 «Обаятельное зло» — статья о самых популярных злодеях в играх.
 Как обстоят дела на ММО-рынке?
 Rouge Legascy и черта между удовольствием и зависимостью.
 Роб Прадо из Blizzard рассказал о преимуществах открытой разработки и формирования комьюнити заранее.
 Создаем «играбельный» UI.
 История Xbox One в трёх актах: запуск, смерть и возрождение.
 «Лингвистическое тестирование: дьявол кроется в деталях» – статья о локализации и необходимости перепроверки.
 Кевин Хардвуд расскажет о том, как создавать геймплейные трейлеры, сравнимые по качеству с трейлерами ААА-проектов.
 Места и времена: детство, видеоигры и ты.
 Как написать хорошее описание к приложению?
 О том, какими красивыми бывают доспехи и одежда в играх рассказали KitchenRiots.
 Битва консолей продолжается, а PS4 в ней по-прежнему лидирует.
 Стив Петерсон с портала alistdaily попытался разобраться в том, почему игры по лицензии наконец-то начали набирать популярность.
 8 запоминающихся анимаций смерти из видеоигр.
 Геймдизайнер Дамиан Монье рассказал об игре «Ведьмак: Дикая охота».
 Как анализировать инди-игры?
 Портал ain.ua опубликовал список крупнейших разработчиков мобильных и социальных игр в Украине.
 Роберт Воркман проанализировал статью Фархада Манджо «Свержение баннерной рекламы: монстр, который проглотил сеть» и высказал свои мысли о ней.

Интересное в Twitter










Игровые новости 

Анонсирована Just Cause 3 – синглплеер для консолей последнего поколения и ПК.
Действия игры Game of Thrones от Telltale будут разворачиваться вокруг семьи Форрестеров.
Microsoft зарегистрировала торговую марку Battletoads.
Выход Bloodborn перенесли на март 2015 года. А «Ведьмак 3» — на май.
Тони Хоук совместно с Activision работает над новой игрой.
The Legend of Zelda: Majora's Mask выйдет на Nintendo 3DS весной 2015 года.
В Bethesda опровергли слух о регистрации новой торговой марки Fallout: Shadow of Boston.
Activision и Bungie работают над сиквелом Destiny.
Акции Ubisoft упали на 9 % из-за Assassin’s Creed: Unity.
Компания BioWare представила сценариста Mass Effect 4. Им стал Крис Шлерф, который ранее работал над Halo 4.

Подкасты

 Video Gamer UK Podcast #84
 Подкаст AppTracktor #15
 Подкаст AppTracktor #16
 Giant Bombcast: о Assassin's Creed: Unity, Master Chief Collection, играх от Blizzard и многом другом.
 Giant Bombcast: о Call of Duty, Sunset Overdrive, Donkey Kong Country и многом другом.

События

10 декабря — Game QA & Localization 2014 San Francisco 
8-14 декабря — Rio Game Play & Conference 2014
12 декабря — Joypad Presents: #VideoGameDemocracy

4-10 января — Awesome Games Done Quick 2015
13-14 января — Pocket Gamer Connects 2015
12-15 января — Hong Kong Toys & Games Fair 2015
19 января — NexGen Developers Day
20 января — MGF London 2015 
20-21 января — Next-Level Indie Forum
22 января — Central European Games Conference
23 января — PAX South
23 января — Global Game Jam
23-26 января — Music and Gaming Festival (MAGFest)
28 января — GameDevHacker Past Trends and Future Bets 
28 января — Taipei Game Show 2015
29 января — 