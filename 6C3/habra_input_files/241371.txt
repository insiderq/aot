   	Только что пришёл мейл от Амазона со следующим содержанием:

aws.amazon.com/blogs/aws/aws-region-germany/

It is time to expand the AWS footprint once again, this time with a new Region in Frankfurt, Germany. AWS customers in Europe can now use the new EU (Frankfurt) Region along with the existing EU (Ireland) Region for fast, low-latency access to the suite of AWS infrastructure services. You can now build multi-Region applications with the assurance that your content will stay within the EU. 

New Region

The new Frankfurt Region supports Amazon Elastic Compute Cloud (EC2) and related services including Amazon Elastic Block Store (EBS), Amazon Virtual Private Cloud, Auto Scaling, and Elastic Load Balancing. 

<...>

This is our eleventh Region. As usual, you can see the full list in the Region menu of the AWS Management Console:.







It also supports AWS Elastic Beanstalk, AWS CloudFormation, Amazon CloudFront, Amazon CloudSearch, AWS CloudTrail, Amazon CloudWatch, AWS Direct Connect, Amazon DynamoDB, Amazon Elastic MapReduce, AWS Storage Gateway, Amazon Glacier, AWS CloudHSM, AWS Identity and Access Management (IAM), Amazon Kinesis, AWS OpsWorks, Amazon Route 53, Amazon Relational Database Service (RDS), Amazon Redshift, Amazon Simple Storage Service (S3), Amazon Simple Notification Service (SNS), Amazon Simple Queue Service (SQS), and Amazon Simple Workflow Service (SWF). 

The Region supports all sizes of T2, M3, C3, R3, and I2 instances. All EC2 instances must be launched within a Virtual Private Cloud in this Region <...>. 

There are also three edge locations in Frankfurt for Amazon Route 53 and Amazon CloudFront. 


Согласно информации, одиннадцатый по счёту регион доступности AWS уже доступен для использования всех вышеперечисленных сервисов, в частности — для создания инстансов Amazon EC2. Впрочем, судя по http://aws.amazon.com/ec2/pricing/