   	Сертификация компании Cisco считается одной из самых престижных сертификаций в области ИТ. Это связано с тем, что в отличии от систем сертификации многих других вендоров, сертификационные экзамены Cisco включат в себя помимо стандартных тестов на вопросы, симуляции и настройку виртуального оборудования.

Если говорить в общем, то Cisco выделяет у себя пять ступеней квалификации (при этом существует девять различных направлений):



Entry (Cisco Certified Entry Networking Technician) – начальный уровень, который подтверждает о наличии у специалиста начальных знаний в сетевых технологиях. Данный уровень существует в двух направлениях Cisco – CCENT (Cisco Certified Entry Networking Technician) и CCT (Technician). Первое направление – общее для всех девяти ступеней, создано для специалистов, начинающих свой путь в сфере сетевых технологий Cisco. 

Второе скорее создано для сервисных центров – специалисты CCT должны уметь делать диагностику, восстанавливать данные, чинить и заменять компоненты в устройствах компании Cisco. В CCT можно выделить три направления:
CCT Data Center – для специалистов, работающих с оборудованием сетей Центров Обработки Данных (ЦОДов) – семейства Cisco Nexus, оборудования Cisco Unified Computing System (Cisco UCS), межсетевых экранов Cisco Adaptive Security Appliance (Cisco ASA).

CCT R&S – для специалистов, работающих с опорными сетями – коммутаторами Cisco Catalyst и маршрутизаторами Cisco ISR опорных сетей, межсетевыми экранами Cisco ASA, маршрутизаторами провайдерского класса Cisco Aggregation Services Routers (Cisco ASR) и другими.
CCT TelePresence – для специалистов, работающих в области оборудования унифицированных коммуникаций – то есть систем видеоконференций и телефонии.

Все сертификаты имеют срок действия три года.



Associate (Cisco Certified Network Associate) – базовый уровень, который подтверждает наличие знаний и умений для проектирования, настройки оборудования, управления и поддержки сетей малого и среднего размера. Рассчитаны на специалистов, работающих с оборудованием от одного до трех лет. На данному уровне существует девять направлений: Routing & Switching, Design, Data Center, Security, Service Provider, Service Provider Operation, Video, Voice, Wireless. Для сдачи экзаменов уровня Associate нет никаких требований (наличия CCENT не требуется). Все сертификаты действительные в течении трех лет.



Professional (Cisco Certified Network Professional) – профессиональный уровень. Наличие сертификации уровня Professional говорит о том, что специалист имеет глубокие знания в данной области и большой опыт работы с оборудованием уровня предприятия. Для получения статуса Professional при сдаче экзамена – есть обязательное требование – наличие статуса Associate, без которого статус Professional получить нельзя. Это значит, что даже при выполнении требований (сдаче экзаменов уровня Professional), чтобы получить статус – нужно будет сдать экзамен (или экзамены, в зависимости от направления) уровня Associate. На данном уровне есть восемь направлений: Routing & Switching, Design, Data Center, Security, Service Provider, Service Provider Operation, Voice, Wireless. Для направления Video (которое появилось позже всех) на данный момент высшим статусом является CCNA Video. Все сертификаты имеют срок годности – три года.



Expert (Cisco Certified Internetwork Expert) – экспертный уровень. Сертификация появилась в 1993 году и на данный момент является самой престижной технической сертификацией. Экзамен на CCIE обычно состоит из двух частей: письменной (в центре тестирования Pearson VUE) и практической (лаборатория Cisco). Обязательных требований для экзамена нет – то есть, его можно сдавать не имея статусов Associate или Professional. На данной ступени, так же как и на предыдущей, восемь направлений: Routing & Switching, Design, Data Center, Security, Service Provider, Service Provider Operation, Collaboration, Wireless. Как можно заметить, в отличии от Professional нету Voice – который объединили с Video и данное направление назвали Collaboration. В отличии от предыдущих ступеней, все сертификаты имеют срок годности – два года.

Кстати, по статистике, только около 3% сертифицированных специалистов доходят до ступени CCIE. Всего специалистов со статусом CCIE около 50 000 специалистов (с учетом того, что некоторые имеют статусы в нескольких направлениях, при этом у четверти из всех недействующая сертификация).

Architect (Cisco Certified Architect) – высший уровень сертификации. Появился в 2010 году, в мире насчитывается около десятка человек, обладающим данным статусом (на начало 2013 года было 7 человек). Для получения данного статуса необходимо написать своеобразный диплом и защитить его перед комиссией. Данный уровень включает в себя направления Design, R&S, Wireless, Security. Обязательным требованием для сдачи является наличие сертификации CCDE (Cisco Certified Design Expert). 
Срок действия сертификации пять лет.



Specialist Certifications (Cisco Specialist Certifications) – специализированные сертификации по различным направлениям, уровня Associate – Professional. 
Некоторые экзамены из данных сертификаций требуются для получения статусов уровней Associate — Professional. Основная задача этих специализаций – получение компаниями-партнерами определенных статусов и компетенций в определенных областях. Областей можно выделить восемь направлений: Business Certifications, Data Center Certifications, Internet of Things Certifications, Network Programmability Certifications, Operating System Software Certifications, Security Certifications, Service Provider Certifications, Video Certifications.
Срок действия данных специальностей обычно два года.

Общая схема по основным ступеням Cisco:



Теперь рассмотрим подробнее по направлениям.

Routing & Switching.
Самое старое и популярное направление (раньше было единственным), посвящено маршрутизации и коммутации сетевого оборудования в сетях, построенных на оборудовании Cisco.

Первая ступень – CCNA Routing & Switching состоит из двух частей:


Для получения статуса необходимо сдать либо два экзамена: 100-101 ICND1 и 200-101 ICND2, либо один общий 200-120 CCNA.

Для подготовки есть курсы: 

Interconnecting Cisco Networking Devices Part 1 (ICND1);
Interconnecting Cisco Networking Devices Part 2 (ICND2); 
Interconnecting Cisco Networking Devices: Accelerated (CCNAX).
Аналогично, можно пройти двумя курсами (каждый по 40 часов), а можно пройти одним ускоренным (60 часов).

Все сертификаты действительны в течении трех лет, для ресертификации (продления срока действия статуса) необходимо будет:

Сдать ещё раз экзамен CCNA, либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.

Вторая ступень – CCNP Routing & Switching.
Данная ступень состоит из трех экзаменов: ROUTE, SWITCH, TSHOOT.
Как следует из названий, первый экзамен посвящен маршрутизации в сетях, второй коммутации в сетях, а третий поиску и устранению неисправностей в сетях. Летом 2014 года появились новые версии экзаменов, старые версии возможно сдать до 29 января 2015 года включительно.

Для получения статуса CCNP R&S необходимо сдать соответственно все три экзамена:

642-902 ROUTE или 300-101 ROUTE
642-813 SWITCH или 300-115 SWITCH
642-832 TSHOOT или 300-135 TSHOOT.

Из этих экзаменов можно выделить TSHOOT – т.к. он по своему формату не похож на другие экзамены, в нем нет теоритической части и различных drap&drop. Он чисто практический, на решение проблем в сети (симуляции сети).
До 29 января можно сдавать любые экзамены для получения статуса (например, два старой редакции и один новой), после 29 го января останутся только новые редакции экзаменов.

Для подготовки можно пройти курсы:

Implementing Cisco IP Routing (ROUTE v2.0)
Implementing Cisco IP Switched Networks (SWITCH v2.0)
Troubleshooting and Maintaining Cisco IP Networks (TSHOOT v2.0)
Стоит учесть, что каждый сданный экзамен действителен в течении одного года, т.е. надо за год успеть сдать еще один – чтобы предыдущий не потерял свою актуальность. После сдачи второго, срок на оба будет продлен на год от даты сдачи второго экзамена.
Сам статус CCNP действителен в течении трех лет. Для ресертификации необходимо сдать экзамен уровня Professional – либо уровня Expert, обычно сдают письменный экзамен уровня Expert.

Третья ступень – CCIE Routing & Switching.
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 400-101 CCIE сдается в центрах тестирования Pearson VUE.
А вот с лабораторной работой интереснее – её нельзя сдать в обычном центре тестирования – прохождение лабораторной возможно только в центрах тестирования Cisco. Первую попытку сдачи лабораторной необходимо сделать не позднее, чем полтора года от сдачи письменного экзамена – письменный экзамен действителен в течении трех лет (если за это время не будет сдана лабораторная – нужно будет заново сдавать письменный экзамен).
Вариант первый: сдача экзамена зарубежом. R&S можно сдать в Индии (Бангалор), Китае (Пекин), Бельгии (Брюссель), ОАЭ (Дубай), Гонконге, США (Сан Хосе и РТП), Австралии (Сидней) и Японии (Токио).
Вариант второй: мобильная лаборатория. Несколько раз в год (обычно два-три раза) к нам в страну приезжает мобильная лаборатория Cisco – в которой возможно так же сдать лабораторную. Записываться лучшее заранее, так как количество мест ограниченно, а спрос достаточно большой. Подробнее о том, в какие даты приедет мобильная лабораторная, можно на сайте Cisco learning network.
Летом 2014 вышла новая версия лабораторной работы CCIE R&S – v5. 
На данный момент экзамен состоит из следующих разделов: два часа поиск и устранение неисправностей на незнакомом стенде, полчаса секция диагностики, пять с половиной часов секция настройки.
Летом 2014 вышла новая версия лабораторной работы CCIE R&S – v5. 
На данный момент экзамен состоит из следующих разделов: два часа поиск и устранение неисправностей на незнакомом стенде, полчаса секция диагностики, пять с половиной часов секция настройки.
Для подготовки можно пройти тренинг Cisco CCIE 360, а также во многих Учебных Центрах существуют различные boot-camp-ы для подготовки.
Сертификация CCIE действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Design.
Направление в области проектирования вычислительных сетей на основе решений для сетевой инфраструктуры.
Первая ступень – CCDA состоит из одного единственного экзамена:
640-864 DESGN
Экзамен больше теоритический – что логично, так как это направление дизайна.
Для получения статуса – необходимо иметь сертификацию CCENT, либо CCNA R&S, либо любой CCIE.
Для подготовки к экзамену есть курс:
Designing for Cisco Internetwork Solutions (DESGN)
Статус CCDA действителен в течении трех лет, для ресертификации необходимо повторно сдавать CCDA, либо любой экзамен уровня Professional или Expert, либо любой технический специализации Cisco Specialist Certifications.

Вторая ступень – CCDP состоит из трех частей:
642-874 ARCH
642-902 ROUTE или 300-101 ROUTE
642-813 SWITCH или 300-115 SWITCH

Тут аналогично с Routing&Switching – вышли новые экзамены и до 29.01.2015 есть возможность сдавать как новые, так и старые версии экзаменов.
Для подготовки рекомендуется пройти соответствующие курсы:
Designing Cisco Network Service Architectures (ARCH)
Implementing Cisco IP Routing (ROUTE v2.0)
Implementing Cisco IP Switched Networks (SWITCH v2.0)
Для получения статуса CCDP необходимо наличие статуса CDDA и CCNA R&S или любой статус CCIE.
Статус CCDP действителен в течении трех лет. Для ресертификации необходимо сдать любой экзамен уровня Professional – либо уровня Expert.

Третья ступень – CCDE.
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 352-001 CCDE сдается в центрах тестирования Pearson VUE.
Лабораторная, сдается только в определенный день (где-то раз в три месяца) и только в Person Professional Center – ближайшие к нам находятся в Турции (Стамбул), Германии (Франкфурт), Греции (Афины), Франции (Париж), Англии (Лондон).
На данный момент актуальная версия экзамена v2. Лабораторная представляет собой восьми часовой экзамен, в котором нужно показать навыки анализа требования для дизайна, разработки дизайна, оптимизации и валидации дизайна по рекомендациям Cisco. При этом экзамен является чисто теоритическим, то есть в нем нет необходимости делать настройки – но есть необходимость писать объяснения, почему именно выбраны те или иные подходы при проектировании.
Сертификация CCDE действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Data Center.
Направление для специалистов, занимающихся оборудованием (как сетевым, так и серверным) Центров Обработки Данных. Курсы и экзамены, относящиеся к этой программе сертификации, посвящены фундаментальным концепциям организации Центров Обработки Данных, включая Unified computing и Unified fabric.
Первая ступень – CCNA Data Center.
Состоит из двух экзаменов:
640-911 DCICN
640-916 DCICT
Для подготовки есть курсы: 
Introducing Cisco Data Center Networking (DCICN)
Introducing Cisco Data Center Technologies (DCICT)
Все сертификаты действительны в течении трех лет, для продления срока действия статуса необходимо будет:
Сдать ещё раз экзамен CCNA DC, либо экзамен CCNA R&S, либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.
Вторая ступень – CCNP Data Center.
Данная ступень состоит из четырех экзаменов: 
Двух обязательных 642-999 DCUCI и 642-997 DCUFI
Двух по выбору из 642-998 DCUCD и 642-996 DCUFD или 642-035 DCUCT и 642-980 DCUFT

То есть обязательные экзамены по общим темам по сетевому оборудованию и серверному оборудованию ЦОДов, и на выбор одно из направлений – дизайн или поиск и устранение неисправностей.
Для подготовки можно пройти курсы:
Implementing Cisco Data Center Unified Computing (DCUCI)
Implementing Cisco Data Center Unified Fabric (DCUFI)
Designing Cisco Data Center Unified Computing (DCUCD)
Designing Cisco Data Center Unified Fabric (DCUFD)
Troubleshooting Cisco Data Center Unified Computing (DCUCT)
Troubleshooting Cisco Data Center Unified Fabric (DCUFT)

Для получения статуса CCNP DC необходимо наличие статуса CCNA DC или любой статус CCIE.
Статус CCNP DC действителен в течении трех лет. Для ресертификации необходимо сдать экзамен уровня Professional – либо уровня Expert.

Третья ступень – CCIE Data Center.
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 350-080 CCIE Data Center сдается в центрах тестирования Pearson VUE.
Лабораторный экзамен сдается в центрах тестирования Cisco: Индии (Бангалор), Китае (Пекин), Бельгии (Брюссель), ОАЭ (Дубай), США (Сан Хосе и РТП), Австралии (Сидней), Япония (Токио). В мобильной лаборатории возможности сдачи данного экзамена нет.
Лабораторная представляет собой восьми часовой экзамен, в котором нужно показать навыки 
Сертификация CCIE DC действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Security.
Направление для специалистов, занимающихся безопасностью в сетевой инфраструктуре, включая шлюзы безопасности, шлюзы сервисов, удаленный доступ, межсетевые подключения в сетях.
Первая ступень – CCNA Security.
Состоит из одного экзамена:
640-554 IINS

Для подготовки рекомендуется пройти курс: 
Implementing Cisco IOS Network Security (IINS)
Все сертификаты действительны в течении трех лет, для продления срока действия статуса необходимо будет:
Сдать экзамен Associate level (кроме ICND 1), либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.

Вторая ступень – CCNP Security.
Данная ступень состоит из четырех экзаменов: 
300-208 SISAS
300-206 SENSS
300-209 SIMOS
300-207 SITCS

Для подготовки можно пройти курсы:
Implementing Cisco Secure Access Solutions (SISAS)
Implementing Cisco Edge Network Security Solutions (SENSS)
Implementing Cisco Secure Mobility Solutions (SIMOS)
Implementing Cisco Threat Control Solutions (SITCS)

Для получения статуса CCNP Security необходимо наличие статуса CCNA Security или любой статус CCIE.
Статус CCNP Security действителен в течении трех лет. Для продления сертификации необходимо сдать экзамен уровня Professional – либо уровня Expert.

Третья ступень – CCIE Security.
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 350-018 CCIE Security сдается в центрах тестирования Pearson VUE.
У CCIE Security, как и у CCIE R&S – прохождение лабораторной возможно только в центрах тестирования Cisco. Первую попытку сдачи лабораторной необходимо сделать не позднее, чем полтора года от сдачи письменного экзамена – письменный экзамен действителен в течении трех лет (если за это время не будет сдана лабораторная – нужно будет заново сдавать письменный экзамен).
И так же два различных варианта сдачи.
Вариант первый: сдача экзамена зарубежом. Security можно сдать в Индии (Бангалор), Китае (Пекин), Бельгии (Брюссель), ОАЭ (Дубай), Гонконге, США (Сан Хосе и РТП), Австралии (Сидней) и Японии (Токио).
Вариант второй: мобильная лаборатория. 
Лабораторная представляет собой восьмичасовой экзамен, в котором нужно показать навыки работы с безопасностью сети в различных вариациях.
Сертификация CCIE Security действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Service Provider.
Направление для специалистов, работающих с сетями сервис провайдеров. Направление пришло на смену направлению CCIP (Cisco Certified Internetwork Professional).

Первая ступень – CCNA Service Provider.
Состоит из двух экзаменов:
640-875 SPNGN1
640-878 SPNGN2

Для подготовки рекомендуется пройти курс: 
Building Cisco Service Provider Next-Generation Networks, Part 1 (SPNGN1)
Building Cisco Service Provider Next-Generation Networks, Part 2 (SPNGN2)

Все сертификаты действительны в течении трех лет, для продления срока действия статуса необходимо будет:
Сдать экзамен Associate level (кроме ICND 1), либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.

Вторая ступень – CCNP Service Provider.

Данная ступень состоит из четырех экзаменов: 
642-883 SPROUTE
642-885 SPADVROUTE
642-887 SPCORE
642-889 SPEDGE

Для подготовки можно пройти курсы:
Deploying Cisco Service Provider Network Routing (SPROUTE)
Deploying Cisco Service Provider Advanced Routing (SPADVROUTE)
Implementing Cisco Service Provider Next-Generation Core Network Services (SPCORE)
Implementing Cisco Service Provider Next-Generation Edge Network Services (SPEDGE)

Для получения статуса CCNP SP необходимо выполнить одно из следующих требований: иметь статус CCNA SP, или CCNA R&S плюс один из старых экзаменов трека CCIP (QOS, BGP, MPLS, BGP+MPLS, ROUTE, BSCI), или иметь валидную сертификацию CCIP или любой CCIE.
Статус CCNP Service Provider действителен в течении трех лет. Для продления сертификации необходимо сдать экзамен уровня Professional – либо уровня Expert.

Третья ступень – CCIE Service Provider.
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 350-029 CCIE SP сдается в центрах тестирования Pearson VUE.
У CCIE SP, как и у CCIE R&S и Security– прохождение лабораторной возможно только в центрах тестирования Cisco (как в других странах, так и в мобильных лабораториях). Первую попытку сдачи лабораторной необходимо сделать не позднее, чем полтора года от сдачи письменного экзамена – письменный экзамен действителен в течении трех лет (если за это время не будет сдана лабораторная – нужно будет заново сдавать письменный экзамен).
И так же два различных варианта сдачи.
Вариант первый: сдача экзамена зарубежом. Security можно сдать в Индии (Бангалор), Китае (Пекин), Бельгии (Брюссель), ОАЭ (Дубай), Гонконге, США (Сан Хосе и РТП), Австралии (Сидней).
Вариант второй: мобильная лаборатория. 
Лабораторная представляет собой восьмичасовой экзамен, включающий в себя построение масштабируемой инфраструктурной сети сервис провайдера по данной спецификации. Так же необходимо обладать навыками настройки, поиска и устранения неисправностей, оптимизации и внедрению оборудования.
Сертификация CCIE SP действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Service Provider Operations.

Данная сертификация создана для заказчиков Cisco из числа операторов связи, а так же для партнеров компании Cisco и инженеров самой компании Cisco. Сертификация создана для специалистов, способных умело поддерживать операторские IP-сети нового поколения. Кроме того, эта сертификация удостоверяет наличие обширных теоретических знаний процессов оперативного управления, типовых решений и систем управления сетями.

Первая ступень – CCNA SP Operations.
Состоит из единственного экзамена:
640-760 SSPO

Для подготовки рекомендуется пройти курс: 
Supporting Cisco Service Provider IP NGN Operations (SSPO)

Предварительные требования: наличие статуса CCENT, CCNA или любого CCIE.
Все сертификаты действительны в течении трех лет, для продления срока действия статуса необходимо будет:
Сдать экзамен Associate level (кроме ICND 1), либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.

Вторая ступень – CCNP SP Operations.
Данная ступень состоит из четырех экзаменов:
642-770 OFCN
642-775 MSPRP
642-780 MSPVM
642-785 MSPQS

Для подготовки рекомендованы следующие курсы:
Operational Foundations for Cisco Service Provider Core Networks (OFCN)
Maintaining Cisco Service Provider Routing Protocols (MSPRP)
Maintaining Cisco Service Provider VPNs and MPLS Networks (MSPVM)
Maintaining Cisco Service Provider Quality of Service (MSPQS)

Статус CCNP SP Operations действителен в течении трех лет. Для продления сертификации необходимо сдать экзамен уровня Professional – либо уровня Expert.

Третья ступень – CCIE SP Operations…
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 350-060 CCIE SP Operations сдается в центрах тестирования Pearson VUE.
CCIE SP Operations Lab Exam можно сдавать только в четырех лабораториях Cisco: Индия (Бангалор), Гонконг, США (Сан Хосе и РТП).
Лабораторная представляет собой восьмичасовой экзамен, разбитый на две части. В первой части дается три часа на вопросы вопросы по операционным процессам в сети, управлению сетью и другими аспектами ITIL. Вторая пятичасовая часть представляет собой поиск и устранение неисправностей, диагностику, решением проблем, работу с сетевыми возможностями IOS и IOS-XR сетевого оборудования.
Сертификация CCIE SP Operations действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Video.

Самая молодая специализация – появилась относительно недавно, весной 2013 года. Сертификация создана для специалистов, работающих с системами видеоконференций и объединённых коммуникаций. В данном направление пока что только одна ступень – Associate. Второй ступени на данный момент нет, а третья ступень объединена с направление Voice и называется CCIE Collaboration.

Первая ступень – CCNA Video.
Состоит из двух экзаменов:
200-001 VIVND
640-461 ICOMM

Курсов по данному направлению три: 
Implementing Cisco Video Network Devices, Part 1 (VIVND1)
Implementing Cisco Video Network Devices, Part 2 (VIVND2)
Introducing Cisco Voice and Unified Communications Administration (ICOMM)

Все сертификаты действительны в течении трех лет, для продления срока действия статуса необходимо будет:
Сдать экзамен Associate level (кроме ICND 1), либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.

Voice.

Направление для специалистов, занимающихся сетями передачи голосовой информации, настройке и администрировании систем Cisco Unified Communication.

Первая ступень – CCNA Voice.
Состоит из одного экзамена:
640-461 ICOMM

Для подготовки рекомендуется пройти курс: 
Introducing Cisco Voice and Unified Communications Administration (ICOMM)
Требования для получения статуса CCNA Voice:
CCENT, CCNA R&S или любой экзамен уровня Expert.

Сертификация действительна в течении трех лет, для продления срока действия статуса необходимо будет:
Сдать экзамен Associate level (кроме ICND 1), либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.

Вторая ступень – CCNP Voice.
Данная ступень состоит из пяти экзаменов: 
642-437 CVOICE
642-447 CIPT1
642-457 CIPT2
642-427 TVOICE
642-467 CAPPS

Для подготовки рекомендуется пройти следующие курсы:
Implementing Cisco Voice Communications and QoS (CVOICE)
Implementing Cisco Unified Communications Manager, Part 1 (CIPT1)
Implementing Cisco Unified Communications Manager, Part 2 (CIPT2)
Troubleshooting Cisco Unified Communications (TVOICE)
Integrating Cisco Unified Communications Applications (CAPPS)

Для получения статуса CCNP Voice необходимо наличие статуса CCNA Voice или CCNA Video или любой статус CCIE.
Статус CCNP Security действителен в течении трех лет. Для продления сертификации необходимо сдать экзамен уровня Professional – либо уровня Expert.

Третья ступень – CCIE Collaboration.
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 400-051 CCIE Collaboration сдается в центрах тестирования Pearson VUE.
Сдача лабораторной возможно только в центрах тестирования Cisco в Индии (Бангалор), Китае (Пекин), Бельгии (Брюссель), ОАЭ (Дубай), Гонконге, США (Сан Хосе и РТП), Австралии (Сидней) и Японии (Токио).
Лабораторная представляет собой восьмичасовой экзамен, в котором нужно показать навыки поиску и устранению неисправностей, диагностики и комплексного подхода в сетях передачи видео и голоса.
Сертификация CCIE Collaboration действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Wireless.

Сертификация для специалистов, работающих с беспроводными технологиями в составе информационно-вычислительных сетях.

Первая ступень – CCNA Wireless.
Состоит из одного экзамена:
640-722 IUWNE

Для подготовки рекомендуется пройти курс: 
Implementing Cisco Unified Wireless Networking Essentials (IUWNE)

Предварительные требования: наличие статуса CCENT, CCNA R&S или любого CCIE.
Все сертификаты действительны в течении трех лет, для продления срока действия статуса необходимо будет:
Сдать экзамен Associate level (кроме ICND 1), либо сдать экзамен более высокой ступени (Professional или Expert), либо любой технический экзамен специализации Cisco Specialist Certifications.

Вторая ступень – CCNP Wireless.
Данная ступень состоит из четырех экзаменов: 
642-732 CUWSS
642-742 IUWVN
642-747 IUWMS
642-737 IAUWS

Для подготовки доступны следующие курсы:
Conducting Cisco Unified Wireless Site Survey (CUWSS)
Implementing Cisco Unified Wireless Voice Networks (IUWVN)
Implementing Cisco Unified Wireless Mobility Services (IUWMS)
Implementing Advanced Cisco Unified Wireless Security (IAUWS)

Для получения статуса
Статус CCNP Wireless действителен в течении трех лет. Для продления сертификации необходимо сдать экзамен уровня Professional – либо уровня Expert.

Третья ступень – CCIE Wireless.
Состоит из двух частей – письменного экзамена и лабораторной.
Письменный экзамен 350-050 CCIE Wireless сдается в центрах тестирования Pearson VUE.
Лабораторную CCIE Wireless можно сдавать только в четырех лабораториях Cisco: Бельгия (Брюссель), Австралия (Сидней), США (Сан Хосе и РТП).
Лабораторная представляет собой восьмичасовой экзамен, в котором необходимо выполнить серию заданий по настройке сетей по заданным спецификациям, такие как Enterprise WLAN solutions, such as implementing the Autonomous infrastructure, Unified infrastructure, Unified Controllers and AP’s, Unified WCS and Location and implementing Voice over Wireless.
Сертификация CCIE Wireless действительна в течении двух лет, для её продления необходимо сдать любой CCIE written exam или CCIE lab exam, или CCAr.

Specialist Certifications

Технические экзамены-специализации, не входящие в основную пирамиду сертификации Cisco, делятся на восемь направлений:

Data Center
Cisco Data Center Unified Computing Design Specialist
Специализация для инженеров, проектирующих масштабируемые, надежные и умные решения для виртуализации ЦОДов и работающих со следующими продуктами: Cisco UCS B and C series, серверной виртуализацией, серверными ОС.
Состоит из двух экзаменов:
642-998 DCUCD
642-999 DCUCI
Рекомендованные курсы:
Designing Cisco Data Center Unified Computing (DCUCD)
Implementing Cisco Data Center Unified Computing (DCUCI) v5.0

Cisco Data Center Unified Computing Support Specialist
Специализация для инженеров, поддерживающих с Cisco UCS и виртуальной инфраструктурой ЦОДа и работающих с продуктами: Cisco UCS B and C series, Cisco UCS инфраструктурой, оборудованием Cisco Nexus.
Для получения необходимо сдать два экзамена:
642-999 DCUCI
642-035 DCUCT
Рекомендованные курсы:
Implementing Cisco Data Center Unified Computing (DCUCI) v5.0
Troubleshooting Cisco Data Center Unified Computing (DCUCT) v5.0

Cisco Data Center Unified Fabric Design Specialist
Данное направление для специалистов, проектирующих и внедряющих конвергентные масштабируемые сети и виртуализацию в ЦОДе. 

Необходимые экзамены:
642-996 DCUFD 
642-997 DCUFI

Рекомендованные курсы:

Implementing Cisco Data Center Unified Fabric (DCUFI) v5.0
Designing Cisco Data Center Unified Fabric (DCUFD) v5.0

Cisco Data Center Unified Fabric Support Specialist
Данное направление для специалистов, внедряющих и поддерживающих конвергентные масштабируемые сети и виртуализацию в ЦОДе. 

Необходимые экзамены:
642-997 DCUFI 
642-980 DCUFT
Рекомендованные курсы:
Troubleshooting Cisco Data Center Unified Fabric (DCUFT) v5.0
Implementing Cisco Data Center Unified Fabric (DCUFI) v5.0

Internet of Things
Cisco Industrial Networking Specialist
Сертификация для специалистов в области работающих в области производства, управления технологическими процессами в нефтяной и газовой промышленности, облаживающими вычислительные системы и сети в данных комплексах.

Необходим всего один экзамен:
600-601 IMINS
Рекомендованный тренинг:
Managing Industrial Networks with Cisco Networking Technologies (IMINS)

Network Programmability
Cisco Business Application Engineer Specialist
Специализация для инженеров приложений, проектирования, разработки и создания бизнес-приложений.

Необходимые экзамены:
600-501 NPIBA 
600-509 NPIBAACI (в данный момент экзамен не доступен – должен появиться в конце 2014 года)
Рекомендованные курсы:
Integrating Business Applications with Network Programmability (NPIBA) 
Integrating Business Applications with Network Programmability for Cisco ACI (NPIBAACI) (в данный момент курс не доступен – должен появиться в конце 2014 года)

Cisco Network Programmability Developer Specialist
Специализация для разработчиков, которые сфокусированы на развитии сетевых технологий на уровнях работы приложений, как в кампусных сетях, так и в центрах обработки данных.

Необходимые экзамены:
600-502 NPDEV 
600-510 NPDEVACI (в данный момент экзамен не доступен – должен появиться в конце 2014 года)
Рекомендованные курсы:
Developing with Cisco Network Programmability (NPDEV)
Developing with Cisco Network Programmability for Cisco ACI (NPDEVACI) (в данный момент курс не доступен – должен появиться в конце 2014 года)
Требования для получения статуса: 
наличие статуса CCNA.

Cisco Network Programmability Design Specialist
Специализация для инженеров, которые имеют экспертизу в архитектурной области и в области разработки приложений.

Необходимые экзамены:
600-503 NPDES
600-511 NPDESACI (в данный момент экзамен не доступен – должен появиться в конце 2014 года)
Рекомендованные курсы:
Designing with Cisco Network Programmability (NPDES)
Designing with Cisco Network Programmability for Cisco ACI (NPDESACI) (в данный момент курс не доступен – должен появиться в конце 2014 года)
Требования для получения статуса: 
наличие статуса CCNP.

Cisco Network Programmability Engineer Specialist
Специализация для инженеров, которые разворачивают сетевые приложения в программируемой сетевой среде.

Необходимые экзамены:
600-504 NPENG 
600-512 NPENGACI (в данный момент экзамен не доступен – должен появиться в конце 2014 года)
Рекомендованные курсы:
Implementing Cisco Network Programmability (NPENG)
Implementing Cisco Network Programmability for Cisco ACI (NPENGACI) (в данный момент курс не доступен – должен появиться в конце 2014 года)
Требования для получения статуса: 
наличие статуса CCNP.

Operating System Software
Cisco IOS XR Specialist
Специализация для инженеров, работающих с Cisco IOS XR.

Необходимые экзамены:
644-906 IMTXR
Рекомендованные курсы:
Implementing and Maintaining Cisco Technologies Using IOS XR (IMTXR)

Security

Cisco Cybersecurity Specialist
Сертификация для специалистов по кибербезопасности, проактивного обнаружения атак, кибер угроз.

Необходимые экзамены:
600-199 SCYBER
Рекомендованные курсы:
Securing Cisco Networks with Threat Detection and Analysis (SCYBER)

Sourcefire Supplemental Training (статус в данный момент не доступен)
Специализация для инженеров, работающих с оборудованием компании Sourcelife (которую купила Cisco в 2013 году).

Service Provider
Cisco Service Provider Mobility CDMA to LTE Specialist
Сертификация для специалистов, работающих в сервис провайдерах с беспроводными технологиями CDMA, LTE и др.

Необходимые экзамены:
600-211 SPCDMA
600-212 SPLTE
Рекомендованные курсы:
Implementing Cisco Service Provider Mobility CDMA Networks (SPCDMA)
Implementing Cisco Service Provider Mobility LTE Networks (SPLTE)

Cisco Service Provider Mobility UMTS to LTE Specialist
Сертификация для специалистов, работающих в сервис провайдерах с беспроводными технологиями CDMA, LTE и др.

Необходимые экзамены:
600-210 SPUMTS
600-212 SPLTE
Рекомендованные курсы:
Implementing Cisco Service Provider Mobility UMTS Networks (SPUMTS)
Implementing Cisco Service Provider Mobility LTE Networks (SPLTE)

Video

Cisco Video Network Specialist
Направление для специалистов, работающих в традиционных аналоговых-видео сетях с возможностью на переквалицикацию на video-over-ip сети.

Необходимые экзамены:
200-001 VIVND
Рекомендованные курсы:
Implementing Cisco Video Network Devices, Part 1 (VIVND1)
Implementing Cisco Video Network Devices, Part 2 (VIVND2)

Cisco TelePresence Solutions Specialist
Направление для специалистов по работе с продуктами Cisco TelePresence.

Необходимые экзамены:
500-005 TX9KMAN
Рекомендованные курсы:
Cisco TX9000 Management (TX9KMAN) course

Business Transformation Certifications

Нетехнические сертификации, направленные на развитие компетенций, необходимых для согласований технических решений и бизнес-целей.
For IT Professionals
Необходимые экзамены:
820-427 BTBBSS
Рекомендованные курсы:
Building Business Specialist Skills (BTBBSS)

For Sales Professionals
Business Value Specialist
Необходимые экзамены:
810-420 BTUBVAF
820-421 BTASBVA
Рекомендованные курсы:
Understanding Cisco Business Value Analysis Fundamentals
Applying Cisco Specialized Business Value Analysis Skills

Certified Business Value Practitioner
Необходимые экзамены:
840-423 BTEABVD
Рекомендованные курсы:
На данный момент нет.
Предварительные требования:
Наличие специализации Business Value Specialist Certification

Transformative Architecture Specialist
Необходимые экзамены:
820-422 BTPBTAE
Рекомендованные курсы:
Performing Cisco Transformative Architecture Engagements

Technician Certifications (CCT)
Технические сертификации для специалистов по диагностике, восстановлению, ремонту и замены критических сетевых и системных устройств Cisco. 
CCT Data Center
Необходимые экзамены:
010-151 DCTECH
Рекомендованные курсы:
Supporting Cisco Data Center System Devices (DCTECH)

CCT Routing & Switching
Необходимые экзамены:
640-692 RSTECH
Рекомендованные курсы:
Supporting Cisco Routing and Switching Network Devices

CCT TelePresence
Необходимые экзамены:
640-792 TPTECH
Рекомендованные курсы:
Supporting Cisco TelePresence System Devices (