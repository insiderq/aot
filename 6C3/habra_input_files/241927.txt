   	Сегодня пришло письмо на почту о том, что 14 декабря 2014 года все бесплатные аккаунты закрываются:
This notice contains important information about the status and data on your Wuala account:
IF YOU USE FREE WUALA STORAGE, PLEASE READ THIS NOTICE CAREFULLY
We are providing you with notice that effective DECEMBER 31, 2014, we will be terminating all existing free storage.
Что предлагается взамен?

Ну, во-первых, ввели новый тарифный план на 5GB по цене 0,99 евро\месяц и 9 евро\год. А во-вторых, до 31 декабря при оплате на год любого тарифа Wuala удваивает покупку (дают промо код, можно либо удвоить место, либо подарить его кому-то другому).
