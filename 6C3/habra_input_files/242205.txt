   	Facebook выложил на гитхабе фреймворк OSquery, он осуществляет низкоуровневый мониторинг процессов в OS X и Linux и хранит их в виде SQL-таблиц. Такой способ по-своему удобен, ведь в запросе можно объединять разные таблицы.

Например, если мы хотим посмотреть названия, pid и порты всех процессов, которые прослушивают порты во всех интерфейсах, то составляем запросик

SELECT DISTINCT 
  process.name, 
  listening.port, 
  process.pid
FROM processes AS process
JOIN listening_ports AS listening
ON process.pid = listening.pid
WHERE listening.address = '0.0.0.0';
Или другой запрос, он выводит список всех демонов OS X, которые запускаются вместе с операционной системой и продолжают выполняться после этого. Возвращается название демона и путь к исполняемому файлу.

SELECT 
  name, 
  program || program_arguments AS executable 
FROM launchd 
WHERE 
  (run_at_load = 'true' AND keep_alive = 'true') 
AND 
  (program != '' OR program_arguments != '');
Подобными запросами отслеживаются работающие процессы, загруженные модули ядра и сетевые соединения.

Новые SQL-таблицы для OSquery довольно просто сделать самому. По желанию пополните 