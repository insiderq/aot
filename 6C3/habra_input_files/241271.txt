   	Доброго дня (вечера, ночи, утра — нужное подчеркнуть) всем хаброжителям! 
Этот пост посвящен мониторингу инстанса Caché с помощью SNMP. Наверняка, многие пользователи Caché этим в той или иной степени уже пользуются. Мониторинг через SNMP поддерживается в стандартной инсталляции Caché уже давно, однако далеко не все интересующие параметры доступны из «коробки». Например, хотелось бы видеть мониторинг количества CSP-сессий, более подробные сведения по использованию лицензии, частные KPI эксплуатируемой системы и т.п. 
В этой статье вы узнаете, как добавить свои параметры для мониторинга Caché с помощью SNMP.

Что уже имеем
Caché возможно мониторить с помощью SNMP. Cписок того, что уже поддерживается, можно увидеть в файлах каталога <Install_dir>/SNMP. Обычно там лежат 2 файла: ISC-CACHE.mib и ISC-ENSEMBLE.mib. Нас интересует файл для Caché — ISC-CACHE.mib. Например интересует, что мы можем получить относительно лицензий и сессий. В таблице приведены соответствующие OID’ы с учетом того, что иерархия берется от корня для Intersystems — 1.3.6.1.4.1.16563


OID
Название
Описание
Тип данных


.1.1.1.1.10
cacheSysLicenseUsed
The current number of licenses used on this Caché instance
INTEGER


.1.1.1.1.11
cacheSysLicenseHigh
The high-water mark for licenses used on this Caché instance
INTEGER


.1.2.16
cacheLicenseExceed
A request for a license has exceeded the licenses available or allowed
Trap message


.1.1.1.1.6
cacheSysCurUser
Current number of users on this Caché instance
INTEGER


В поставке отсутствуют многие важные показатели, например, количество CSP-сессий, не полна информация о лицензиях и, понятно, отсутствует мониторинг показателей, специфичных для приложений. 
Пример того, что мы хотели бы знать:

Сколько у нас пользователей CSP;
Ограничение нашей лицензии по пользователям;
Когда лицензионный ключ закончится.

Добавим также пару параметров для оценки производительности. Сами по себе параметры есть в поставке, но мы хотим знать их прирост за минуту, например:

Прирост «глобальных» ссылок за минуту;
Число выполненных команд за минуту;
Число вызовов рутин за минуту.


Как добавить «свои» параметры
Опираться можно на документ «Monitoring Caché using SNMP».
Наш тестовый инстанс (TEST) имеет версию Caché 2013.1.3. Операционная система — Linux CentOS release 6.5 (Final). Инсталляция Cache на Linux ОС описана на хабре. Полезной будет и родная документация от InterSystems.
Вот краткий план работ:

Создаем класс для сбора метрик;
Регистрируем и активируем новый класс в Caché с помощью ^%SYSMONMGR;
Создаем пользовательский MIB с помощью методов класса MonitorTools.SNMP. Для начала PEN (Private Enterprise Number) берем фиктивный 99990, впоследствии нужно зарегистрироваться в IANA. Эта процедура бесплатна, занимает неделю-другую и сопровождается недолгой перепиской вида «а зачем вам свой PEN понадобился»;
Запускаем сервис мониторинга с подключенным субагентом Caché;
Проверяем с помощью snmpwalk, что наши только что созданные пользовательские OID’ы нам доступны;
Добавляем наши OID’ы в стороннюю систему мониторинга. Например, возьмем Zabbix. Документацию по Zabbix можно найти здесь. По инсталляции и настройке Zabbix конкретно на CentOS можно посмотреть здесь. Убеждаемся, что мониторинг работает;
Добавляем запуск системного монитора в нашей области TEST при старте системы.

А теперь выполняем план по пунктам.
1. Создаем класс для сбора метрик
Класс сбора метрик наследуется от %Monitor.Adaptor. В студии переходим в область %SYS. Экспортируем пакет Monitor. Пакет содержит закрытый класс Sample, который из Студии не виден, но при экспорте в xml доступен.
Допустим, что наша рабочая область — это область TEST. Переходим в нее. Импортируем сюда класс Monitor.Sample. Создаем класс, в котором описываем реализацию снятия 6-ти метрик, указанных выше в разделе «Что уже имеем».
Class metrics.snmp.MetricsClass metrics.snmp.Metrics Extends %Monitor.Adaptor
{
/// Give the application a name. This allows you to group different
/// classes together under the same application level in the SNMP MIB.
/// The default is the same as the Package name.
Parameter APPLICATION = "Monitoring";
/// CSP sessions count
Property Sessions As %Monitor.Integer;
/// License user limit
Property KeyLicenseUnits As %Monitor.Integer;
/// License key expiration date
Property KeyExpirationDate As %Monitor.String;
/// Global references speed
Property GloRefSpeed As %Monitor.Integer;
/// Number of commands executed
Property ExecutedSpeed As %Monitor.Integer;
/// Number of routine loads/save
Property RoutineLoadSpeed As %Monitor.Integer;
/// The method is REQUIRED. It is where the Application Monitor
/// calls to collect data samples, which then get picked up by the
/// ^SNMP server process when requested.
Method GetSample() As %Status
{
      set ..Sessions = ..getSessions()
      set ..KeyLicenseUnits = ..getKeyLicenseUnits()
      set ..KeyExpirationDate = ..getKeyExpirationDate()
       
    set perfList = ..getPerformance()
      set ..GloRefSpeed = $listget(perfList,1)
      set ..ExecutedSpeed = $listget(perfList,2)
      set ..RoutineLoadSpeed = $listget(perfList,3)
      quit $$$OK
}
/// Get CSP sessions count
Method getSessions() As %Integer
{
    //q $system.License.CSPUsers()  // This method will only work if we dont’ use WebAddon
    // This will work even if  we use WebAddon
    set csp = ""
    try {
        set cn = $NAMESPACE
        znspace "%SYS"
        set db = ##class(SYS.Stats.Dashboard).Sample()
        set csp = db.CSPSessions
        znspace cn
    } catch e {
        set csp = "0"
    }
    quit csp
}
/// Get license user's power
Method getKeyLicenseUnits() As %Integer
{
    quit $system.License.KeyLicenseUnits()
}
/// Get license expiration date in human-readable format
Method getKeyExpirationDate() As %String
{
    quit $zdate($system.License.KeyExpirationDate(),3)
}
/// Get performance metrics (gloref, rourines etc.)
Method getPerformance(param As %String) As %Integer
{
    set cn = $NAMESPACE
    znspace "%SYS"
    set m = ##class(SYS.Monitor.SystemSensors).%New()
    do m.GetSensors()
    znspace cn
    quit $listbuild(m.SensorReading("GlobalRefsPerMin"), 
                    m.SensorReading("RoutineCommandsPerMin"), 
                    m.SensorReading("RoutineLoadsPerMin"))
}
}


Проверяем, что нужные нам данные вызовом метода GetSample() выгружаются:
Проверка работы GetSample()TEST>s x=##class(metrics.snmp.Metrics).%New()

TEST>d x.GetSample()

TEST>zw x
x=<OBJECT REFERENCE>[2@metrics.snmp.Metrics]
+----------------- general information — | oref value: 2
| class name: metrics.snmp.Metrics
| reference count: 2
+----------------- attribute values — | ExecutedSpeed = 1155679
| GloRefSpeed = 171316
| KeyExpirationDate = «2014-10-11»
| KeyLicenseUnits = 100
| RoutineLoadSpeed = 186
| Sessions = 1

2. Регистрируем и активируем новый класс в Caché с помощью ^%SYSMONMGR
Открываем терминал и переходим в область TEST:
Подключаем системный монитор[root@server ~]# csession test -U test
Узел: server, Экземпляр: TEST

TEST>d ^%SYSMONMGR
1. Выбираем пункт 5, Manage Application Monitor.
2. Выбираем пункт 2, Manage Monitor Classes.
3. Выбираем пункт 3, Register Monitor System Classes. Наблюдаем за компиляцией:

Экспорт в XML начался в 08/18/2014 16:00:51
Экспортируемый класс: Monitor.Sample
Экспорт успешно завершен.

Загрузка началась в 08/18/2014 16:00:51
Загрузка файла /opt/intersystems/ensemble/mgr/Temp/45DDB3FppRHCuw.stream как xml
Импорт класса: Monitor.Sample
Компиляция класса: Monitor.Sample
Компиляция программы:: Monitor.Sample.G1.MAC
Компиляция таблицы: Monitor.Sample
Компиляция программы: Monitor.Sample.1
Загрузка успешно завершена.

4. Выбираем пункт 1, Activate/Deactivate Monitor Class
Class??
 Num MetricsClassName Activated
 1) %Monitor.System.AuditCount N
 …
 15) metrics.snmp.Metrics N
 
Class? 15 metrics.snmp.Metrics
Activate class? Yes => Yes

5. Выбираем пункт 6, Exit
6. Еще раз выбираем пункт 6, Exit
7. Выбираем пункт 1, Start/Stop System Monitor
8. Выбираем пункт 2, Stop System Monitor
Stopping System Monitor… System Monitor not running!

9. Выбираем пункт 1, Start System Monitor
Starting System Monitor… System Monitor started
10. Выбираем пункт 3, Exit
11. Выбираем пункт 4, View System Monitor State
 Component State
System Monitor OK
%SYS.Monitor.AppMonSensor

3. Создаем пользовательский MIB
Пользовательский MIB создается с помощью методов класса MonitorTools.SNMP. Для примера PEN (Private Enterprise Number) возьмем фиктивныЙ, 99990, впоследствии PEN нужно зарегистрировать в IANA. Посмотреть уже зарегистрированные номера можно здесь. Например, PEN InterSystems имеет номер 16563.
16563
InterSystems
Robert Davis
rdavis&intersystems.com
Для создания MIB-файла мы будем использовать класс MonitorTools.SNMP, в частности, метод CreateMIB(). Этот метод принимает на вход 10 аргументов:


Имя и тип аргумента
Описание
Что подставляем


AppName As %String
Имя приложения
Значение параметра APPLICATION класса metrics.snmp.Metrics — Monitoring


Namespace As %String
Наша область
TEST


EntID As %Integer
PEN компании
99990 (Fiction)


AppID As %Integer
OID приложения внутри компании
42


Company As %String
имя компании (прописными)
fiction


Prefix As %String
префикс всех создаваемых нами SNMP-объектов
fiction


CompanyShort As %String
краткий префикс компании (прописными)
fict


MIBname As %String
имя MIB-файла
ISC-TEST


Contact As %String
контактная информация (в частности, адрес)
Оставляем значение по умолчанию: Earth, Russia, Somewhere in the forests, Subject: ISC-TEST.mib


List As %Boolean
Аналог verbose. Показывать прогресс задания по созданию MIB-файла
1


Собственно, создание MIB-файла:
CreateMIB()%SYS>d ##class(MonitorTools.SNMP).CreateMIB(«Monitoring»,«TEST»,99990,42,«fiction»,«fict»,«fiction»,«ISC-TEST»,,1)
Create SNMP structure for Application — Monitoring
 Group — Metrics
 ExecutedSpeed = Integer
 GloRefSpeed = Integer
 KeyExpirationDate = String
 KeyLicenseUnits = Integer
 RoutineLoadSpeed = Integer
 Sessions = Integer

Create MIB file for Monitoring
 Generate table Metrics
 Add object ExecutedSpeed, Type = Integer
 Add object GloRefSpeed, Type = Integer
 Add object KeyExpirationDate, Type = String
 Add object KeyLicenseUnits, Type = Integer
 Add object RoutineLoadSpeed, Type = Integer
 Add object Sessions, Type = Integer
MIB done.

В каталоге <Install_dir>/mgr/TEST появился новый MIB ISC-TEST.mib.

4. Запускаем сервис мониторинга с подключенным субагентом Caché
Открываем панель управления System Administration -> Security -> Services -> %Service_Monitor (нажать) -> Service Enabled (отметить).


Также указываем, что хотим стартовать SNMP-субагента при старте Caché (нажимаем Configure Monitor Settings):

В Linux CentOS для SNMP мониторинга мы используем пакет net-snmp. Ставим его, конфигурируем на использование субагентов и на то, что общение мастер-агента и субагентов будет осуществляться через стандартный для этого порт 705. 

[root@server ~]# grep -i agentx /etc/services
agentx 705/tcp # AgentX
agentx 705/udp # AgentX

Маленькую статью по конфигурационному файлу snmpd.conf в дополнение к ману можно посмотреть на cyberciti. Вот наша конечная настройка:
Наш snmpd.conf[root@server~]# yum install net-snmp
[root@server ~]# grep '^[^#]' /etc/snmp/snmpd.conf
master agentx
agentXSocket TCP:localhost:705
com2sec local localhost public
group MyRWGroup v1 local
group MyRWGroup v2c local
group MyRWGroup usm local
view all included .1 80
view system included .iso.org.dod
access MyROGroup "" any noauth exact all none none
access MyRWGroup "" any noauth exact all all none
syslocation server (edit /etc/snmp/snmpd.conf)
syscontact Root <root@localhost> (configure /etc/snmp/snmp.local.conf)
dontLogTCPWrappersConnects yes

Рестартуем в Linux демонов snmpd и snmptrapd. Стартуем ^SNMP сервис для начала работы SNMP-субагента от Caché:
%SYS>d start^SNMP

5. Проверяем, что наши, только что созданные, пользовательские OID’ы доступны.
Это можно сделать с помощью snmpwalk — покажем OID, отображающий количество CSP-сессий:

[root@server mgr]# snmpwalk -On -v 2c -c public localhost 1.3.6.1.4.1.99990
.1.3.6.1.4.1.99990.42.1.1.1.1.4.84.69.83.84 = INTEGER: 448035
.1.3.6.1.4.1.99990.42.1.1.1.2.4.84.69.83.84 = INTEGER: 64190
.1.3.6.1.4.1.99990.42.1.1.1.3.4.84.69.83.84 = STRING: «2014-10-11»
.1.3.6.1.4.1.99990.42.1.1.1.4.4.84.69.83.84 = INTEGER: 200
.1.3.6.1.4.1.99990.42.1.1.1.5.4.84.69.83.84 = INTEGER: 93
.1.3.6.1.4.1.99990.42.1.1.1.6.4.84.69.83.84 = INTEGER: 1

В файле ISC-TEST.mib указана последовательность наших OID:

FictMetricsR ::=
 SEQUENCE {
 fictExecutedSpeed Integer32,
 fictGloRefSpeed Integer32,
 fictKeyExpirationDate DisplayString,
 fictKeyLicenseUnits Integer32,
 fictRoutineLoadSpeed Integer32,
 fictSessions Integer32
 }

Соответственно, например, число сессий — это последний OID 1.3.6.1.4.1.99990.42.1.1.1.6. Можно сравнить с количеством сессий, показываемых SMP-дашбордом:


6. Добавляем наши OID’ы в стороннюю систему мониторинга.
Например, возьмем Zabbix. Документацию по Zabbix можно найти здесь. По инсталляции и настройке Zabbix конкретно на CentOS можно посмотреть здесь. Zabbix был выбран как система, позволяющая не только рисовать графики, но и мониторить Plain Text (в нашем случае, это срок действия лицензии и мощность лицензии по пользователям). После добавления наших 6-ти метрик к Items нашего локального хоста и создания 4-х графиков и 2-х PlainText параметров (как элементов screen) видим такую картину (приведен пример небольшой «живой» системы):

Вверху — информация о том, когда лицензия заканчивается, и сколько мы имеем лицензионных слотов. Графики говорят сами за себя.

7. Добавляем запуск системного монитора в нашей области TEST при старте системы
Есть неплохой документ об использовании пользовательских рутин, срабатывающих при старте и остановке Caché. Называются они соответственно %ZSTART и %ZSTOP.
Что нас во всем этом интересует, так это чтобы при старте поднимать в пользовательской области TEST системный монитор (^%SYSMONMGR). По умолчанию этот монитор стартует только в области %SYS. Соответственно, рассматривать будем только программу ^%ZSTART. Исходник %ZSTART.mac (создаем и сохраняем ее в области %SYS).
Наш автостарт%ZSTART; User startup routine.
SYSTEM;
    ; Cache starting
    do $zu(9,"","Starting System Monitor in TEST namespace by ^%ZSTART...Begin")
    znspace "TEST"
    set sc = ##class(%SYS.Monitor).Start()
    do $system.OBJ.DisplayError(sc)
    if (sc = 1) {
    do $zutil(9,"","Starting System Monitor in TEST namespace by ^%ZSTART...OK")
    } else {
    do $zutil(9,"","Starting System Monitor in TEST namespace by ^%ZSTART...ERROR")
    }
    ; Starting SNMP
    znspace "%SYS"
    do start^SNMP
    quit
LOGIN;
    ; a user logs into Cache (user account or telnet)
    quit
JOB;
    ; JOB'd process begins
    quit
CALLIN;
    ; a process enters via CALLIN interface
    quit


Рестартуем (по возможности) Caché, чтобы убедиться, что сбор SNMP-статистики после рестарта Caché продолжается. 
На этом все. Возможно, у кого-то будут замечания по выбору параметров мониторинга или коду, но задача ставилась показать возможность такого мониторинга в принципе, а добавить нужный параметр или отрефакторить код всегда можно в дальнейшем.