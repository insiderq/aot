		В последнее время все больше и больше постов посвящается поделкам на основе плат Arduino. Я тоже хочу рассказать о нескольких проектах, созданных на основе этих плат. Не являюсь автором этих проектов и имею к ним косвенное отношение. Но настоящие авторы дали мне добро на использование их разработок в этой публикации. Дело в том, что они не имеют никакого отношения к IT и, соответственно, не имеют аккаунтов на Хабре и его отпрыске Geektimes. 

Привожу дословное описание проектов от авторов.

В быту
Управление кухонным освещением от Engineer200
Проект призван начать решать проблемы поиска выключателей, забывания отключения и необходимости зажмуривания. Первая часть реализует плавное включение подсветки рабочей поверхности на кухне и подсветки внутри шкафа, например, под мойку обычно приходится нырять с фонариком. Подсветка рабочего поверхности кухни реализуется размещением светодиодной ленты на нижней части верхних шкафов. Короткие нажатия на кнопку просто включают и выключают свет. Длительное нажатие включает режим изменения уровня света, постепенное притухание, по достижению ноля, снова включается ярко. Когда кнопка отпущена — уровень света запоминается и дальнейшие короткие нажатия на кнопку, включают свет до запомненного уровня. Внутри шкафа свет плавно включается-выключается по срабатыванию геркона, встраиваемого в стенку шкафа. Дальнейшее развитие предполагает подключение датчиков присутствия, освещённости, увеличение количества каналов управления, осуществление декоративной подсветки стеклянных полок и дверей. 

Программа тестировалась на Arduino UNO с контроллером AFMega8A-PU, а реализована на Arduino PRO Mini.

Принципиальная схема:



Файл проекта в программе FLProg.
 
Скетч для ArduinoIDE 

bool _gtv1 = 0;
bool _gtv2 = 0;
int _gtv3 = 255;
int _gtv4 = 0;
bool _gtv9 = 0;
bool _gtv11 = 1;
int _gtv12 = 255;
bool _gtv13 = 0;
bool _gtv14 = 0;
bool _gtv15 = 0;
bool _gtv16 = 0;
bool _gtv17 = 0;
bool _gtv18 = 0;
int _gtv19 = 255;
int _gtv20 = 255;
int _gtv21 = 255;
int _gtv22 = 255;
int _gtv23 = 0;
int _gtv24 = 0;
bool _gtv5 = 0;
bool _gtv6 = 0;
int _gtv8 = 0;
int _gtv10 = 0;
int _gtv25 = 0;
bool _tim1I = 0;
bool _tim1O = 0;
unsigned long _tim1P = 0UL;
bool _tim3I = 0;
bool _tim3O = 0;
unsigned long _tim3P = 0UL;
bool _tim4I = 0;
bool _tim4O = 0;
unsigned long _tim4P = 0UL;
int _swi3;
bool _trgs1 = 0;
int _swi5;
bool _trgs2 = 0;
int _swi6;
bool _trgs3 = 0;
bool _gen1I = 0;
bool _gen1O = 0;
unsigned long _gen1P = 0UL;
int _swi4;
bool _count1I = 0;
int _count1P = 0;
bool _gen3I = 0;
bool _gen3O = 0;
unsigned long _gen3P = 0UL;
int _swi7;
bool _count3I = 0;
int _count3P = 0;
bool _gen4I = 0;
bool _gen4O = 0;
unsigned long _gen4P = 0UL;
int _swi8;
bool _count4I = 0;
int _count4P = 0;
bool _gen2I = 0;
bool _gen2O = 0;
unsigned long _gen2P = 0UL;
int _swi1;
int _swi2;
bool _count2I = 0;
int _count2P = 0;
bool _gen5I = 0;
bool _gen5O = 0;
unsigned long _gen5P = 0UL;
int _swi9;
int _swi10;
bool _count5I = 0;
int _count5P = 0;
bool _gen6I = 0;
bool _gen6O = 0;
unsigned long _gen6P = 0UL;
int _swi11;
int _swi12;
bool _count6I = 0;
int _count6P = 0;
bool _tim2I = 0;
bool _tim2O = 0;
unsigned long _tim2P = 0UL;
bool _tim5I = 0;
bool _tim5O = 0;
unsigned long _tim5P = 0UL;
bool _tim6I = 0;
bool _tim6O = 0;
unsigned long _tim6P = 0UL;
bool _gen7I = 0;
bool _gen7O = 0;
unsigned long _gen7P = 0UL;
int _swi13;
bool _count7I = 0;
int _count7P = 0;
bool _gen8I = 0;
bool _gen8O = 0;
unsigned long _gen8P = 0UL;
int _swi14;
bool _count8I = 0;
int _count8P = 0;
bool _gen9I = 0;
bool _gen9O = 0;
unsigned long _gen9P = 0UL;
int _swi15;
bool _count9I = 0;
int _count9P = 0;
bool _bounseInput2S = 0;
bool _bounseInput2O = 0;
unsigned long _bounseInput2P = 0UL;
bool _bounseInput4S = 0;
bool _bounseInput4O = 0;
unsigned long _bounseInput4P = 0UL;
bool _bounseInput7S = 0;
bool _bounseInput7O = 0;
unsigned long _bounseInput7P = 0UL;
bool _bounseInput13S = 0;
bool _bounseInput13O = 0;
unsigned long _bounseInput13P = 0UL;
bool _bounseInput8S = 0;
bool _bounseInput8O = 0;
unsigned long _bounseInput8P = 0UL;
bool _bounseInput12S = 0;
bool _bounseInput12O = 0;
unsigned long _bounseInput12P = 0UL;
void setup()
{
  pinMode(8, INPUT);
  digitalWrite(8, HIGH);
  pinMode(12, INPUT);
  digitalWrite(12, HIGH);
  pinMode(13, INPUT);
  pinMode(2, INPUT);
  digitalWrite(2, HIGH);
  pinMode(4, INPUT);
  digitalWrite(4, HIGH);
  pinMode(7, INPUT);
  digitalWrite(7, HIGH);

  _bounseInput12O =  digitalRead(12);
  _bounseInput8O =  digitalRead(8);
  _bounseInput2O =  digitalRead(2);
  _bounseInput4O =  digitalRead(4);
  _bounseInput7O =  digitalRead(7);
  _bounseInput13O =  digitalRead(13);
}
void loop()
{
  bool  _bounceTmp2 =  (digitalRead (2));

  if (_bounseInput2S)
  {
    if (millis() >= (_bounseInput2P + 40))
    {
      _bounseInput2O = _bounceTmp2;
      _bounseInput2S = 0;
    }
  }
  else
  {
    if (_bounceTmp2 != _bounseInput2O )
    {
      _bounseInput2S = 1;
      _bounseInput2P = millis();
    }
  }
  bool  _bounceTmp4 =  (digitalRead (4));

  if (_bounseInput4S)
  {
    if (millis() >= (_bounseInput4P + 40))
    {
      _bounseInput4O = _bounceTmp4;
      _bounseInput4S = 0;
    }
  }
  else
  {
    if (_bounceTmp4 != _bounseInput4O )
    {
      _bounseInput4S = 1;
      _bounseInput4P = millis();
    }
  }
  bool  _bounceTmp7 =  (digitalRead (7));

  if (_bounseInput7S)
  {
    if (millis() >= (_bounseInput7P + 40))
    {
      _bounseInput7O = _bounceTmp7;
      _bounseInput7S = 0;
    }
  }
  else
  {
    if (_bounceTmp7 != _bounseInput7O )
    {
      _bounseInput7S = 1;
      _bounseInput7P = millis();
    }
  }
  bool  _bounceTmp13 =  (digitalRead (13));

  if (_bounseInput13S)
  {
    if (millis() >= (_bounseInput13P + 40))
    {
      _bounseInput13O = _bounceTmp13;
      _bounseInput13S = 0;
    }
  }
  else
  {
    if (_bounceTmp13 != _bounseInput13O )
    {
      _bounseInput13S = 1;
      _bounseInput13P = millis();
    }
  }
  bool  _bounceTmp8 =  (digitalRead (8));

  if (_bounseInput8S)
  {
    if (millis() >= (_bounseInput8P + 40))
    {
      _bounseInput8O = _bounceTmp8;
      _bounseInput8S = 0;
    }
  }
  else
  {
    if (_bounceTmp8 != _bounseInput8O )
    {
      _bounseInput8S = 1;
      _bounseInput8P = millis();
    }
  }
  bool  _bounceTmp12 =  (digitalRead (12));

  if (_bounseInput12S)
  {
    if (millis() >= (_bounseInput12P + 40))
    {
      _bounseInput12O = _bounceTmp12;
      _bounseInput12S = 0;
    }
  }
  else
  {
    if (_bounceTmp12 != _bounseInput12O )
    {
      _bounseInput12S = 1;
      _bounseInput12P = millis();
    }
  }



  if (_bounseInput8O)
  {
    _tim1O  = 1;
    _tim1I  = 1;
  }
  else
  {
    if (_tim1I)
    {
      _tim1I = 0;
      _tim1P = millis();
    }
    else
    {
      if (_tim1O)
      { if ( _isTimer(_tim1P, 100)) _tim1O = 0;
      }
    }
  }
  if (_bounseInput12O)
  {
    _tim3O  = 1;
    _tim3I  = 1;
  }
  else
  {
    if (_tim3I)
    {
      _tim3I = 0;
      _tim3P = millis();
    }
    else
    {
      if (_tim3O)
      { if ( _isTimer(_tim3P, 100)) _tim3O = 0;
      }
    }
  }
  if (_bounseInput13O)
  {
    _tim4O  = 1;
    _tim4I  = 1;
  }
  else
  {
    if (_tim4I)
    {
      _tim4I = 0;
      _tim4P = millis();
    }
    else
    {
      if (_tim4O)
      { if ( _isTimer(_tim4P, 100)) _tim4O = 0;
      }
    }
  }
  _gtv11 = _tim1O;
  _gtv15 = _tim3O;
  _gtv16 = _tim4O;
  if ( (_gtv9) || ( ( (!(_gtv9)) && (_gtv11) && (!(_gtv1)) ) && ((_gtv3) == (0)) ) ) _trgs1 = 1;
  if ( ( (!(_gtv9)) && (_gtv11) && (!(_gtv1)) ) && ((_gtv3) > (0)) ) _trgs1 = 0;
  if (_trgs1)
  {
    _swi3 = _gtv12;
  }
  else
  {
    _swi3 = 0;
  }
  _gtv3 = _swi3;
  if ( (_gtv17) || ( ( (!(_gtv17)) && (_gtv15) && (!(_gtv13)) ) && ((_gtv21) == (0)) ) ) _trgs2 = 1;
  if ( ( (!(_gtv17)) && (_gtv15) && (!(_gtv13)) ) && ((_gtv21) > (0)) ) _trgs2 = 0;
  if (_trgs2)
  {
    _swi5 = _gtv19;
  }
  else
  {
    _swi5 = 0;
  }
  _gtv21 = _swi5;
  if ( (_gtv18) || ( ( (!(_gtv18)) && (_gtv16) && (!(_gtv14)) ) && ((_gtv22) == (0)) ) ) _trgs3 = 1;
  if ( ( (!(_gtv18)) && (_gtv16) && (!(_gtv14)) ) && ((_gtv22) > (0)) ) _trgs3 = 0;
  if (_trgs3)
  {
    _swi6 = _gtv20;
  }
  else
  {
    _swi6 = 0;
  }
  _gtv22 = _swi6;
  if (_gtv9)
  {
    _swi4 = 0;
  }
  else
  {
    _swi4 = 2;
  }

  if ( ((_gtv4) < (_gtv3)) || ((_gtv4) > (_gtv3)) )
  {
    if (! _gen1I)
    {
      _gen1I = 1;
      _gen1O = 1;
      _gen1P = millis();
    }
  }
  else
  {
    _gen1I = 0 ;
    _gen1O = 0;
  }
  if (_gen1I )
  {
    if ( _isTimer ( _gen1P , _swi4 ))
    {
      _gen1P = millis();
      _gen1O = ! _gen1O;
    }
  }

  if (_gen1O)
  {
    if (! _count1I)
    {
      if ( ((_gtv4) < (_gtv3)) && (!((_gtv4) > (_gtv3))) ) {
        _count1P = _count1P + 1;
      } else {
        _count1P = _count1P - 1;
      };
      _count1I = 1;
    }
  }
  else
  {
    _count1I = 0;
  }
  if (_count1P < 0 ) _count1P = 0;
  if (0) _count1P = 0;
  analogWrite(9, _count1P);
  _gtv4 = _count1P;
  if (_gtv17)
  {
    _swi7 = 0;
  }
  else
  {
    _swi7 = 2;
  }

  if ( ((_gtv23) < (_gtv21)) || ((_gtv23) > (_gtv21)) )
  {
    if (! _gen3I)
    {
      _gen3I = 1;
      _gen3O = 1;
      _gen3P = millis();
    }
  }
  else
  {
    _gen3I = 0 ;
    _gen3O = 0;
  }
  if (_gen3I )
  {
    if ( _isTimer ( _gen3P , _swi7 ))
    {
      _gen3P = millis();
      _gen3O = ! _gen3O;
    }
  }

  if (_gen3O)
  {
    if (! _count3I)
    {
      if ( ((_gtv23) < (_gtv21)) && (!((_gtv23) > (_gtv21))) ) {
        _count3P = _count3P + 1;
      } else {
        _count3P = _count3P - 1;
      };
      _count3I = 1;
    }
  }
  else
  {
    _count3I = 0;
  }
  if (_count3P < 0 ) _count3P = 0;
  if (0) _count3P = 0;
  analogWrite(10, _count3P);
  _gtv23 = _count3P;
  if (_gtv18)
  {
    _swi8 = 0;
  }
  else
  {
    _swi8 = 2;
  }

  if ( ((_gtv24) < (_gtv22)) || ((_gtv24) > (_gtv22)) )
  {
    if (! _gen4I)
    {
      _gen4I = 1;
      _gen4O = 1;
      _gen4P = millis();
    }
  }
  else
  {
    _gen4I = 0 ;
    _gen4O = 0;
  }
  if (_gen4I )
  {
    if ( _isTimer ( _gen4P , _swi8 ))
    {
      _gen4P = millis();
      _gen4O = ! _gen4O;
    }
  }

  if (_gen4O)
  {
    if (! _count4I)
    {
      if ( ((_gtv24) < (_gtv22)) && (!((_gtv24) > (_gtv22))) ) {
        _count4P = _count4P + 1;
      } else {
        _count4P = _count4P - 1;
      };
      _count4I = 1;
    }
  }
  else
  {
    _count4I = 0;
  }
  if (_count4P < 0 ) _count4P = 0;
  if (0) _count4P = 0;
  _gtv24 = _count4P;
  analogWrite(11, _count4P);

  if (_gtv9)
  {
    if (! _gen2I)
    {
      _gen2I = 1;
      _gen2O = 1;
      _gen2P = millis();
    }
  }
  else
  {
    _gen2I = 0 ;
    _gen2O = 0;
  }
  if (_gen2I )
  {
    if ( _isTimer ( _gen2P , 7 ))
    {
      _gen2P = millis();
      _gen2O = ! _gen2O;
    }
  }

  if (_gen2O)
  {
    if (! _count2I)
    {
      _count2P = _count2P + 1;
      _count2I = 1;
    }
  }
  else
  {
    _count2I = 0;
  }
  if (_count2P < 0 ) _count2P = 0;
  if (_gtv2) _count2P = 0;
  if (((300) - (_count2P)) > (255))
  {
    _swi1 = 255;
  }
  else
  {
    _swi1 = (300) - (_count2P);
  }
  if (_gtv9)
  {
    _swi2 = _swi1;
  }
  else
  {
    _swi2 = _gtv12;
  }
  _gtv2 = (_count2P) >= (300);
  _gtv12 = _swi2;

  if (_gtv17)
  {
    if (! _gen5I)
    {
      _gen5I = 1;
      _gen5O = 1;
      _gen5P = millis();
    }
  }
  else
  {
    _gen5I = 0 ;
    _gen5O = 0;
  }
  if (_gen5I )
  {
    if ( _isTimer ( _gen5P , 7 ))
    {
      _gen5P = millis();
      _gen5O = ! _gen5O;
    }
  }

  if (_gen5O)
  {
    if (! _count5I)
    {
      _count5P = _count5P + 1;
      _count5I = 1;
    }
  }
  else
  {
    _count5I = 0;
  }
  if (_count5P < 0 ) _count5P = 0;
  if (_gtv5) _count5P = 0;
  if (((300) - (_count5P)) > (255))
  {
    _swi9 = 255;
  }
  else
  {
    _swi9 = (300) - (_count5P);
  }
  if (_gtv17)
  {
    _swi10 = _swi9;
  }
  else
  {
    _swi10 = _gtv19;
  }
  _gtv5 = (_count5P) >= (300);
  _gtv19 = _swi10;

  if (_gtv18)
  {
    if (! _gen6I)
    {
      _gen6I = 1;
      _gen6O = 1;
      _gen6P = millis();
    }
  }
  else
  {
    _gen6I = 0 ;
    _gen6O = 0;
  }
  if (_gen6I )
  {
    if ( _isTimer ( _gen6P , 7 ))
    {
      _gen6P = millis();
      _gen6O = ! _gen6O;
    }
  }

  if (_gen6O)
  {
    if (! _count6I)
    {
      _count6P = _count6P + 1;
      _count6I = 1;
    }
  }
  else
  {
    _count6I = 0;
  }
  if (_count6P < 0 ) _count6P = 0;
  if (_gtv6) _count6P = 0;
  if (((300) - (_count6P)) > (255))
  {
    _swi11 = 255;
  }
  else
  {
    _swi11 = (300) - (_count6P);
  }
  if (_gtv18)
  {
    _swi12 = _swi11;
  }
  else
  {
    _swi12 = _gtv20;
  }
  _gtv6 = (_count6P) >= (300);
  _gtv20 = _swi12;
  _gtv1 = _gtv11;
  _gtv13 = _gtv15;
  _gtv14 = _gtv16;
  if (_gtv11)
  {
    _tim2O  = 1;
    _tim2I  = 1;
  }
  else
  {
    if (_tim2I)
    {
      _tim2I = 0;
      _tim2P = millis();
    }
    else
    {
      if (_tim2O)
      { if ( _isTimer(_tim2P, 1000)) _tim2O = 0;
      }
    }
  }
  if (_gtv15)
  {
    _tim5O  = 1;
    _tim5I  = 1;
  }
  else
  {
    if (_tim5I)
    {
      _tim5I = 0;
      _tim5P = millis();
    }
    else
    {
      if (_tim5O)
      { if ( _isTimer(_tim5P, 1000)) _tim5O = 0;
      }
    }
  }
  if (_gtv16)
  {
    _tim6O  = 1;
    _tim6I  = 1;
  }
  else
  {
    if (_tim6I)
    {
      _tim6I = 0;
      _tim6P = millis();
    }
    else
    {
      if (_tim6O)
      { if ( _isTimer(_tim6P, 1000)) _tim6O = 0;
      }
    }
  }
  _gtv9 =  (!(_tim2O)) && (!(_gtv11)) ;
  _gtv17 =  (!(_tim5O)) && (!(_gtv15)) ;
  _gtv18 =  (!(_tim6O)) && (!(_gtv16)) ;
  if (_bounseInput2O)
  {
    _swi13 = 255;
  }
  else
  {
    _swi13 = 0;
  }

  if ( ((_gtv8) < (_swi13)) || ((_gtv8) > (_swi13)) )
  {
    if (! _gen7I)
    {
      _gen7I = 1;
      _gen7O = 1;
      _gen7P = millis();
    }
  }
  else
  {
    _gen7I = 0 ;
    _gen7O = 0;
  }
  if (_gen7I )
  {
    if ( _isTimer ( _gen7P , 3 ))
    {
      _gen7P = millis();
      _gen7O = ! _gen7O;
    }
  }

  if (_gen7O)
  {
    if (! _count7I)
    {
      if ( ((_gtv8) < (_swi13)) && (!((_gtv8) > (_swi13))) ) {
        _count7P = _count7P + 1;
      } else {
        _count7P = _count7P - 1;
      };
      _count7I = 1;
    }
  }
  else
  {
    _count7I = 0;
  }
  if (_count7P < 0 ) _count7P = 0;
  if (0) _count7P = 0;
  analogWrite(3, _count7P);
  _gtv8 = _count7P;
  if (_bounseInput4O)
  {
    _swi14 = 255;
  }
  else
  {
    _swi14 = 0;
  }

  if ( ((_gtv10) < (_swi14)) || ((_gtv10) > (_swi14)) )
  {
    if (! _gen8I)
    {
      _gen8I = 1;
      _gen8O = 1;
      _gen8P = millis();
    }
  }
  else
  {
    _gen8I = 0 ;
    _gen8O = 0;
  }
  if (_gen8I )
  {
    if ( _isTimer ( _gen8P , 3 ))
    {
      _gen8P = millis();
      _gen8O = ! _gen8O;
    }
  }

  if (_gen8O)
  {
    if (! _count8I)
    {
      if ( ((_gtv10) < (_swi14)) && (!((_gtv10) > (_swi14))) ) {
        _count8P = _count8P + 1;
      } else {
        _count8P = _count8P - 1;
      };
      _count8I = 1;
    }
  }
  else
  {
    _count8I = 0;
  }
  if (_count8P < 0 ) _count8P = 0;
  if (0) _count8P = 0;
  analogWrite(5, _count8P);
  _gtv10 = _count8P;
  if (_bounseInput7O)
  {
    _swi15 = 255;
  }
  else
  {
    _swi15 = 0;
  }

  if ( ((_gtv25) < (_swi15)) || ((_gtv25) > (_swi15)) )
  {
    if (! _gen9I)
    {
      _gen9I = 1;
      _gen9O = 1;
      _gen9P = millis();
    }
  }
  else
  {
    _gen9I = 0 ;
    _gen9O = 0;
  }
  if (_gen9I )
  {
    if ( _isTimer ( _gen9P , 3 ))
    {
      _gen9P = millis();
      _gen9O = ! _gen9O;
    }
  }

  if (_gen9O)
  {
    if (! _count9I)
    {
      if ( ((_gtv25) < (_swi15)) && (!((_gtv25) > (_swi15))) ) {
        _count9P = _count9P + 1;
      } else {
        _count9P = _count9P - 1;
      };
      _count9I = 1;
    }
  }
  else
  {
    _count9I = 0;
  }
  if (_count9P < 0 ) _count9P = 0;
  if (0) _count9P = 0;
  analogWrite(6, _count9P);
  _gtv25 = _count9P;



}
bool _isTimer(unsigned long startTime, unsigned long period )
{
  unsigned long endTime;
  endTime = startTime + period;
  return (millis() >= endTime);
}


 
Готовое устройство:



Видео в процессе сборки:



Видео законченного результата:



На работе
Cварочный контроллер для сварки арматуры от ALEX

Написал программу сварочного контроллера для сварки арматуры. Запуск от ножной педали при наличии прутка арматуры.

Регулировка трех технологических параметров:

«Время ПРИЖИМА», «Время СВАРКИ»
«Время ПРОКОВКИ»
Дополнительно предусмотрен переключатель «АВТОМАТ- РУЧНОЙ» 

Принципиальная схема:



Файл проекта в программе FLProg.

Скетч для ArduinoIDE 

bool _gtv1 = 0;
bool _tim1I = 0;
bool _tim1O = 0;
unsigned long _tim1P = 0UL;
bool _gen1I = 0;
bool _gen1O = 0;
unsigned long _gen1P = 0UL;
bool _trgr1 = 0;
bool _gen2I = 0;
bool _gen2O = 0;
unsigned long _gen2P = 0UL;
bool _tim2I = 0;
bool _tim2O = 0;
unsigned long _tim2P = 0UL;
bool _tim3I = 0;
bool _tim3O = 0;
unsigned long _tim3P = 0UL;
bool _gen4I = 0;
bool _gen4O = 0;
unsigned long _gen4P = 0UL;
void setup()
{
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);

}
void loop()
{



  if ( ( (digitalRead (2))) && ( (digitalRead (3))) )
  {
    if (_tim1I)
    {
      if ( _isTimer(_tim1P, nil)) _tim1O = 1;
    }
    else
    {
      _tim1I = 1;
      _tim1P = millis();
    }
  }
  else
  {
    _tim1O = 0;
    _tim1I = 0;
  }

  if (_tim1O)
  {
    if (! _gen1I)
    {
      _gen1I = 1;
      _gen1O = 1;
      _gen1P = millis();
    }
  }
  else
  {
    _gen1I = 0 ;
    _gen1O = 0;
  }
  if (_gen1I &&  _gen1O)   _gen1O = !( _isTimer ( _gen1P , 10 ));


  if ( ( ( (digitalRead (2))) && (!( (digitalRead (4)))) ) || ( ( (digitalRead (4))) && (_gtv1) ) )
  {
    if (! _gen2I)
    {
      _gen2I = 1;
      _gen2O = 1;
      _gen2P = millis();
    }
  }
  else
  {
    _gen2I = 0 ;
    _gen2O = 0;
  }
  if (_gen2I &&  _gen2O)   _gen2O = !( _isTimer ( _gen2P , 10 ));

  if (_gen2O) _trgr1 = 0;
  if (_gen1O) _trgr1 = 1;
  if (_trgr1)
  {
    if (_tim2I)
    {
      if ( _isTimer(_tim2P, (( (analogRead (1))) / (8)) + (100))) _tim2O = 1;
    }
    else
    {
      _tim2I = 1;
      _tim2P = millis();
    }
  }
  else
  {
    _tim2O = 0;
    _tim2I = 0;
  }

  if (_tim2O)
  {
    if (! _gen4I)
    {
      _gen4I = 1;
      _gen4O = 1;
      _gen4P = millis();
    }
  }
  else
  {
    _gen4I = 0 ;
    _gen4O = 0;
  }
  if (_gen4I &&  _gen4O)   _gen4O = !( _isTimer ( _gen4P , (( (analogRead (0))) / (8)) + (100) ));

  if (_gen4O)
  {
    _tim3O  = 1;
    _tim3I  = 1;
  }
  else
  {
    if (_tim3I)
    {
      _tim3I = 0;
      _tim3P = millis();
    }
    else
    {
      if (_tim3O)
      { if ( _isTimer(_tim3P, (( (analogRead (2))) / (8)) + (100))) _tim3O = 0;
      }
    }
  }
  digitalWrite(5, _trgr1);
  digitalWrite(6, _gen4O);
  _gtv1 = (!(_tim3O));



}
bool _isTimer(unsigned long startTime, unsigned long period )
{
  unsigned long endTime;
  endTime = startTime + period;
  return (millis() >= endTime);
}



В машине
Управление осветительными приборами автомобиля от karam.d
Блок управления светом на автомобиле.

При включении режима автомат (габариты, ходовые огни, ближний свет) включаются и выключаются автоматически. Как поступил сигнал с тахометра вкл режим авт и загораются габариты и диод (автомат вкл). После пропадания сигнала (через 10 сек) вык режим автомат, а габариты гаснут после выкл зажигания. При появлении сигнала с датчика скорости вкл ходовые огни или ближний свет фар (в зависимости от освещённости ). При пропадании (через 2 мин) они выключаются.

Когда блок включен, контролируется целостность ламп ближнего света, напряжение на борту (мин, норма, макс) сопровождается световой и звуковой сигнализацией (звуковой сигнал можно отключить нажатием кнопки). Если подключить поворотники (к А7), то они будут сопровождаться звуковым сигналом. Проект собран на Arduino Nano. Работает уже месяц как часы.

Принципиальная схема:



Файл проекта в программе FLProg.

Схема в diptrace.

Скетч для ArduinoIDE 

bool _gtv1 = 0;
bool _gtv5 = 0;
bool _gtv6 = 0;
bool _gtv7 = 0;
bool _gtv8 = 0;
bool _gtv9 = 0;
bool _gtv10 = 0;
bool _gtv11 = 0;
bool _gtv2 = 0;
bool _gtv3 = 0;
bool _gtv4 = 0;
bool _gtv12 = 0;
bool _gtv13 = 0;
bool _gtv14 = 0;
bool _bounse1S = 0;
bool _bounse1O = 0;
unsigned long _bounse1P = 0UL;
bool _tim1I = 0;
bool _tim1O = 0;
unsigned long _tim1P = 0UL;
bool _tim6I = 0;
bool _tim6O = 0;
unsigned long _tim6P = 0UL;
bool _trgrt2 = 0;
bool _trgrt2I = 0;
bool _gen4I = 0;
bool _gen4O = 0;
unsigned long _gen4P = 0UL;
bool _count3I = 0;
int _count3P = 0;
bool _tim10I = 0;
bool _tim10O = 0;
unsigned long _tim10P = 0UL;
bool _tim9I = 0;
bool _tim9O = 0;
unsigned long _tim9P = 0UL;
bool _tim2I = 0;
bool _tim2O = 0;
unsigned long _tim2P = 0UL;
bool _trgrt1 = 0;
bool _trgrt1I = 0;
bool _tim8I = 0;
bool _tim8O = 0;
unsigned long _tim8P = 0UL;
bool _gen2I = 0;
bool _gen2O = 0;
unsigned long _gen2P = 0UL;
bool _count1I = 0;
int _count1P = 0;
bool _tim3I = 0;
bool _tim3O = 0;
unsigned long _tim3P = 0UL;
bool _gen3I = 0;
bool _gen3O = 0;
unsigned long _gen3P = 0UL;
bool _count2I = 0;
int _count2P = 0;
bool _tim4I = 0;
bool _tim4O = 0;
unsigned long _tim4P = 0UL;
bool _tim5I = 0;
bool _tim5O = 0;
unsigned long _tim5P = 0UL;
bool _tim7I = 0;
bool _tim7O = 0;
unsigned long _tim7P = 0UL;
bool _gen1I = 0;
bool _gen1O = 0;
unsigned long _gen1P = 0UL;
bool _bounse3S = 0;
bool _bounse3O = 0;
unsigned long _bounse3P = 0UL;
bool _bounse2S = 0;
bool _bounse2O = 0;
unsigned long _bounse2P = 0UL;
bool _trgt1 = 0;
bool _trgt1I = 0;
void setup()
{
  pinMode(2, INPUT);
  pinMode(4, INPUT);
  pinMode(7, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);

}
void loop()
{



  bool  _bounceTmp1 = ( (analogRead (2))) > (500);

  if (_bounse1S)
  {
    if (millis() >= (_bounse1P + 40))
    {
      _bounse1O = _bounceTmp1;
      _bounse1S = 0;
    }
  }
  else
  {
    if (_bounceTmp1 != _bounse1O )
    {
      _bounse1S = 1;
      _bounse1P = millis();
    }
  }
  _gtv1 = _bounse1O;
  if ( (digitalRead (2))) {
    if (_trgrt2I) {
      _trgrt2 = 0;
    } else {
      _trgrt2 = 1;
      _trgrt2I = 1;
    }
  } else {
    _trgrt2 = 0;
    _trgrt2I = 0;
  };
  if (_trgrt2)
  {
    _tim1O  = 1;
    _tim1I  = 1;
  }
  else
  {
    if (_tim1I)
    {
      _tim1I = 0;
      _tim1P = millis();
    }
    else
    {
      if (_tim1O)
      { if ( _isTimer(_tim1P, 10000)) _tim1O = 0;
      }
    }
  }
  if ( (!(_gtv1)) && (_tim1O) )
  {
    if (_tim6I)
    {
      if ( _isTimer(_tim6P, 3000)) _tim6O = 1;
    }
    else
    {
      _tim6I = 1;
      _tim6P = millis();
    }
  }
  else
  {
    _tim6O = 0;
    _tim6I = 0;
  }

  if ( (_tim6O) ^ (_gtv14) )
  {
    if (! _gen4I)
    {
      _gen4I = 1;
      _gen4O = 1;
      _gen4P = millis();
    }
  }
  else
  {
    _gen4I = 0 ;
    _gen4O = 0;
  }
  if (_gen4I )
  {
    if ( _isTimer ( _gen4P , 1 ))
    {
      _gen4P = millis();
      _gen4O = ! _gen4O;
    }
  }

  if (_gen4O)
  {
    if (! _count3I)
    {
      _count3P = _count3P + 1;
      _count3I = 1;
    }
  }
  else
  {
    _count3I = 0;
  }
  if (_count3P < 0 ) _count3P = 0;
  _gtv5 = _tim6O;
  digitalWrite(7, _tim6O);
  _gtv14 =  _count3P  >=  920;
  if (_tim6O) {
    analogWrite(3, (map((_count3P), (0), (1023), (0), (254))));
  }
  if ( (_gtv5) && (( (analogRead (5))) > (500)) )
  {
    if (_tim10I)
    {
      if ( _isTimer(_tim10P, 2005)) _tim10O = 1;
    }
    else
    {
      _tim10I = 1;
      _tim10P = millis();
    }
  }
  else
  {
    _tim10O = 0;
    _tim10I = 0;
  }
  digitalWrite(8, _tim10O);
  _gtv11 = _tim10O;
  if ( (_gtv5) && (( (analogRead (4))) > (500)) )
  {
    if (_tim9I)
    {
      if ( _isTimer(_tim9P, 2005)) _tim9O = 1;
    }
    else
    {
      _tim9I = 1;
      _tim9P = millis();
    }
  }
  else
  {
    _tim9O = 0;
    _tim9I = 0;
  }
  _gtv10 = _tim9O;
  digitalWrite(12, _tim9O);
  if ( (digitalRead (4))) {
    if (_trgrt1I) {
      _trgrt1 = 0;
    } else {
      _trgrt1 = 1;
      _trgrt1I = 1;
    }
  } else {
    _trgrt1 = 0;
    _trgrt1I = 0;
  };
  if (_trgrt1)
  {
    _tim2O  = 1;
    _tim2I  = 1;
  }
  else
  {
    if (_tim2I)
    {
      _tim2I = 0;
      _tim2P = millis();
    }
    else
    {
      if (_tim2O)
      { if ( _isTimer(_tim2P, 120000)) _tim2O = 0;
      }
    }
  }
  if ( (_gtv5) && (_tim2O) )
  {
    if (_tim8I)
    {
      if ( _isTimer(_tim8P, 2000)) _tim8O = 1;
    }
    else
    {
      _tim8I = 1;
      _tim8P = millis();
    }
  }
  else
  {
    _tim8O = 0;
    _tim8I = 0;
  }

  if ( (_tim8O) && (!(_gtv3)) && (!(_gtv4)) )
  {
    if (! _gen2I)
    {
      _gen2I = 1;
      _gen2O = 1;
      _gen2P = millis();
    }
  }
  else
  {
    _gen2I = 0 ;
    _gen2O = 0;
  }
  if (_gen2I )
  {
    if ( _isTimer ( _gen2P , 1 ))
    {
      _gen2P = millis();
      _gen2O = ! _gen2O;
    }
  }

  if (_gen2O)
  {
    if (! _count1I)
    {
      _count1P = _count1P + 1;
      _count1I = 1;
    }
  }
  else
  {
    _count1I = 0;
  }
  if (_count1P < 0 ) _count1P = 0;
  if (_gtv3) _count1P = 0;
  _gtv6 = _tim8O;
  _gtv4 =  _count1P  >=  920;
  analogWrite(5, (map((_count1P), (0), (1023), (0), (254))));
  if (( (analogRead (6))) > (700))
  {
    _tim3O  = 1;
    _tim3I  = 1;
  }
  else
  {
    if (_tim3I)
    {
      _tim3I = 0;
      _tim3P = millis();
    }
    else
    {
      if (_tim3O)
      { if ( _isTimer(_tim3P, 120000)) _tim3O = 0;
      }
    }
  }

  if ( ( (_tim3O) && (_gtv5) && (_gtv6) ) ^ (_gtv12) )
  {
    if (! _gen3I)
    {
      _gen3I = 1;
      _gen3O = 1;
      _gen3P = millis();
    }
  }
  else
  {
    _gen3I = 0 ;
    _gen3O = 0;
  }
  if (_gen3I )
  {
    if ( _isTimer ( _gen3P , 1 ))
    {
      _gen3P = millis();
      _gen3O = ! _gen3O;
    }
  }

  if (_gen3O)
  {
    if (! _count2I)
    {
      _count2P = _count2P + 1;
      _count2I = 1;
    }
  }
  else
  {
    _count2I = 0;
  }
  if (_count2P < 0 ) _count2P = 0;
  if (!(_gtv13)) _count2P = 0;
  _gtv13 = _tim3O;
  _gtv3 =  (_tim3O) && (_gtv5) && (_gtv6) ;
  _gtv12 =  _count2P  >=  920;
  analogWrite(9, (map((_count2P), (0), (1023), (0), (254))));
  analogWrite(6, (map((_count2P), (0), (1023), (0), (254))));
  if (( (analogRead (0))) > (600.00))
  {
    if (_tim4I)
    {
      if ( _isTimer(_tim4P, 4000)) _tim4O = 1;
    }
    else
    {
      _tim4I = 1;
      _tim4P = millis();
    }
  }
  else
  {
    _tim4O = 0;
    _tim4I = 0;
  }
  if (( (analogRead (1))) < (500.00))
  {
    if (_tim5I)
    {
      if ( _isTimer(_tim5P, 4000)) _tim5O = 1;
    }
    else
    {
      _tim5I = 1;
      _tim5P = millis();
    }
  }
  else
  {
    _tim5O = 0;
    _tim5I = 0;
  }
  _gtv8 = _tim4O;
  _gtv7 = _tim5O;
  digitalWrite(10,  (_gtv8) || (_gtv7) );
  _gtv9 = (!  (_gtv8) || (0) );
  digitalWrite(11,  (_gtv7) || (_gtv9) );
  if ( ( (_gtv8) || (_gtv7) || (_gtv10) || (_gtv11) ) ^ ( ( (_gtv8) || (_gtv7) || (_gtv10) || (_gtv11) ) && (_gtv2) ) )
  {
    if (_tim7I)
    {
      if ( _isTimer(_tim7P, 2000)) _tim7O = 1;
    }
    else
    {
      _tim7I = 1;
      _tim7P = millis();
    }
  }
  else
  {
    _tim7O = 0;
    _tim7I = 0;
  }

  if (_tim7O)
  {
    if (! _gen1I)
    {
      _gen1I = 1;
      _gen1O = 1;
      _gen1P = millis();
    }
  }
  else
  {
    _gen1I = 0 ;
    _gen1O = 0;
  }
  if (_gen1I )
  {
    if ( _isTimer ( _gen1P , 1000 ))
    {
      _gen1P = millis();
      _gen1O = ! _gen1O;
    }
  }
  bool  _bounceTmp3 = ( (analogRead (7))) > (500);

  if (_bounse3S)
  {
    if (millis() >= (_bounse3P + 40))
    {
      _bounse3O = _bounceTmp3;
      _bounse3S = 0;
    }
  }
  else
  {
    if (_bounceTmp3 != _bounse3O )
    {
      _bounse3S = 1;
      _bounse3P = millis();
    }
  }
  digitalWrite(13,  (_gen1O) || (_bounse3O) );
  bool  _bounceTmp2 = ( (analogRead (3))) > (500);

  if (_bounse2S)
  {
    if (millis() >= (_bounse2P + 40))
    {
      _bounse2O = _bounceTmp2;
      _bounse2S = 0;
    }
  }
  else
  {
    if (_bounceTmp2 != _bounse2O )
    {
      _bounse2S = 1;
      _bounse2P = millis();
    }
  }
  bool  _tmp1 = _bounse2O;
  if (_tmp1)  {
    if (! _trgt1I) _trgt1 = ! _trgt1;
  }
  _trgt1I = _tmp1;
  _gtv2 = _trgt1;



}
bool _isTimer(unsigned long startTime, unsigned long period )
{
  unsigned long endTime;
  endTime = startTime + period;
  return (millis() >= endTime);
}




Сразу оговорюсь. Наверняка у многих возникнет вопрос о читаемости скетчей для Arduino IDE. Скетчи созданы автоматически программой с помощью графической среды программирования. Как я уже говорил, авторы проектов не имеют никакого отношения к IT и к программированию. Это умные и хозяйственные люди с прямыми руками и четким пониманием, что им нужно. 
