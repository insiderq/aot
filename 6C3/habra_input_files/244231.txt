   	

Мы сами еще не до конца в это верим — Payler получил Премию Рунета 2014 в области экономики и бизнеса. 

Наш процессинг признали лучшим финансовым проектом в российском сегменте Интернета. Сказать, что мы гордимся такой честью – ничего не сказать, еще год назад мы даже не думали о премиях, а сейчас нас признали лучшими. Мы строили с нуля продукт, бренд, команду и хотим сказать огромное спасибо всем, кто участвовал и участвует в проекте:
 — Арт-директору Александру Илюхину и Руслану Мирсалихову, за проектирование и точный выверенный дизайн;
 — Техническому директору Геннадию Трафименкову, Станиславу Басенко, Степану, Максиму Павлову, Андрею Крисанову, Дмитрию Филюстину, Алексею и Александру Сычевым, Никите Северу, Ивану из Биллинграда, за управление и разработку такого сложного и многофункционального продукта;
 — Нашему секретному системному администратору Станиславу — за бесперебойность и за то, что он качественно описал техническое позиционирование нашего продукта, после чего мы попали в номинанты премии;
 — Сэйлзам, Александру Таирову, Вадиму Имангалиеву и Сергею Чурносову — за то, что оперативно и без устали подключали и подключают новых клиентов;
 — Клиентскому менеджменту Марине Жирновой и Александре Быстровой, за оперативную и добросовестную работу с документами наших клиентов;
 — Маркетологу Олегу Вахромееву за PR и помощь в продвижении нашего бренда; 
 — Всем кто, когда либо работал и работает в Polonium Arts — за фундаментальные знания в области разработки и менеджмента, которые мы заложили в основу продукта Payler;
 — Партнерам Виталию Х., Роману С., Дмитрию Г., Денису М., Алексею Т., за развитие нашей группы компаний и будущий выход на международный уровень.

Будущий год мы посвящаем развитию новых технологий и новых направлений. 

Наша цель достичь уровня НКО (Небанковская Кредитная Организация) или даже стать Банком. Как минимум. 
Спасибо большое экспертам, друзьям, партнерам по маркетингу, команде Хабрахабра, конкурентам и даже тем, кто нас еще не знает — спасибо всем, вы про нас еще услышите в наступающим году! 

