   	Закончилась очередная неделя и в России ожидаемо подорожали iУстройства, снова с нами сборник всех издательств, вышла новая версия Unity, внутриигровые покупки обложили НДС. Еще больше новостей – в очередном дайджесте. 






Обходим защиту iOS клиента Dropbox

Что в первую очередь сделает iOS разработчик, если перед ним встанет задача спрятать пользовательские данные от любопытных глаз? Конечно же, встроит экран парольной защиты. Особо хитрый разработчик даже не будет хранить установленный пользователем пароль в NSUserDefaults, а аккуратно спрячет его в связку ключей — в этом случае приложение в глазах как его, так и заказчика, сразу же перемещается в разряд «сверхзащищенных».




Документ с информацией о мобильных издателях и возможностях самиздата

Я вновь открыл возможность любому редактировать документ. Надеюсь, что это принесет намного больше пользы, чем вреда. Конечно, в идеале, вынести все это в формат wiki. Если кто захочет это оформить — смело забирайте всю информацию и переносите в нормальный вариант.




Внедряем материальный дизайн

Буквально вчера мы в Surfingbird обновили дизайн приложения и сегодня, по свежим следам, хотелось бы поделиться впечатлениями от перехода на material design.

 iOS

(+15) Apple подняла цены в России
(+12) Стэнфордские курсы «Разработка iOS приложений» — неавторизованный конспект лекций на русском языке
(+9) Многоликие функции Swift
Каникулы iTunes Connect: с 22 по 29 декабря
Apple ищет разработчика VR

 Android

(+14) Автоматическое тестирование Android приложений c любовью
(+9) Маршруты на картах Google в Android-приложении
(+6) Google: рекомендации для приложений в Google Play
(+4) Запуск приложений в Android Virtual Device на удаленном Linux-сервере
(+3) Модификация исходного кода android-приложения с использованием apk-файла
Google Play заработал на Кубе

 Windows Phone

(+20) Интервью с Руди Хайном: девелопер бестселлеров делится секретами успеха своих приложений
(+11) Обнови приложение и получи комплексный пакет продвижения

 Tizen

(+11) Победители конкурса Tizen App Challenge

 Разработка

(+110) Интерактивное голосовое редактирование текста с помощью новых речевых технологий от Яндекса
(+21) 10 языков, на которые стоит перевести вашу мобильную игру
(+17) Опыт использования MVVM в реальных проектах
(+15) PathSense – новый SDK для определения местонахождения без использования GPS
(+14) Редизайн приложения РЖД: концепт
(+11) 5 принципов тестирования мобильных приложений
(+7) Тестирование компонентов в Unity Engine
(+5) Первый опыт в игровой разработке. Ошибки и выводы
В Unity 4.6 появился редактор UI
Это не разработчики тормозят, это процесс так устроен
«Платформа Софт» выпустила библиотеку для считывания данных кредитных карт через NFC
12 языков программирования, за которые платят больше всего
Правила создания free-to-play игр
Как UX влияет на рост продукта — на примерах Dropbox, Pinterest, Airbnb и Kim Kardashian

 Маркетинг и монетизация

(+32) Внутриигровые покупки («Мейл.Ру Геймз») обложили НДС
Отчет «Эффективность мобильной рекламы на смартфонах и планшетах в третьем квартале 2014 года»
Twitter начнет отслеживать установленные у пользователей приложения
Batch Insights: все пуш-уведомления на свете
Opera запускает магазин приложений и игр для операторов
Как мобильность меняет розничную торговлю
Fetch связала рекламу и действия пользователей
Machine Zone потратила на показы TV-рекламы — $4,5 млн
Как анализировать инди-игры?

 Устройства

(+52) Что такое «российская разработка»?
(+30) Клавиатура Октодон: Метания и метаморфозы
(+15) Wi-Fi-лампочка на базе модуля WizFi250
С широко открытыми глазами: почему разработчикам не следует списывать Google Glass со счетов


← Дайджест за прошлую неделю