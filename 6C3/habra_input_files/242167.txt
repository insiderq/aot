   	Вчера мне пришло уведомление о том, что открыто beta-тестирование профайлера от SensioLabs - blackfire.io




Для того чтоб им воспользоваться нам понадобиться аккаунт SensioLabsConnect.
Если вы ранее не регистрировали для участия в бета тесте то на сайте вы увидите:
You have been registered for the beta.
We will let you know once your registration has been validated.
Так что придёт немного подождать пока Вам дадут доступ.


Состоит этот продукт из 3-х частей:
 — Probe: «зонд», серверная часть.
 — Agent: «агент», серверная часть.
 — Companion: «спутник», плагин для GoogleChrome доступный в Google WebStore.

Процесс установки.
Подготовка
Ubuntu\Demian:
Регистрируем ключ для packagecloud:
curl -s https://packagecloud.io/gpg.key | sudo apt-key add -

Добавляем репозиторий и обновим список пакетов:
echo "deb http://packages.blackfire.io/debian any main" | sudo tee /etc/apt/sources.list.d/blackfire.list
sudo apt-get update


OSX:
brew tap blackfireio/homebrew-blackfire


Red Hat
sudo yum install pygpgme
curl "http://packages.blackfire.io/fedora/blackfire.repo" | sudo tee /etc/yum.repos.d/blackfire.repo


Probe:
У себя на сайте они пишут что не поддерживают php5.6, но у меня никаких проблем не возникло.
Так же советуют для избежания конфликтов отключить XDebug и\или XHProf
Ubuntu\Debian
sudo apt-get install blackfire-php


OSX:
PHP 5.6:
brew install blackfire-php56

PHP 5.5:
brew install blackfire-php55

PHP 5.4:
brew install blackfire-php54

PHP 5.3:
brew install blackfire-php53


Red Hat:
sudo yum install blackfire-php


Agent:

Ubuntu\Debian
sudo apt-get install blackfire-agent


OSX:
brew install blackfire-agent

На OSX «Agent» не запускается автоматом, что бы добавить его в автозагрузку нам понадобится:
ln -sfv /usr/local/opt/blackfire-agent/*.plist ~/Library/LaunchAgents
launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.blackfire-agent.plist

Перезапуск после обновления конфига:
launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.blackfire-agent.plist
launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.blackfire-agent.plist


Red Hat:

sudo yum install blackfire-agent


Теперь необходимо настроить его указав Server Id и Server Token:
blackfire-agent -register

Их можно найти на вкладке Account: blackfire.io/account/agents

Потом его желательно перезапустить:
sudo /etc/init.d/blackfire-agent restart


Companion:

chrome.google.com/webstore/detail/blackfire/miefikpgahefdbcgoiicnmpbeeomffld

Подробное руководство по установке можно найти тут: blackfire.io/getting-started/

Теперь можно начать профилирование.


Сейчас доступно 20 слотов для хранения результатов. 

А вот так выглядит результат профилирования, очень похоже на xhprof.

