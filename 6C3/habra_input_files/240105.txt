		Введение
Всем привет. 
Вначале небольшой экскурс.
Эта статья является своеобразным переводом обучающей программы от Jesse Freeman. Ему спасибо за мою теперешнюю возможность получить инвайт. Остальным спасибо за понимание, что это моя первая статья. 
В утопическом мире населенном только хабралюдьми, я постараюсь сделать так, чтобы за этой статьей последовало еще 9, но мир не идеален, так что пока не известно как всё пойдет. Вроде всё, тогда поехали.

Импортирование спрайтов
Чтобы импортировать спрайты в ваш проект в Unity достаточно просто перетащить необходимые файлы в любую выбранную вами папку во вкладке Project. Внизу иллюстрация.
GIF
 Как видите всё до безобразия просто. Теперь давайте на примере какого-нибудь спрайта рассмотрим настройки. Перейдем к Assets\Artwork\Sprites\Player и кликнем по текстуре игрока. В инспекторе откроется меню. Если по каким-либо причинам в открывшемся меню напротив вкладки Texture Type указано Texture, то у вас сейчас используется 3D-проект, это легко исправить просто создав новый проект, не забыв переключить на 2D. Внизу иллюстрация.
Как исправить
Если всё правильно
Отлично, теперь перетащим спрайт игрока на сцену, и что мы видим спрайт стал gameobject'ом.


Следующее свойство спрайтов о котором я расскажу называется Pixels To Units. Как вы видите спрайт игрока 80x80 пикселей, а в Unity 100x100 пикселей соответствуют одному квадратному метру, поэтому перейдя к настройкам импорта спрайта в строке Pixels To Units вы увидите значение 100 и это означает, что спрайт отображается корректно.Давайте изменим значение на 80, в таком случае вы увидите, что спрайт игрока увеличился, теперь он занимает равно один юнит квадратный.
GIF
Примечание от переводчика для более корректного отображения спрайтов рекомендуется использовать в этом поле значение 100. 

Следующее свойство спрайтов о котором я расскажу называется Pivot. Если кратко — это точка привязки. По умолчанию установлена на центр. Вы можете изменить ее в случае необходимости, но в большинстве случае привязка в центре подходит. Здесь обойдемся без иллюстраций.
Едем дальше.

Поле Filter Mode по умолчанию имеет значение Bilinear, но так как у нас Pixel Art нам нужно изменить его значение на Point, после применения изменений вы видите, что спрайт стал выглядеть лучше, но при приближении видны аномалии, для того чтобы избавиться от них изменим значение поля Format на TrueColor.
Теперь спрайт не имеет никаких аномалий. Запомните такие настройки, поскольку их нужно применять каждый раз при работе с Pixel Art'ом. 
GIF
Внимание: этот надстройку необходимо применить ко всем спрайтам, а их много и они разложены по папкам по этому внизу будет прикреплена иллюстрация того, как Вы можете сэкономить себе кучу времени.
GIF
Возможно следующие уроки будут объемнее, а пока всё.

