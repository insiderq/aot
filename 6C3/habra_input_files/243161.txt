   	
В этой статье я познакомлю вас с появившимися в CSS3 единицами измерения vw, vh, vmin и vmax.

Что такое «Viewport Units»
Viewport Units — это относительные единицы измерения, рассчитывающиеся в процентах от размеров области просмотра браузера. Эти единицы измерения появились в третьей версии спецификации CSS.

Единицы измерения vh и vw
vh и vw можно расшифровать, как viewport height и viewport width — высота и ширина области просмотра соответственно. 1vh равен одному проценту от высоты области просмотра, 1vw равняется одному проценту от ширины области просмотра.

Единицы измерения vmin и vmax
vmin и vmax расшифровывается, как viewport minimal и viewport maximal. 1vmin сравнивает значения 1vh и 1vw, выбирая меньшее из них. 1vmax делает то же самое, но выбирает большее из двух значений. Иначе говоря, если у смартфона ширина экрана меньше высоты, то vmin будет рассчитываться относительно ширины, а vmax будет рассчитываться относительно высоты экрана.

Пример использования
Сейчас в тренде использование полноэкранных секций. Особенно часто их используют в лендингах. Есть много способов сделать такую секцию, однако самый простой и изящный способ — применить единицу измерения vh.

section {
	height: 100vh; /* высота секции равна высоте области просмотра */
}

Посмотрите демо.

Поддержка браузерами
Довольно много браузеров поддерживают эти единицы измерения, что не может не радовать. С последними версиями Chrome, Safari, Opera и Firefox проблем нет. IE начиная с девятой версии имеет частичную поддержку. Проблема с мобильными браузерами: Opera Mini вообще не поддерживает эти единицы, Android Browser поддерживает их только с версии 4.4. 