   	Многим любителям компьютерных игр хорошо знаком контролер Xbox 360. Это заслуженный «ветеран», который имеет значительную армию поклонников. Его можно подключить к компьютеру с помощью шнура или беспроводного адаптера, и многие игроки предпочитают играть с помощью него в игры на PC. Обновленная версия для Xbox Оne тоже может быть подключена к PC через микро-USB разъём, а также имеет ряд улучшений, которые придутся по вкусу игрокам. Для этого контроллера уже выпущены драйвера для Windows 8 и Windows 7 и они совместимы с API XInput. Приложения, которые рассчитаны на работу с геймпадами будут сразу способны работать с новым контроллером. 


Контроллер Xbox 360 зарекомендовал себя как добротный и качественный продукт, который долго служит, лишен каких-то фатальных недостатков. Наблюдается устойчивый рост продаж этих контроллеров, а также количество игр, в которые можно играть с помощью этого устройства.


Что нового?

Обновленная версия для Xbox One вобрала в себя все самое лучшее что было в Xbox 360 контроллере, он стал более удобным. Немного изменилась форма, теперь контроллер лучше «сидит» в руках. Многие также отметят отсутствие отсека для батарей. Теперь задник контроллера не имеет этого выступа. 

Тем не менее, этот контроллер использует для питания обыкновенные батарейки AA или аккумуляторы аналогичного размера. Также следует отметить отсутствие отверстий под шурупы, улучшенную чувствительность DPAD и джойстиков, новые вибромоторы в курках. Есть и «невидимые» изменения, коснувшиеся беспроводного протокола работы. Xbox 360 контроллер работал на скорости около 1.6 мегабита в секунду В новой версии пропускная способность возросла более чем в 20 раз, что позволяет расширить возможности подключаемых аксессуаров. Одним из таких устройств может быть стерео гарнитура:

API
Основным методом работы с геймпадом в ОС Windows является использование С++ API XInput. Следует отметить отсутствие инициализирующих функций, вы просто опрашиваете состояние контроллера:

XINPUT_STATE state;
DWORD result=XInputGetState(0, &state);

if (result == ERROR_SUCCESS)
{
  if (state.Gamepad.wButtons & XINPUT_GAMEPAD_A)
  {
       //нажата кнопка A
  }
}


Функция XInputGetState принимает в качестве параметров индекс контроллера (их может быть подключено несколько) а также структуру с состоянием куда возвращаются значения кнопок:
typedef struct _XINPUT_STATE
{
    DWORD                               dwPacketNumber; //порядковый номер пакета
    XINPUT_GAMEPAD                      Gamepad;
} XINPUT_STATE, *PXINPUT_STATE;

typedef struct _XINPUT_GAMEPAD
{
    WORD                                wButtons; 
    BYTE                                bLeftTrigger; 
    BYTE                                bRightTrigger;
    SHORT                               sThumbLX;
    SHORT                               sThumbLY;
    SHORT                               sThumbRX;
    SHORT                               sThumbRY;
} XINPUT_GAMEPAD, *PXINPUT_GAMEPAD;



Кнопки закодированы битами:

#define XINPUT_GAMEPAD_DPAD_UP          0x0001
#define XINPUT_GAMEPAD_DPAD_DOWN        0x0002
#define XINPUT_GAMEPAD_DPAD_LEFT        0x0004
#define XINPUT_GAMEPAD_DPAD_RIGHT       0x0008
#define XINPUT_GAMEPAD_START            0x0010
#define XINPUT_GAMEPAD_BACK             0x0020
#define XINPUT_GAMEPAD_LEFT_THUMB       0x0040
#define XINPUT_GAMEPAD_RIGHT_THUMB      0x0080
#define XINPUT_GAMEPAD_LEFT_SHOULDER    0x0100
#define XINPUT_GAMEPAD_RIGHT_SHOULDER   0x0200
#define XINPUT_GAMEPAD_A                0x1000
#define XINPUT_GAMEPAD_B                0x2000
#define XINPUT_GAMEPAD_X                0x4000
#define XINPUT_GAMEPAD_Y                0x8000


Чуть сложнее обстоит дело с значениями курков и двух джойстиков. Значения осей X, Y находятся в пределах SHRT_MIN-SHRT_MAX (-32768 +32767), а для курков _UI8_MAX (255). Обычно в играх эти значения нормализуют до -1.0 +1.0. Также для джойстиков следует учитывать так называемый dead zone. Возвращаемые значения осей при нейтральном положении могут отличаться от нуля, и для того чтобы не учитывать их следует использовать стандартные значения «мертвой зоны», вычислять которую следует по следующему алгоритму:

float magnitude = sqrt(state.Gamepad.sThumbRX*state.Gamepad.sThumbRX
+ state.Gamepad.sThumbRY*state.Gamepad.sThumbRY);

if (magnitude > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
{
  //джойстик действительно сдвинули с места
}


Стандартные значения этих пороговых значений имеют следующий вид:
#define XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE  7849
#define XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE 8689
#define XINPUT_GAMEPAD_TRIGGER_THRESHOLD    30


Вы можете посмотреть на более подробные примеры работы с геймпадом на сайте code.msdn.com а также воспользоваться оберткой которая входит в DirectX Toolkit.
Помимо функций непосредственно связанных с опросом состояний контроллера, в XInput также входят функции управления вибромоторами и подключаемыми аксессуарами, например для записи голоса с гарнитуры или воспроизведения звука на гарнитуру.

Поддержка джойстика также есть для управляемого кода в библиотеке XNA и Monogame. В Internet Explorer Developer Channel  вошла экспериментальная реализация W3C Gamepad API и вы можете использовать ваш контроллер для создания игр на HTML/Javasctipt как для веб так и для приложений Windows 8:

<!DOCTYPE html>
<html>
<head>
  <title>Gamepad API Sample</title>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <script>
    function gameLoop() {
      var gamepads = navigator.getGamepads();
      for (var playerIndex = 0; playerIndex < gamepads.length; playerIndex++) {
        var gamepad = gamepads[playerIndex];
        if (gamepad) {
          if (gamepad.buttons[6].pressed || gamepad.buttons[7].pressed) {
            // A trigger is pressed, fire weapon.
            fireWeapon(playerIndex);
          }
        }
      }

      window.requestAnimationFrame(gameLoop);
    }
    gameLoop();
  </script>
</head>
<body>



Если вы хотите внедрить поддержку геймпада в игру на HTML5 для Windows 8 уже сейчас, не дожидаясь выхода в свет следующей версии Internet Explorer, то вы можете воспользоваться оберткой над XInput для Javasctipt.

Поддержка геймпада также есть и в Unity3d. Класс Input содержит все необходимые методы для работы с состояниями контроллера:

using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour {
    void Start () 
    {
    }
    void Update () 
    {
        var axisX = Input.GetAxis("Horisontal");
        if (Input.GetButtonDown("A Btn"))
        {
            //нажата кнопка A
        }
    }
}


Только не забудьте сконфигурировать корректные названия для кнопок и джойстиков (Edit/Project Settings/Input):


Из приведенных примеров очевидно, что работа с геймпадом очень проста и не требует сверхсложных усилий. Если вы разрабатываете игру для Windows 8 с помощью C++, C#, Unity или HTML – обязательно подключите поддержку геймпадов, эта возможность придется по вкусу многим игрокам.

Полезные ссылки

Попробовать Azure бесплатно на 30 дней! 
 
Центр разработки Microsoft Azure (azurehub.ru) – сценарии, руководства, примеры, рекомендации по разработке 
Twitter.com/windowsazure_ru — последние новости Microsoft Azure
Сообществе Microsoft Azure на Facebook – эксперты, вопросы 

 
Изучить курсы виртуальной академии Microsoft по облачным и другим технологиям
 
Бизнес и облако: лучшие практики решений
Windows 8.1 Update для крупных организаций. Начало работы
Гибридное облако Microsoft: Руководство по типовым решениям
Набор средств для подготовки пользователей к Windows 8.1
Введение в графическую библиотеку Win2D

 
Загрузить бесплатную или пробную Visual Studio
Стать разработчиком