     

В нашем проекте мы постоянно меняем основной лендинг или создаем дополнительные лендинги для направлений нашего сервиса. Каждый раз стараемся найти лучший вариант представления нашего продукта. Но что менять, а что оставлять, не всегда понятно с первого взгляда. Поэтому мы перевели статью, в которой препарируются 20 лэндингов известных стартапов. 

Это как собрать идеальную девушку из топ моделей или любимых актрис. Только про лэндинги. 
Итак, запаситесь чашкой кофе, понеслась.
 
Главная страница или лендинг вашего стартапа — это, наверное,  самая важная страница вашего сайта. Думайте о ней как о пустом холсте, на котором вы, как владелец бизнеса или веб дизайнер, можете зацепить вашего первого посетителя.

В первую очередь вашa главная страница должна объяснять о чем ваш продукт или сервис.  И это еще не все — она должна делать гораздо больше:

Она должна убеждать ваших посетителей, что вам можно верить, что вы надежны и действительно существуете.
Она должна говорить вашим посетителям, куда идти дальше.
Она должна предоставлять достаточное количество информации для того, чтобы не дать посетителям нажать на кнопку «назад».

И все это она должна делать в очень короткое время.

К сожалению, нет волшебной формулы с помощью которой можно сделать идеальную главную страницу. Но можно выделить определенный набор элементов, которые постоянно используются в большинстве стартапов на своих страницах, чтобы конвертировать первых посетителей в покупателей. Цель данного исследования — выявить  определенный набор идей которые можно проверять на своих страницах.

Первый шаг: Найдем 20 SaaS стартапов
Мы хотели проанализировать комбинацию старых и новых стартапов для определения элементов на их страницах, из которых можно будет увидеть какую-то модель/шаблон создания «идеальной» страницы. И так начнем с нескольких «топовых стартапов» и «наиболее успешных стартапов»:

ZenDesk
Basecamp
Boostable
Intercom
Optimizely
Contently
NextBigSound
CampaignMonitor
KISSmetrics
BrowserStack
Trello
NewRelic
MixPanel
Wistia
Recurly
Geckoboard
Sqwiggle
Hootsuite
UserVoice
Pingdom

 
Следующий шаг: Анализ каждой главной страницы
Следующий шаг начинается с просмотра каждой главной страницы и элементов,  используемых на этих страницах. Итак что мы нашли:




ZenDesk
Сервис для поддержки  клиентов от Zendesk.

Элементы на странице: Фиксированный хедер, слоган + призыв к действию + скриншот, список функций, список клиентов, призыв к действию и футер.



Basecamp
Basecamp — это любимые всеми приложения для управления проектами. 

Элементы на странице: Хедер, Слоган (использующий социальные доказательства) + призыв к действию / соц. доказательства, скриншоты, FAQ, еще больше соц. доказательств, ссылки на дальнейшую информацию и футер.



Boostable
Повысьте свою видимость в маркетплейсах с помощью рекламы с Boostable.

Элементы на странице: Слоган + поле для емейла + призыв к действию, как это работает, демо, команда и инвестиционная информация, карьера и футер.



Intercom
Сервис по сбору информации о пользователях и коммуникации с клиентами.

Элементы на странице: Фиксированный хедер, Слоган + поле для емейла + призыв к действию, сравнение функций, список клиентов, функции + скриншоты, отзывы и футер.



Optimizely
Сервис для А/Б тестов, который вы действительно используете.

Элементы на странице: Хедер, Слоган + поле для емейла + призыв к действию и футер



Contently
Contently дает возможность журналистам и брендам привлекать аудиторию более интересным содержанием.

Элементы на странице: Хедер, слоган + список клиентов + призыв к действию, как это работает, список клиентов, соц. доказательство и футер



NextBigSound
Аналитика и инсайты для музыкальной индустрии.

Элементы на странице: Хедер, Слоган + поиск, главные функции,  отчет, кейсы, отзывы и футер



CampaignMonitor
Посылайте красивые емейлы рассылки с Campaign Monitor.

Элементы на странице: Фиксированный хедер, слоган + призыв к действию, список клиентов, функции и футер.



KISSmetrics
Информация о клиентах и веб аналитика.

Элементы на странице: Слоган + после для веб сайта + призыв к действию и футер.



BrowserStack
Проверяйте свой сайт на поддержку кроссбраузерности на реальных браузерах.

Элементы на странице: Хедер, Слоган + Видео + форма подписки, список клиентов, отзывы, функции и футер.



Trello
Trello следит за всем, от общей картины до маленьких деталей.

Элементы на странице: Слоган + призыв к действию, функции + скриншоты, список клиентов, призыв к действию и футер.



NewRelic
Слежение и управление за производительностью приложения.

Элементы на странице: Хедер, слоган + призыв к действию, список клиентов, отзывы, функции + видео и футер.



MixPanel
Наиболее продвинутая аналитическая платформа для мобильного и веб рынка.

Элементы на странице: Хедер, Слоган + призыв к действию, список клинтов, кейсы, отзывы и футер.



Wistia
Wistia предоставляет профессиональный видео хостинг.

Элементы на странице: Хедер, слоган, функции + видео, отзывы и футер.



Recurly
Эксперты по оплате подписок и повторяемым оплатам.

Элементы на странице: Хедер, Слоган + призыв к действию, список клиентов, выгода + отзывы, функции и футер.



Geckoboard
Информационные доски для бизнеса.

Элементы на странице: Хедер, слоган + призыв к действию + видео на фоне, список клиентов, выгода, как это работает, отзывы и футер.



Sqwiggle
ПО для совместной работы онлайн для удаленных и виртуальных команд.

Элементы на странице: Хедер, слоган _ призыв к действию + скриншоты, проблемы, отзывы, выгода, отзывы, форма для регистрации и футер. 



Hootsuite
Доска для управления соц. медиа.

Элементы на странице: Фиксированный хедер, слоган + призыв к действию, планы, функции, целевой рынок и футер.



UserVoice
ПО для онлайн помощи и получение обратной связи от пользователей.

Элементы на странице: Фиксированный хедер, слоган + призыв к действию, функции + скриншоты, кейсы, видео, список клиентов и футер.



Pingdom
Простой мониторинг производительности и времени готовности к работе.

Элементы на странице: Фиксированный хедер, слоган + призыв к действию + скриншоты, список клиентов, отзывы, выгода, соц. доказательства, функции, планы и цены, клиенты  и футер



 
Следующий шаг: Анализируем каждый используемый элемент
С помощью собранной информации давайте посмотрим как часто используются каждые конкретные элементы.

Вот несколько интересных статистических фактов об этих 20-ти стартапах:

100% - содержат слоган 75% -  содержат описание после слогана

95% -  содержат призыв к действию на первом экране 20% -  содержат призыв к действию с полем ввода (емейл или веб сайт) перед ним

50% -  содержат слово «бесплатно» в их призывах к действию

10% -  содержат полную форму регистрации

5% -   содержат информацию о ценах 20% -  указывают выгоду от пользования их продуктом 65% -  указывают список функций их продукта

Мультимедия

20% - содержат видео

35% - содержат скриншоты

Хедеры и футеры

85% - содержат хедер

30% - содержат фиксированный хедер, который остается на экране при прокручивании

100% - содержат футер

Социальные доказательства

80% - содержат какую-то форму соц. доказательств (список клиентов, отзывы или кейсы)

60% - содержат список клиентов

50% - содержат отзывы

15% - содержат кейсы
 

Что же эти цифры говорят нам?

Важно помнить, поскольку мы не имеем в нашем распоряжении конкретных коэффициентов конверсии, то мы не можем определить какие элементы работают лучше других (статистически). Однако, поскольку целью анализа было определить список идей для тестирования, то информации более чем достаточно.

Если вы собираетесь обновить свою главную страницу, тогда следующая часть статьи будет наиболее интересна для вас. Ниже мы покажем список идей которые вы можете попробовать.

 
1. Ваш слоган: Ваш шанс выделиться
Вашей странице необходим слоган. Думайте о нем как о способе рассказать вашим посетителям о вашем сервисе за 5 секунд. Надо чтобы он был коротким и внушительным. Он должен выделять выгоды от вашего сервиса в одном предложении.

Все стартапы содержали слоган — и 75%  из них писали описание сразу под ним. Старайтесь использовать как можно более короткие фразы, произнесите их пару раз вслух. Представьте, что у вас есть 5 секунд чтобы объяснить о чем ваш продукт или сервис другу… чтобы вы сказали?

 

Тестируем идеи:

Тестируйте новые слоганы
Добавляйте или меняйте описание после слогана

Вот несколько примеров от стартапов:

    



             

 
2. Ваш призыв к действию: Увлеките ваших клиентов на первом экране
Почти все стартапы (95%) содержали призыва к действию на первом экране. Текст в этом призыве может серьезно повлиять на количество посетителей кликнувших на него, это точно стоит тестировать. Половина стартапов использовали слово «бесплатно» в своих призывах, несмотря на то, что у них у всех существуют платные подписки.

Так же исследование показало, что 20% стартапов помещали поле для емейла или сайта перед призывом к действию. Это создавало впечатление у посетителей, что они уже потратили определенное время на регистрацию и готовы работать с сервисом, при этом еще не начав регистрироваться.

Тестируем идеи:

Тестируйте новые фразы для призывов к действию — попробуйте  слово «бесплатно»
Попробуйте разные цвета и форматы
Добавьте поле для ввода перед призывом, чтобы зацепить посетителя
Если ваша форма для регистрации не большая, попробуйте вставить её в главную страницу

 

Вот несколько примеров от стартапов:

                  

 
3. Социальные доказательства: Формируйте доверие и вовлеченность
Неудивительно, что большинство страниц (80%) использовали какую-либо форму социальных доказательств. Они работают, потому что посетителям нужно чувствовать, что вашим продуктом/ сервисом пользуются и он настоящий.

Три основных формы социальных доказательств:

Список клиентов или количество клиентов (60%)
Отзывы (50%)
Кейсы (15%)

Даже если ваш стартап только начал свою работу, не так уж сложно получить отзывы от друзей, которые попробовали использовать ваш продукт. И со временем вы замените менее популярных клиентов, отзыв и кейсы на более впечатляющие имена.

 

Тестируем идеи:

Добавьте список клиентов — даже небольшой список сможет помочь.
Используйте сервис www.userstats.com чтобы быстро добавить соц. доказательства на ваш сайт.
Попросите ваших клиентов  написать отзыв.
Если вы знаете клиента, который доволен вашим продуктом, попробуйте построить кейс на основе  его примера.

 

Вот несколько примеров от стартапов:

               

 
4. Функции: Объясните как работает ваш продукт
Хотя 60% стартапов показали список своих функций, 20% из них посвятили целую секцию выгодам от их продукта. Пока ваш продукт не достиг такой популярности, что большинство людей уже знают как он работает, вам нужно объяснять что же ваш сервис делает.

Существует много различных путей объяснить о чем ваш продукт  — вы можете показать скриншоты или видео. Или описать в текстовой форме как работает каждый элемент. Один из стартапов разместил отзыв после каждой функции — это отличный способ показать выгоды словами пользователей.

 

Тестируем идеи:

Добавьте скриншоты, чтобы визуально объяснить что делает ваш продукт.
Если возможно, создайте короткое видео, описывающее какие выгоды принесет ваш продукт.
Постарайтесь получить отзыв на каждую функцию в вашем продукте, это поможет показать выгоды от них словами клиентов
Делайте ваши функции более ориентированными на выгоду. Просто отвечайте: Как эта функция поможет моим клиентам?

 

Вот несколько примеров от стартапов:

              

 
5. Больше информации: Для посетителей, которые хотят узнать больше
Вы точно найдете определенное количество посетителей, которые не будут удовлетворены информацией на главной странице. Эти пользователи захотят найти как можно больше информации о вашем продукте перед тем, как начать думать о его использовании. Именно поэтому вам нужно убедиться, что посетители легко могут попасть на другие части вашего сайта с главной страницы.

Некоторые стартапы делали это, добавляя второй призыв к действию, ведущий на подробное описание продукта, на страницу FAQ или добавляли полезные ссылки в футер. Убедитесь, что вашим посетителям есть куда идти дальше, если вы не хотите отправлять их на регистрацию.

 

Тестируем идеи:

Добавьте второй призыв к действию, ведущий на дополнительную информацию.
Добавляйте FAQ секцию на страницу с ответами на основные вопросы.
Вставьте ссылки на блок или базу ответов в футер.

 

Вот несколько примеров от стартапов:

        

 
И так финальный список: идея для проверки
Благодаря анализу сайтов можно подчеркнуть 17 идей которые стоит попробовать на вашей странице. Важно помнить, что идея которая работает на одном сайте может не подходить к вашему.  Подробное А/Б тестирование необходимо для проверки, как влияет тот или иной элемент на конверсию. И помните: иногда удаление контента с вашей страницы может оказаться самым эффективным решением. И так финальный список:

Тестируйте новые слоганы.
Добавляйте или меняйте описание после слогана.
Тестируйте новые фразы для призывов к действию — попробуйте  слово «бесплатно».
Попробуйте разные цвета и форматы.
Добавьте поле для ввода перед призывом, чтобы зацепить посетителя.
Если ваша форма для регистрации небольшая, попробуйте вставить её в главную страницу.
Добавьте список клиентов — даже небольшой список сможет помочь.
Используйте сервис www.userstats.com чтобы быстро добавить соц. доказательства на ваш сайт.
Попросите ваших клиентов написать отзыв.
Если вы знаете клиента, который доволен вашим продуктом, попробуйте построить кейс на основе его примера.
Добавьте скриншоты, чтобы визуально объяснить что делает ваш продукт.
Если возможно, создайте короткое видео, описывающее какие выгоды принесет ваш продукт.
Постарайтесь получить отзыв на каждую функцию в вашем продукте, это поможет показать выгоды от них словами клиентов.
Делайте ваши функции более ориентированными на выгоду. Просто отвечайте: Как эта функция поможет моим клиентам?
Добавьте второй призыв к действию, ведущий на дополнительную информацию.
Добавляйте FAQ секцию на страницу с ответами на основные вопросы.
Вставьте ссылки на блок или базу ответов в футер.

 
