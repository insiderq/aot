   	Здравствуйте, Друзья! 

В статье вкратце расскажу о том, как применить локализацию к вашему приложению с помощью DevUtils ELAS for Microsoft Translation, это расширения для DevUtils ELAS Core. 
DevUtils ELAS Core это пакет Nuget, но он не добавляет ни каких дополнительных Reference'ов в проект он только встраивается в процесс сборки проекта, а так же добавляет в Solution файл своей конфигурации ".elas\ElasConfiguration.props". После сборки в проекте (на момент написания статьи последняя версия 0.0.5 которая поддерживает .Net4.x, Windows Store, Windows Store Phone проекты и Resx, Resw файлы) рядом с каждым файлом ресурсов создастся Xliff(Xlf) файл. 
DevUtils ELAS for Microsoft Translation автоматически переведёт строки с помощью Microsoft Translation (требуется подключение к интернету). На данный момент используется бесплатная подписка, это 2`000`000 символов в месяц. Если будет превышен этот лимит вы можете самостоятельно зарегистрироваться/подписаться на data market (Obtaining an Access Token пункт 1 и 2) и добавить значения в ElasConfiguration.props.

Пример:
<?xml version="1.0" encoding="utf-8"?>
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
...
	<PropertyGroup>
		<ElasMicrosoftTranslationClientId>%Ваш Client ID%</ElasMicrosoftTranslationClientId>
		<ElasMicrosoftTranslationClientSecret>%Ваш Client secret%</ElasMicrosoftTranslationClientSecret>
	</PropertyGroup>
...
</Project>


После проанализируйте Xliff файл(ы) и переведите «target state» в значение «translated» и опять запустите «Build». Перевод готов.

Рекомендации
Для комфортной работы с Xliff файлами в Visual Studio добавьте себе XML схему, она находится в packages\DevUtils.Elas.Core.x.x.x.x\Schemas\xliff-core-1.2-transitional.xsd.
При локализации большого файла ресурсов (первый раз) чтобы не возникло ситуация что Build процесс «завис», включите MSBuild project build output verbosity в Visual Studio ниже чем Minimal, в это случае вы увидите ход выполнения автоматического перевода с помощью Microsoft Translate.

Заключение
Проект находится на начальной стадии разработки. В будущем планируется расширить список поддерживаемых проектов и источников для перевода.
