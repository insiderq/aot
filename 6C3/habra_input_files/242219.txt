   	

Решил поделиться сим знанием. Всё это я сам собрал в интернете. Опробовал и удостоверился, что оно работает. Дам немного комментариев к этой инструкции, чтобы было понятнее.

Начну с того, что свой загрузочный cd диск хотел создать давно, но руки не доходили. Обычно я просто обходился дампом системы, если хотелось сделать копию рабочей. Но каждый раз делать всё руками — руки отсохнут. Тем более, что не каждому объяснишь как сделать копию системы, как разбить и отформатировать диск, сделать диск загрузочным. Не у всех, знаете ли, есть рабочий линукс под рукой. Ну вот руки то мои взмолились и надоело всё делать по шпаргалкам — детсад ей богу.

Перейдем к практике. Установим эти замечательные пакеты.

apt-get install xorriso live-build extlinux syslinux squashfs-tools


xorriso для создания загрузочного образа
syslinux, extlinux для использования mbr загрузки
squashfs-tools для создание сжатой файловой системы
live-build  для создания самой системы, которая будет зажата и помещена в образ iso

Создаем каталог для образа и распаковываем минимальную систему повыбранной архитектуре. chroot — это корневая папка, где ибудет образ.

mkdir ~/livework && cd ~/livework
debootstrap --arch=i386 wheezy chroot


Далее чрутимся, монтируем для эмуляции рабочей системы необходимые каталоги. Для генерации UUID устанавливаем dbus-uuidgen. Далее ставим ядро и необходимые утилиты для live загрузки. Ну и далее себе ни в чем не отказываем, устанавливаем всё что хочется. Можно и иксы установить и сделать автозагрузку этих иксов под пользователем или рутом. Позже, когда вы уже сделаете диск, его можно протестировать на виртуалке и если что не нравится тут же переделать войдя чрутом в папку chroot.

cd ~/livework
chroot chroot
mount none -t proc /proc
mount none -t sysfs /sys
mount none -t devpts /dev/pts
export HOME=/root
export LC_ALL=C
apt-get install dialog dbus
dbus-uuidgen > /var/lib/dbus/machine-id
apt-get install linux-image-686 live-boot
apt-get install dump bzip2 mc icewm ....
passwd
apt-get clean
rm /var/lib/dbus/machine-id && rm -rf /tmp/*
umount /proc /sys /dev/pts
exit


Короче, образ системы мы создали. Далее создаем папку для live загрузчика. Копируем в нее vmlinuz и inird ядра вашей созданной системы. И создаём сжатую файловую ситему из папки chroot

mkdir -p binary/live && mkdir -p binary/isolinux
cp chroot/boot/vmlinuz-* binary/live/vmlinuz
cp chroot/boot/initrd.img-* binary/live/initrd
mksquashfs chroot binary/live/filesystem.squashfs -e boot


Далее копируем файлы, необходимые для загрузки с CD, редактируем меню загрузки.

cp /usr/lib/syslinux/isolinux.bin binary/isolinux/.
cp /usr/lib/syslinux/menu.c32 binary/isolinux/.
nano binary/isolinux/isolinux.cfg
ui menu.c32
prompt 0
menu title Boot Menu
timeout 300

label live-686
	menu label ^Live (686)
	menu default
	linux /live/vmlinuz
 	append initrd=/live/initrd boot=live persistence quiet

label live-686-failsafe
	menu label ^Live (686 failsafe)
	linux /live/vmlinuz
	append initrd=/live/initrd boot=live persistence config memtest noapic noapm nodma nomce nolapic nomodeset nosmp nosplash vga=normal

endtext


Всё, готово! Теперь только осталось создать образ диска.

xorriso -as mkisofs -r -J -joliet-long -l -cache-inodes -isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin -partition_offset 16 -A "Debian Live"  -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o remaster.iso binary


Когда вы во второй раз начнете собирать диск, то удалите файл binary/live/filesystem.squashfs, иначе комп будет долго выяснять чего же нужно добавить в гигабайтный архив. А вы будете нервничать, почесывая затылок вожидании нового ребилда.
У меня в папке livework лежит скрипт, который я запускаю, когда хочу пересоздать диск.

rm binary/live/filesystem.squashfs
mksquashfs chroot binary/live/filesystem.squashfs -e boot
xorriso -as mkisofs -r -J -joliet-long -l -cache-inodes -isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin -partition_offset 16 -A "Debian Live"  -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o install.iso binary


Если вы хотите тот-же образ системы сделать на флешке, то нет ничего проще.
Разница только в том, что вам не надо создавать образ диска, а вместо этого нужно пометить раздел с системой загрузочным (fdisk) и записать в загрузочную область диска mbr. Ну и для загрузки использовать extlinux вместо isolinux, если у вас раздел отформатирован в EXT. В примере диск /dev/sda, а в /mnt примонтирован образ системы

extlinux -i /mnt && cat /usr/lib/extlinux/mbr.bin > /dev/sda
cp /usr/lib/extlinux/*.c32 /mnt && cp /usr/lib/syslinux/vesamenu.c32 


Меню загрузки можно скопировать с вашего ISO образа но в другой файл, так как теперь у вас не ISO а EXT.

cp isolinux/isolinux.cfg /mnt/extlinux.conf


Чуть отойду от темы. Лайв ISO образ хорош тем, что он стабилен и не изменяем. Плохо же, что он грузит оперативку. Если вы хотите освободится от сжатого образа, то загрузка у вас изменится. Итак, в случае, если у нас есть НЕ сжатый образ системы и мы хотим просто прописать его загрузку, то пишем такой конфиг.
nano /mnt/extlinux.conf


Содержимое файла. Тут ###uuid### замените на ваш или вообще впишите  root=/dev/sda1, например.
До ядра пишется полный путь, ссылки не канают. Повторюсь, тут мы отошли немного от темы, конфиг нужен не для сжатой системы, а для обычной.

ui vesamenu.c32
prompt 0
timeout 300

menu title Boot Zagruzka
menu color title 1;33;44
menu color sel 7;37;40
menu color unsel 33;44
menu color border 33;44
label Linux-Debian-686
        kernel /boot/vmlinuz-3.2.0-0.bpo.2-686-pae
        append initrd=/boot/initrd.img-3.2.0-0.bpo.2-686-pae root=UUID=###uuid### ro quiet

label Linux-Debian-686 (rescue mode)
        kernel /boot/vmlinuz-3.2.0-0.bpo.2-686-pae
        append initrd=/boot/initrd.img-3.2.0-0.bpo.2-686-pae root=UUID=###uuid### ro single


Ну и собственно, возвращаясь к сжатой системе LiveCD, копируем папку на диск со сжатой файловой системой.

cp -R live /mnt

