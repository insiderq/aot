{
    "0_status": "success",
    "1_total_words": 355,
    "2_total_polar_words": 46,
    "3_total_positive_words": 23,
    "4_total_negative_words": 14,
    "5_total_neutral_words": 9,
    "6_total_polarity_score": 16,
    "7_total_polarity_divided_by_total_words": 0.04507042253521127,
    "8_total_polarity_divided_by_total_polar_words": 0.34782608695652173
}