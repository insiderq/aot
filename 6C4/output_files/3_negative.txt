{
    "0_status": "success",
    "1_total_words": 237,
    "2_total_polar_words": 40,
    "3_total_positive_words": 16,
    "4_total_negative_words": 13,
    "5_total_neutral_words": 11,
    "6_total_polarity_score": 5,
    "7_total_polarity_divided_by_total_words": 0.02109704641350211,
    "8_total_polarity_divided_by_total_polar_words": 0.125
}