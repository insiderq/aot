{
    "0_status": "success",
    "1_total_words": 172,
    "2_total_polar_words": 29,
    "3_total_positive_words": 11,
    "4_total_negative_words": 8,
    "5_total_neutral_words": 10,
    "6_total_polarity_score": 6,
    "7_total_polarity_divided_by_total_words": 0.03488372093023256,
    "8_total_polarity_divided_by_total_polar_words": 0.20689655172413793
}