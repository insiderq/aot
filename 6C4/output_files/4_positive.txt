{
    "0_status": "success",
    "1_total_words": 433,
    "2_total_polar_words": 65,
    "3_total_positive_words": 29,
    "4_total_negative_words": 22,
    "5_total_neutral_words": 14,
    "6_total_polarity_score": 11,
    "7_total_polarity_divided_by_total_words": 0.025404157043879907,
    "8_total_polarity_divided_by_total_polar_words": 0.16923076923076924
}