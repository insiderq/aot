{
    "0_status": "success",
    "1_total_words": 400,
    "2_total_polar_words": 51,
    "3_total_positive_words": 21,
    "4_total_negative_words": 18,
    "5_total_neutral_words": 12,
    "6_total_polarity_score": 0,
    "7_total_polarity_divided_by_total_words": 0.0,
    "8_total_polarity_divided_by_total_polar_words": 0.0
}