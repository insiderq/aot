{
    "0_status": "success",
    "1_total_words": 246,
    "2_total_polar_words": 61,
    "3_total_positive_words": 19,
    "4_total_negative_words": 31,
    "5_total_neutral_words": 11,
    "6_total_polarity_score": -22,
    "7_total_polarity_divided_by_total_words": -0.08943089430894309,
    "8_total_polarity_divided_by_total_polar_words": -0.36065573770491804
}