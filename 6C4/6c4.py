#!/usr/local/bin/python3
# -*- coding: UTF-8-*-
import subprocess 
import re
import json
import sys
import os
"""
програма распознает эмоциональную окраску текста с использованием словаря.
Все слова в тексте приводятся в начальную форму с помощью морфоанализатора mystem
Затем каждое из них проверяется на принадлежность словарю.
Затем на принадлежность словарю проверяются пары и тройки слов.
При наличии слова или слововосочетания в словаре суммарный окрас
увеличивается или уменьшается на 2 или 1 (зависит от веса данного слова)
"""


def load_dictionary(filename):
	"""
	загружает весь словарь в структуру dictionary для более быстрого поиска в нем.
	вычленяет из строки вес слова и его окраску.
	"""
	dic = {}
	file_dic = open(filename, 'r', encoding='cp1251')
	line = ""
	while True:
		line = file_dic.readline()
		if not line:
			break
		m = re.search(r'type=(.*) len=.* word1=(.*) pos1=.* stemmed1=.* priorpolarity=(.*)', line)	
		if m:
			word = m.group(2)
			polarity = m.group(3)
			weigth = m.group(1)
			dic[word] = [polarity, weigth]
	return dic

def load_text_lemmas(filename):
	"""
	загружает исходный текст в mystem и сохраняет все выходные начальные формы.
	возвращает массив лемм всех вслов файла.
	"""
	shell_command = os.path.join("mystem","mystem")+" -l --format json \"{}\"".format(filename)
	exit_arr = []
	try:
		json_result = subprocess.check_output(shell_command, shell=True).decode("utf-8")
		json_results = json_result.split("[]")

	except subprocess.CalledProcessError as e:
		return exit_arr
	results = []
	
	for r in json_results:
		try:
			results.append(json.loads(r))
		except (IndexError, KeyError, ValueError) as e:
			pass

	for paragraph in results:
		for r in paragraph:
			try:
				exit_arr.append(r["analysis"][0]["lex"])
			except (IndexError, KeyError, ValueError) as e:
				pass

	return exit_arr

def calculate_polarity(lemmas, dic):
	"""
	Реализация наивного алгоритма подсчета окраски.
	"""
	#каунтеры с начальными значениями
	total_polarity = 0
	total_words = len(lemmas)
	total_positive_words = 0
	total_negative_words = 0
	total_neutral_words = 0
	"""
	Небольшой хак
	Продляем массив на две клетки, чтобы 
	на последней итерации сработали индексыi+1 i+2
	"""
	lemmas.append("")
	lemmas.append("")

	"""
	В словаре встречаются фразы из двух и из трех слов. 
	Фразы из двух слов очень полезны, поскольку могут содержать 
	Отрицание: "не нравится", которое, при прохождении по одному слову
	сработало бы в другую сторону."""
	i = 0
	while i < len(lemmas)-2:

		key3 = lemmas[i]+" "+lemmas[i+1]+" "+lemmas[i+2]
		key2 = lemmas[i]+" "+lemmas[i+1]
		key1 = lemmas[i]

		d3 = dic.get(key3)
		d2 = dic.get(key2)
		d1 = dic.get(key1)
		"""
		сначала пытаемся проверить словосочетание длины 3 
		затем длины 2, а затем уже одинарное слово.
		при нахождении словосочетания в словаре важно пропустить эти слова, 
		чтобы они не стали потом проверяться по отдельности.
		Пример: 
			1) "не нравится" -2 пункта. 
			2) Проверяем слова по отдельности "не" и "нравится"
			3) "нравится" +2 пункта.
			4) Итого окрас не изменился, даже с явно отрицательной фразой "не нравится".
		"""
		if d3:
			i+=3 #skip 3 words
			d = d3
		elif d2:
			i+=2 #skip 2 words
			d = d2
		elif d1:
			i+=1 #skip word
			d = d1
		else:
			i+=1 
			continue # next iteration

		"""
		Здесь собственно проверка веса слова и его полярности.
		"""
		if d[0] == "positive":
			sign = 1
			total_positive_words +=1
		elif d[0] == "negative":
			sign = -1
			total_negative_words += 1
		elif d[0] == "neutral":
			sign = 0
			total_neutral_words += 1
		if d[1] == "strongsubj":
			weight = 2
		elif d[1] == "weaksubj":
			weight = 1

		# вносим изменения в итоговую оценку.
		total_polarity += weight*sign

	# возвращаем статистику
	return [total_polarity, total_words, total_neutral_words, total_negative_words, total_positive_words]

def process_file(dic, input_filename, ouput_filename):
	"""
	Обабатывает файл алгоритмом проверки тональности.
	Формирует структуру выходного файла.
	В выходном файле каждый ключ начинается с цифры, 
	это просто для придания им порядка после сериализации
	"""
	result_file = open(ouput_filename,'w', encoding="utf-8")
	lemmas = load_text_lemmas(input_filename)
	result_dict = {}
	if (len(lemmas)==0):
		result_dict["0_status"] = "error"
		result_dict["1_reason"] = "Input Text is Empty"
		result_file.write(json.dumps(result_dict, indent = 4, sort_keys=True))
		result_file.close()
		return
		
	result = calculate_polarity(lemmas, dic)
	result_dict["1_total_words"] = result[1]

	if result[2]+result[3]+result[4]==0:
		result_dict["0_status"] = "error"
		result_dict["1_reason"] = "No Polar Words"
		result_file.write(json.dumps(result_dict, indent = 4, sort_keys=True))
		return
	result_dict["2_total_polar_words"] = result[2]+result[3]+result[4]
	result_dict["3_total_positive_words"] = result[4]
	result_dict["4_total_negative_words"] = result[3]
	result_dict["5_total_neutral_words"] = result[2]
	result_dict["6_total_polarity_score"] = result[0]
	result_dict["7_total_polarity_divided_by_total_words"] = float(result[0])/result[1]
	result_dict["8_total_polarity_divided_by_total_polar_words"] = float(result[0])/(result[2]+result[3]+result[4])
	result_dict["0_status"] = "success"
	result_file.write(json.dumps(result_dict, indent = 4, sort_keys=True))
	result_file.close()
	return

# Загружаем словарь в память единоразово, потом будем везде передавать
dic = load_dictionary("rus_dic.txt")
# Пробегаем по всем файлам в директории input_files и процессим их.
input_files = os.listdir("input_files")
for input_file in input_files:
	if input_file[-3:] == "txt":
		input_filename = os.path.join("input_files",input_file)
		output_filename = os.path.join("output_files",input_file)
		process_file(dic,input_filename, output_filename)